<?php

namespace Drupal\managed;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\managed\Core\ManagedEntityTrait;
use Drupal\managed\Core\ManagedEntityType;


/**
 * Base class of all entities that should be taken care of by the
 * module `managed`.
 *
 * Adding the `@Entity` annotation to a subclass of this class will
 * emit a corresponding entity type. Keep in mind that managed entities
 * are always content entities and are therefore stored in the database.
 *
 * To create bundles of an entity a subclass must be created decorated with
 * the `@Bundle` annotation. The related entity must contain a field annotated
 * with the `@BundleKey('bundle')` annotation.
 */
abstract class Entity extends ContentEntityBase
{
  use ManagedEntityTrait;


  /**
   * Provides base field definitions for an entity type.
   *
   * Implementations typically use the class
   * \Drupal\Core\Field\BaseFieldDefinition for creating the field definitions;
   * for example a 'name' field could be defined as the following:
   * @code
   * $fields['name'] = BaseFieldDefinition::create('string')
   *   ->setLabel(t('Name'));
   * @endcode
   *
   * By definition, base fields are fields that exist for every bundle. To
   * provide definitions for fields that should only exist on some bundles, use
   * \Drupal\Core\Entity\FieldableEntityInterface::bundleFieldDefinitions().
   *
   * The definitions returned by this function can be overridden for all
   * bundles by hook_entity_base_field_info_alter() or overridden on a
   * per-bundle basis via 'base_field_override' configuration entities.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entityType
   *   The entity type definition. Useful when a single class is used for multiple,
   *   possibly dynamic entity types.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   *   An array of base field definitions for the entity type, keyed by field
   *   name.
   *
   * @see \Drupal\Core\Entity\EntityManagerInterface::getFieldDefinitions()
   * @see \Drupal\Core\Entity\FieldableEntityInterface::bundleFieldDefinitions()
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entityType) {
    if (!($entityType instanceof ManagedEntityType)) {
      throw new \InvalidArgumentException();
    }

    $fields = array();
    foreach ($entityType->getFields() as $field) {
      $fields[$field->getName()] = $field->getDefinition();
    }

    return $fields;
  }


  /**
   * Provides field definitions for a specific bundle.
   *
   * This function can return definitions both for bundle fields (fields that
   * are not defined in $base_field_definitions, and therefore might not exist
   * on some bundles) as well as bundle-specific overrides of base fields
   * (fields that are defined in $base_field_definitions, and therefore exist
   * for all bundles). However, bundle-specific base field overrides can also
   * be provided by 'base_field_override' configuration entities, and that is
   * the recommended approach except in cases where an entity type needs to
   * provide a bundle-specific base field override that is decoupled from
   * configuration. Note that for most entity types, the bundles themselves are
   * derived from configuration (e.g., 'node' bundles are managed via
   * 'node_type' configuration entities), so decoupling bundle-specific base
   * field overrides from configuration only makes sense for entity types that
   * also decouple their bundles from configuration. In cases where both this
   * function returns a bundle-specific override of a base field and a
   * 'base_field_override' configuration entity exists, the latter takes
   * precedence.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entityType
   *   The entity type definition. Useful when a single class is used for multiple,
   *   possibly dynamic entity types.
   * @param string $bundleID
   *   The bundle.
   * @param \Drupal\Core\Field\FieldDefinitionInterface[] $baseFields
   *   The list of base field definitions.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   *   An array of bundle field definitions, keyed by field name.
   *
   * @see \Drupal\Core\Entity\EntityManagerInterface::getFieldDefinitions()
   * @see \Drupal\Core\Entity\FieldableEntityInterface::baseFieldDefinitions()
   *
   * @todo WARNING: This method will be changed in
   *   https://www.drupal.org/node/2346347.
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entityType, $bundleID, array $baseFields) {
    if (!($entityType instanceof ManagedEntityType)) {
      throw new \InvalidArgumentException();
    }

    $fields = array();
    $bundle = $entityType->getBundle($bundleID);
    if (!is_null($bundle)) {
      foreach ($bundle->getFields() as $field) {
        $fields[$field->getName()] = $field->getDefinition();
      }
    }

    return $fields;
  }
}
