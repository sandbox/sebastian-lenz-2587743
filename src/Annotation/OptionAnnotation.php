<?php

namespace Drupal\managed\Annotation;

use Drupal\managed\Vendor\Addendum\Annotation;
use Drupal\managed\Vendor\Addendum\ReflectionAnnotatedProperty;
use Drupal\managed\Utils\StringUtils;


/**
 * A decorator for fields that expose a list field defining a single
 * option within the list field.
 *
 * @ManagedAnnotation(id="Option")
 */
class OptionAnnotation extends Annotation
{
  /**
   * The human readable label of this option.
   *
   * @var string
   */
  protected $label;



  /**
   * Initialize this annotation. Called by the finalized constructor of the
   * base class `Drupal\managed\Vendor\Addendum\Annotation`.
   *
   * @param ReflectionAnnotatedProperty $reflection
   * @throws \Exception
   */
  protected function checkConstraints($reflection) {
    if (!$reflection instanceof ReflectionAnnotatedProperty) {
      throw new \Exception('The annotation `@Option` must decorate a class property.');
    }

    if (!isset($this->label) && !isset($this->value)) {
      throw new \Exception('The annotation `@Option` requires either a `label` value or a `key` value.');
    }

    if (!isset($this->label)) {
      $this->label = StringUtils::humanize($this->value);
    }
    elseif (!isset($this->value)) {
      $this->value = $this->label;
    }
  }


  /**
   * Return the human readable label of this option.
   * @return string
   */
  public function getLabel() {
    return $this->label;
  }


  /**
   * Return the underlying value of this option.
   * @return float|int|string
   */
  public function getValue() {
    return $this->value;
  }
}
