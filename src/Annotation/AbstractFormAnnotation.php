<?php

namespace Drupal\managed\Annotation;

use Drupal\managed\Utils\StringUtils;
use Drupal\managed\Vendor\Addendum\Annotation;
use Drupal\managed\Vendor\Addendum\ReflectionAnnotatedProperty;


/**
 * Base class of all annotations that define an element that should be
 * placed inside a form.
 */
abstract class AbstractFormAnnotation extends Annotation
{
  /**
   * The base name of the field.
   *
   * This will always be set to the name of the defining property.
   *
   * @var string
   */
  private $name;

  /**
   * The human readable label.
   *
   * Defaults to the humanized version of the property name.
   *
   * @var string
   */
  protected $label;

  /**
   * A human readable description.
   *
   * @var string
   */
  protected $description;

  /**
   * The weight of this field.
   *
   * Defaults to index of the position the property is defined within the class.
   *
   * @var int
   */
  protected $weight;



  /**
   * Initialize this annotation. Called by the finalized constructor of the
   * base class `Drupal\managed\Vendor\Addendum\Annotation`.
   *
   * @param \Drupal\managed\Vendor\Addendum\ReflectionAnnotatedProperty $reflection
   * @throws \Exception
   */
  protected function checkConstraints($reflection) {
    if (!$reflection instanceof ReflectionAnnotatedProperty) {
      throw new \Exception('A form annotation must decorate a class property.');
    }
  }


  /**
   * Allow this field to pull data from the final property reflection.
   *
   * @param \Drupal\managed\Vendor\Addendum\ReflectionAnnotatedProperty $reflection
   */
  public function pullReflectionData($reflection) {
    $this->name = $reflection->getName();

    if (!isset($this->label)) {
      $this->label = StringUtils::humanize($this->name);
    }
  }


  /**
   * Set the weight of this field.
   *
   * @param int $weight
   * @param bool $force
   * @return \Drupal\managed\Annotation\Field\AbstractFieldAnnotation
   */
  public function setWeight($weight, $force = true) {
    if ($force || !isset($this->weight)) {
      $this->weight = $weight;
    }

    return $this;
  }


  /**
   * Return the weight of this field.
   *
   * @return int
   */
  public function getWeight() {
    return $this->weight;
  }


  /**
   * Return the base name of the field.
   *
   * @return string
   */
  public function getName() {
    return $this->name;
  }


  /**
   * Return the human readable label.
   *
   * @return string
   */
  public function getLabel() {
    return $this->label;
  }


  /**
   * Return the human readable description.
   *
   * @return string
   */
  public function getDescription() {
    return $this->description;
  }
}
