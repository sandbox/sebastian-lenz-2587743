<?php

namespace Drupal\managed\Annotation;

use Drupal\managed\Annotation\Relation\AbstractRelationAnnotation;
use Drupal\managed\Vendor\Addendum\Annotation;
use Drupal\managed\Discovery\Location;
use Drupal\managed\Annotation\Field\AbstractFieldAnnotation;
use Drupal\managed\Utils\StringUtils;


/**
 * Base class of all annotations that define a fieldable
 * content type.
 */
abstract class AbstractFieldableAnnotation extends Annotation
{
  /**
   * The unique identifier of this entity type.
   *
   * Defaults to the lowercased, underscored class name of the
   * defining class, e.g. "SuperUser" will become "super_user".
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the type.
   *
   * Defaults to the humanized class name.
   *
   * @var string
   */
  protected $label;

  /**
   * A brief description of what this type is for.
   *
   * @var string
   */
  protected $description;

  /**
   * The fields defined by this annotation.
   *
   * @var \Drupal\managed\Annotation\Field\AbstractFieldAnnotation[]
   */
  private $fields = array();

  /**
   * The relations defined by this annotation.
   *
   * @var \Drupal\managed\Annotation\Relation\AbstractRelationAnnotation[]
   */
  private $relations = array();

  /**
   * A list of all discovered form elements.
   *
   * @var \Drupal\managed\Annotation\AbstractFormAnnotation[][]
   */
  private $formElements = array();

  /**
   * The maximum weight of the form elements of this definition.
   *
   * @var int
   */
  private $maxWeight = 0;

  /**
   * The name of the provider of this entity type.
   *
   * @var string
   */
  private $provider;

  /**
   * The name of the entity type class.
   *
   * @var string
   */
  private $class;



  /**
   * Allow this annotation to pull data from the final class reflection.
   *
   * @param \Drupal\managed\Vendor\Addendum\ReflectionAnnotatedClass $reflection
   * @return $this
   */
  public function pullReflectionData($reflection) {
    $this->class = $reflection->getName();

    // Create an id based on the class name if none has been provided
    if (!isset($this->id)) {
      $id = $this->class;
      $lastBackslash = strrpos($id, '\\');
      if ($lastBackslash !== FALSE) {
        $id = substr($id, $lastBackslash + 1);
      }

      $this->id = StringUtils::underscore($id);
    }

    // Create a label based on the id if none has been provided
    if (!isset($this->label)) {
      $this->label = StringUtils::humanize($this->id);
    }

    return $this;
  }


  /**
   * Allow this annotation to pull data from one of the parent class
   * reflections.
   *
   * If this function returns FALSE, the traversal should stop and no further
   * parent classes will be processed.
   *
   * @param \Drupal\managed\Vendor\Addendum\ReflectionAnnotatedClass $reflection
   * @return bool
   */
  public function pullParentClassData($reflection) {
    $className = $reflection->getName();

    /** @var \Drupal\managed\Vendor\Addendum\ReflectionAnnotatedProperty $property */
    foreach ($reflection->getProperties() as $property) {
      $ownerName = $property->getOriginalDeclaringClass()->getName();
      if ($ownerName != $className) {
        continue;
      }

      foreach ($property->getAllAnnotations() as $annotation) {
        if ($annotation instanceof AbstractFieldAnnotation) {
          $annotation->pullReflectionData($property);
          $annotation->pullOwnerData($this);

          $this->formElements[$ownerName][] = $annotation;
          $this->fields[$annotation->getName()] = $annotation;
          break;
        }
        elseif ($annotation instanceof AbstractRelationAnnotation) {
          $annotation->pullReflectionData($property);

          $this->formElements[$ownerName][] = $annotation;
          $this->relations[$annotation->getName()] = $annotation;
          break;
        }
      }
    }

    return true;
  }


  /**
   * Allow the annotation to read data from the scanner location.
   *
   * @param \Drupal\managed\Discovery\Location $location
   *   The location that contains this annotation.
   * @return $this
   */
  public function pullLocationData(Location $location) {
    $this->provider = $location->getExtension()->getName();
    return $this;
  }


  /**
   * Allow all fields to fetch data from the final owner definition.
   *
   * @return $this
   */
  public function pullFieldData() {
    $this->pullFormWeights();

    foreach ($this->fields as $field) {
      $field->pullOwnerData($this);
    }

    return $this;
  }


  /**
   * Apply the weight data to all form elements.
   *
   * @param int $baseWeight
   * @return $this
   */
  public function pullFormWeights($baseWeight = 0) {
    if (!isset($this->formElements)) {
      return $this;
    }

    array_reverse($this->formElements);

    /** @var \Drupal\managed\Annotation\AbstractFormAnnotation[] $elements */
    foreach ($this->formElements as $elements) {
      foreach ($elements as $element) {
        $element->setWeight($baseWeight, FALSE);
        $baseWeight += 10;
      }
    }

    $this->maxWeight = $baseWeight;
    unset($this->formElements);

    return $this;
  }


  /**
   * Test whether this entity is translatable.
   *
   * @return bool
   */
  public function isTranslatable() {
    return false;
  }


  /**
   * Test whether this entity is revisionable.
   *
   * @return bool
   */
  public function isRevisionable() {
    return false;
  }


  /**
   * Return the unique identifier of this entity type.
   *
   * Align function name to \Drupal\Core\Entity\EntityType::id()
   *
   * @return string
   */
  public function id() {
    return $this->id;
  }


  /**
   * Return the extra field data for this definition.
   *
   * @param array $data
   * @return array
   */
  public function getExtraFieldData($data = array()) {
    foreach ($this->relations as $relation) {
      $data = array_merge_recursive($data, $relation->getExtraFieldData());
    }

    return $data;
  }


  /**
   * Return the human-readable name of the type.
   *
   * @return string
   */
  public function getLabel() {
    return $this->label;
  }


  /**
   * Return a brief description of what this type is for.
   *
   * @return string
   */
  public function getDescription() {
    return $this->description;
  }


  /**
   * Return the name of the entity type class.
   *
   * @return string
   */
  public function getClass() {
    return $this->class;
  }


  /**
   * Return all fields attached to this annotation.
   *
   * @return \Drupal\managed\Annotation\Field\AbstractFieldAnnotation[]
   */
  public function getFields() {
    return $this->fields;
  }


  /**
   * Return all relations attached to this annotation.
   *
   * @return \Drupal\managed\Annotation\Relation\AbstractRelationAnnotation[]
   */
  public function getRelations() {
    return $this->relations;
  }


  /**
   * The maximum weight of the form elements of this definition.
   *
   * @return int
   */
  public function getMaxWeight() {
    return $this->maxWeight;
  }


  /**
   * Return the name of the entity type class.
   *
   * @return string
   */
  public function getProvider() {
    return $this->provider;
  }
}
