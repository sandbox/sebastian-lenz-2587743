<?php

namespace Drupal\managed\Annotation\Behaviour;

use Drupal\managed\Behaviour\ListBuilder\ColumnDefinitionInterface;


/**
 * Creates an admin interface located in a tab within the `Content` section.
 *
 * @ManagedAnnotation(id="ContentAdminBehaviour")
 */
class ContentAdminBehaviourAnnotation extends AbstractBehaviourAnnotation implements ColumnDefinitionInterface
{
  /**
   * The name of the default administrative permission.
   *
   * Defaults to 'administer @ENTITY_TYPE_ID'.
   *
   * @var string
   */
  protected $adminPermission;

  /**
   * A list of fields that should be displayed as columns by the list builder.
   *
   * Don't forget to include the column '@operations' to show a column
   * containing the action button.
   *
   * @var string[]
   */
  protected $listColumns = array('@label', '@bundle', '@langcode', '@operations');



  /**
   * Return the name of the class implementing the behaviour.
   *
   * @return string
   */
  public function getBehaviourClass() {
    return 'Drupal\managed\Behaviour\ContentAdminBehaviour';
  }


  /**
   * Return the name of the admin permission.
   *
   * @param string $entityTypeID
   * @return string
   */
  public function getAdminPermission($entityTypeID) {
    if (isset($this->adminPermission)) {
      return $this->adminPermission;
    } else {
      return 'administer ' . $entityTypeID;
    }
  }


  /**
   * Allow this behaviour to manipulate the given entity definition.
   *
   * @param array $definition
   */
  public function onAlterEntityDefinition(&$definition) {
    $definition['admin_permission'] = $this->getAdminPermission($definition['id']);
  }


  /**
   * Return a list of fields that should be displayed as columns by
   * the list builder.
   *
   * @return string[]
   */
  public function getColumnDefinitions() {
    return $this->listColumns;
  }
}
