<?php

namespace Drupal\managed\Annotation\Behaviour;

use Drupal\managed\Core\ManagedEntityType;


/**
 * Creates an admin interface located in a tab within the `Content` section
 * and sets up frontend urls for each entity.
 *
 * @ManagedAnnotation(id="ContentBehaviour")
 */
class ContentBehaviourAnnotation extends ContentAdminBehaviourAnnotation
{
  /**
   * Return the name of the class implementing the behaviour.
   *
   * @return string
   */
  public function getBehaviourClass() {
    return 'Drupal\managed\Behaviour\ContentBehaviour';
  }
}
