<?php

namespace Drupal\managed\Annotation\Behaviour;

use Drupal\managed\Vendor\Addendum\Annotation;


/**
 * Base class of all annotations describing entity behaviours.
 */
abstract class AbstractBehaviourAnnotation extends Annotation
{
  /**
   * Return the name of the class implementing the behaviour.
   *
   * @return string
   */
  abstract public function getBehaviourClass();


  /**
   * Allow this behaviour to manipulate the given entity definition.
   *
   * @param array $definition
   */
  public function onAlterEntityDefinition(&$definition) { }
}
