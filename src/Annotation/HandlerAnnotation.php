<?php

namespace Drupal\managed\Annotation;

use Drupal\managed\Vendor\Addendum\Annotation;
use Drupal\managed\Vendor\Addendum\ReflectionAnnotatedClass;


/**
 * A decorator for classes that wants to register themselves as a handler
 * for a particular entity type.
 *
 * @ManagedAnnotation(id="Handler")
 */
class HandlerAnnotation extends Annotation
{
  /**
   * The name of the class that defined this handler.
   *
   * @var string
   */
  private $class;

  /**
   * Does the class implement Drupal\Core\Form\FormInterface?
   *
   * @var bool
   */
  private $isForm;

  /**
   * The entity type id or the name of the entity type class this handler targets.
   *
   * @var string
   */
  protected $entity;

  /**
   * The type of the handler this class defines.
   *
   * @var string
   */
  protected $type;



  /**
   * Initialize this annotation. Called by the finalized constructor of the
   * base class `Drupal\managed\Vendor\Addendum\Annotation`.
   *
   * @param ReflectionAnnotatedClass $reflection
   * @throws \Exception
   */
  protected function checkConstraints($reflection) {
    if (!$reflection instanceof ReflectionAnnotatedClass) {
      throw new \Exception('The annotation `@Handler` must decorate a class.');
    }

    $this->class = $reflection->getName();
    $this->isForm = $reflection->implementsInterface('Drupal\Core\Form\FormInterface');
  }


  /**
   * Return whether the class implements Drupal\Core\Form\FormInterface.
   *
   * @return bool
   */
  public function isForm() {
    return $this->isForm;
  }


  /**
   * Return the class name of the handler.
   *
   * @return string
   */
  public function getClass() {
    return $this->class;
  }


  /**
   * Return the entity type id or the entity type class this handler is targeting.
   *
   * @return string
   */
  public function getEntityTypeOrClass() {
    return $this->entity;
  }


  /**
   * Return the type of the handler this class defines.
   *
   * @return string
   */
  public function getHandlerType() {
    return $this->type;
  }
}
