<?php

namespace Drupal\managed\Annotation;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\managed\Annotation\Field\ReferenceFieldAnnotation;
use Drupal\managed\Annotation\Field\TimestampFieldAnnotation;
use Drupal\managed\Vendor\Addendum\ReflectionAnnotatedClass;
use Drupal\managed\Console\RouteSubscriber;
use Drupal\managed\Core\ManagedEntityType;
use Drupal\managed\Annotation\Behaviour\AbstractBehaviourAnnotation;
use Drupal\managed\Core\ManagedEntityTypeDefinitionInterface;
use Drupal\managed\Utils\StringUtils;


/**
 * A decorator for classes that should emmit an entity type.
 *
 * Classes using this decorator must be a subclass of
 * \Drupal\managed\Entity. This decorator has no required arguments,
 * see the comments on the properties to find out what they will be set to.
 *
 * @ManagedAnnotation(id="Entity")
 */
class EntityAnnotation extends AbstractFieldableAnnotation implements ManagedEntityTypeDefinitionInterface
{
  /**
   * The human-readable name of the entity bundles.
   *
   * Defaults to the label with ` type` appended.
   *
   * @var string
   */
  protected $bundleLabel;

  /**
   * The plural version of the label.
   *
   * Defaults to the pluralized version of the label.
   *
   * @var string
   */
  protected $pluralLabel;

  /**
   * The name of the entity type's base table.
   *
   * Defaults to the entity id.
   *
   * @var string
   */
  protected $table;

  /**
   * A list of all bundles belonging to this entity.
   *
   * @var \Drupal\managed\Annotation\BundleAnnotation[]
   */
  private $bundles;

  /**
   * The behaviour definition attached to this entity.
   *
   * @var \Drupal\managed\Annotation\Behaviour\AbstractBehaviourAnnotation
   */
  private $behaviour;

  /**
   * The handler mapping of this entity type.
   *
   * @var array
   */
  private $handlers = array();



  /**
   * Initialize this annotation. Called by the finalized constructor of the
   * base class `Drupal\managed\Vendor\Addendum\Annotation`.
   *
   * @param ReflectionAnnotatedClass $reflection
   * @throws \Exception
   */
  protected function checkConstraints($reflection) {
    parent::checkConstraints($reflection);

    if (!$reflection instanceof ReflectionAnnotatedClass) {
      throw new \Exception('The annotation `@Entity` must decorate a class.');
    }

    if (!$reflection->isSubclassOf('Drupal\managed\Entity')) {
      throw new \Exception('The annotation `@Entity` must decorate a subclass of `Drupal\managed\Entity`.');
    }
  }


  /**
   * Allow this annotation to pull data from the final class reflection.
   *
   * @param \Drupal\managed\Vendor\Addendum\ReflectionAnnotatedClass $reflection
   * @return $this
   */
  public function pullReflectionData($reflection) {
    parent::pullReflectionData($reflection);

    if (!isset($this->table)) {
      $this->table = $this->id();
    }

    if (!isset($this->pluralLabel)) {
      $this->pluralLabel = StringUtils::pluralize($this->label);
    }

    if (!isset($this->bundleLabel)) {
      $this->bundleLabel = $this->label . ' type';
    }

    foreach ($reflection->getAllAnnotations() as $annotation) {
      if (!($annotation instanceof AbstractBehaviourAnnotation)) {
        continue;
      }

      $this->behaviour = $annotation;
      break;
    }

    return $this;
  }


  /**
   * Allow this annotation to pull data from one of the parent class
   * reflections.
   *
   * If this function returns FALSE, the traversal should stop and no further
   * parent classes will be processed.
   *
   * @param \Drupal\managed\Vendor\Addendum\ReflectionAnnotatedClass $reflection
   * @return bool
   */
  public function pullParentClassData($reflection) {
    if ($reflection->getName() == 'Drupal\managed\Entity') {
      return false;
    }

    return parent::pullParentClassData($reflection);
  }


  /**
   * Check for bundles that belong to this entity and add them.
   *
   * @param \Drupal\managed\Annotation\BundleAnnotation[] $bundles
   * @return $this
   */
  public function pullBundles($bundles) {
    foreach ($bundles as $bundle) {
      if ($bundle->getEntityClass() == $this->getClass()) {
        $this->bundles[$bundle->id()] = $bundle;
      }
    }

    return $this;
  }


  /**
   * Check for handlers that belong to this entity and add them.
   *
   * @param \Drupal\managed\Annotation\HandlerAnnotation[] $handlers
   * @return $this
   */
  public function pullHandlers($handlers) {
    foreach ($handlers as $handler) {
      $target = $handler->getEntityTypeOrClass();
      if ($target == $this->id || $target == $this->getClass()) {
        if ($handler->isForm()) {
          $this->handlers['form'][$handler->getHandlerType()] = $handler->getClass();
        } else {
          $this->handlers[$handler->getHandlerType()] = $handler->getClass();
        }
      }
    }

    return $this;
  }


  /**
   * Allow all fields to fetch data from the final owner definition.
   *
   * @return $this
   */
  public function pullFieldData() {
    parent::pullFieldData();
    if (!isset($this->bundles)) {
      return $this;
    }

    $weight = $this->getMaxWeight() + 10;

    foreach ($this->bundles as $bundle) {
      $bundle->pullFormWeights($weight);

      foreach ($bundle->getFields() as $field) {
        $field->pullOwnerData($this);
      }
    }

    return $this;
  }


  /**
   * Test whether this entity has a field defining the given entity key.
   *
   * @param string $key
   * @return bool
   */
  private function hasKey($key) {
    foreach ($this->getFields() as $field) {
      if ($field->getEntityKey() == $key) {
        return true;
      }
    }

    return false;
  }


  /**
   * Test whether this entity is translatable.
   *
   * An entity is considered translatable if it contains a field
   * defining the `langcode` entity key.
   *
   * @return bool
   */
  public function isTranslatable() {
    return $this->hasKey('langcode');
  }


  /**
   * Test whether this entity is revisionable.
   *
   * An entity is considered revisionable if it contains a field
   * defining the `revision` entity key.
   *
   * @return bool
   */
  public function isRevisionable() {
    return $this->hasKey('revision');
  }


  /**
   * Return the plural version of the label.
   *
   * @return string
   */
  public function getPluralLabel() {
    return $this->pluralLabel;
  }


  /**
   * Return a map of all entity keys and their corresponding field names.
   *
   * @return array
   */
  private function getEntityKeys() {
    $entityKeys = array();

    foreach ($this->getFields() as $field) {
      if (!$field->isEntityKey()) {
        continue;
      }

      $key = $field->getEntityKey();
      if (isset($entityKeys[$key])) {
        drupal_set_message(t(
          'The entity key `@key` is provided twice for entity `@entity`.',
          array('@key' => $key, '@entity' => $this->id())
        ), 'error');
        continue;
      }

      switch ($key) {
        case 'revision_uid':
          if ($field instanceof ReferenceFieldAnnotation && $field->getTargetEntityTypeID() == 'user') {
            break;
          } else {
            drupal_set_message(t(
              'The field `@field` of the entity `@entity` using the entity key `revision_uid` must be a reference to a user.',
              array('@field' => $field->getName(), '@entity' => $this->id())
            ), 'error');
            continue;
          }
        case 'revision_timestamp':
          if ($field instanceof TimestampFieldAnnotation) {
            break;
          } else {
            drupal_set_message(t(
              'The field `@field` of the entity `@entity` using the entity key `revision_timestamp` must contain a timestamp.',
              array('@field' => $field->getName(), '@entity' => $this->id())
            ), 'error');
            continue;
          }
      }

      $entityKeys[$key] = $field->getName();
    }

    return $entityKeys;
  }


  /**
   * Generate the definition data for the entity type instance.
   *
   * @return array
   */
  public function getEntityDefinition() {
    if (is_array($this->bundles) && $this->hasKey('bundle')) {
      $this->handlers += array(
        'storage' => 'Drupal\managed\Core\Storage\ManagedEntityStorage'
      );
    }

    $definition = array(
      'id'                  => $this->id,
      'class'               => $this->getClass(),
      'provider'            => $this->getProvider(),
      'label'               => new TranslatableMarkup($this->label),
      'bundle_label'        => new TranslatableMarkup($this->bundleLabel),
      'handlers'            => $this->handlers,
      'base_table'          => $this->table,
      'data_table'          => $this->table . '_field_data',
      'revision_table'      => $this->table . '_revision',
      'revision_data_table' => $this->table . '_field_revision',
      'translatable'        => $this->isTranslatable(),
      'entity_keys'         => $this->getEntityKeys(),
      'field_ui_base_route' => RouteSubscriber::getEntityRouteName($this->id())
    );

    if (!is_null($this->behaviour)) {
      $this->behaviour->onAlterEntityDefinition($definition);
    }

    return $definition;
  }


  /**
   * Generate the entity type definition declared by this annotation.
   *
   * @return \Drupal\managed\Core\ManagedEntityType
   */
  public function getEntityType() {
    $definition = $this->getEntityDefinition();

    $entityType = new ManagedEntityType($definition);
    $entityType->pullDefinitionData($this);

    if (!is_null($this->behaviour)) {
      $behaviourClass = $this->behaviour->getBehaviourClass();
      $behaviour = new $behaviourClass($entityType);
      $behaviour->initializeEntityType();
    }

    return $entityType;
  }


  /**
   * Return a list of all bundles belonging to this entity.
   *
   * @return \Drupal\managed\Annotation\BundleAnnotation[]
   */
  public function getBundles() {
    return $this->bundles;
  }


  /**
   * Return the behaviour definition attached to this entity.
   *
   * @return \Drupal\managed\Annotation\Behaviour\AbstractBehaviourAnnotation
   */
  public function getBehaviour() {
    return $this->behaviour;
  }
}
