<?php

namespace Drupal\managed\Annotation;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\managed\Vendor\Addendum\ReflectionAnnotatedClass;


/**
 * A decorator for classes that define an entity bundle.
 *
 * The related entity type must be within the base classes of the
 * defining class. Make sure the entity class owns a field decorated with
 * `@EnityKey('bundle')` to make bundles work.
 *
 * @ManagedAnnotation(id="Bundle")
 */
class BundleAnnotation extends AbstractBundleAnnotation
{
  /**
   * The name of the parent class defining the entity this bundle belongs to.
   *
   * @var string
   */
  private $entityClass;



  /**
   * Initialize this annotation. Called by the finalized constructor of the
   * base class `Drupal\managed\Vendor\Addendum\Annotation`.
   *
   * @param ReflectionAnnotatedClass $reflection
   * @throws \Exception
   */
  protected function checkConstraints($reflection) {
    if (!$reflection instanceof ReflectionAnnotatedClass) {
      throw new \Exception('The annotation `@Bundle` must decorate a class.');
    }

    if (!$reflection->isSubclassOf('Drupal\managed\Entity')) {
      throw new \Exception('The annotation `@Bundle` must decorate a subclass of `Drupal\managed\Entity`.');
    }

    parent::checkConstraints($reflection);
  }


  /**
   * Allow this annotation to pull data from one of the parent class
   * reflections.
   *
   * If this function returns FALSE, the traversal should stop and no further
   * parent classes will be processed.
   *
   * @param \Drupal\managed\Vendor\Addendum\ReflectionAnnotatedClass $reflection
   * @return bool
   */
  public function pullParentClassData($reflection) {
    if ($reflection->getName() == 'Drupal\managed\Entity') {
      return false;
    }

    if ($reflection->hasAnnotation('Entity')) {
      $this->entityClass = $reflection->getName();
      return false;
    }

    return parent::pullParentClassData($reflection);
  }


  /**
   * Return the name of the parent class defining the entity this bundle
   * belongs to.
   *
   * @return string
   */
  public function getEntityClass() {
    return $this->entityClass;
  }


  /**
   * Return the bundle info.
   *
   * @return array
   */
  public function getInfo() {
    return array(
      'label' => new TranslatableMarkup($this->label)
    );
  }
}
