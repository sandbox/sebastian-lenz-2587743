<?php

namespace Drupal\managed\Annotation\Drupal;

use Drupal\Component\Annotation\Plugin;


/**
 * An annotation that exposes annotations to the module `managed`.
 *
 * The module `managed` uses a different annotation parser than Drupal
 * core, therefore annotations are not exchangeable. Annotations for this
 * module must always be a subclass of `Drupal\managed\Vendor\Addendum\Annotation`.
 *
 * @Annotation
 */
class ManagedAnnotation extends Plugin
{
}
