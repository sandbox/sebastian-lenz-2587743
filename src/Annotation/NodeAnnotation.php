<?php

namespace Drupal\managed\Annotation;

use Drupal\managed\Vendor\Addendum\ReflectionAnnotatedClass;


/**
 * A decorator for classes that should emmit a node type.
 *
 * Classes using this decorator must be a subclass of
 * \Drupal\managed\Node. This decorator has no required arguments,
 * see the comments on the properties to find out what they will be set to.
 *
 * @ManagedAnnotation(id="Node")
 */
class NodeAnnotation extends AbstractBundleAnnotation
{
  /**
   * The handler mapping of this entity type.
   *
   * @var array
   */
  private $handlers = array();



  /**
   * Initialize this annotation. Called by the finalized constructor of the
   * base class `Drupal\managed\Vendor\Addendum\Annotation`.
   *
   * @param ReflectionAnnotatedClass $reflection
   * @throws \Exception
   */
  protected function checkConstraints($reflection) {
    parent::checkConstraints($reflection);

    if (!$reflection instanceof ReflectionAnnotatedClass) {
      throw new \Exception('The annotation `@Node` must decorate a class.');
    }

    if (!$reflection->isSubclassOf('Drupal\managed\Node')) {
      throw new \Exception('The annotation `@Node` must decorate a subclass of `Drupal\managed\Node`.');
    }
  }


  /**
   * Allow this annotation to pull data from one of the parent class
   * reflections.
   *
   * If this function returns FALSE, the traversal should stop and no further
   * parent classes will be processed.
   *
   * @param \Drupal\managed\Vendor\Addendum\ReflectionAnnotatedClass $reflection
   * @return bool
   */
  public function pullParentClassData($reflection) {
    if ($reflection->getName() == 'Drupal\managed\Node') {
      return false;
    }

    return parent::pullParentClassData($reflection);
  }


  /**
   * Check for handlers that belong to this entity and add them.
   *
   * @param \Drupal\managed\Annotation\HandlerAnnotation[] $handlers
   * @return $this
   */
  public function pullHandlers($handlers) {
    foreach ($handlers as $handler) {
      $target = $handler->getEntityTypeOrClass();
      if ($target == 'node') {
        if ($handler->isForm()) {
          $this->handlers['form'][$handler->getHandlerType()] = $handler->getClass();
        } else {
          $this->handlers[$handler->getHandlerType()] = $handler->getClass();
        }
      }
    }

    return $this;
  }


  /**
   * Return the node type definition.
   *
   * @return array
   */
  public function getDefinition() {
    return array(
      'type'        => $this->id,
      'name'        => $this->label,
      'description' => $this->description
    );
  }
}
