<?php

namespace Drupal\managed\Annotation;


/**
 * Base class of all annotations declaring a bundle.
 */
abstract class AbstractBundleAnnotation extends AbstractFieldableAnnotation
{
}
