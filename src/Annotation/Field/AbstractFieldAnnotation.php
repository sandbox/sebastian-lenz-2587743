<?php

namespace Drupal\managed\Annotation\Field;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\managed\Annotation\AbstractFormAnnotation;
use Drupal\managed\Annotation\AbstractFieldableAnnotation;


/**
 * Base class of all decorators emitting a field.
 *
 * Field annotations must be placed on class properties.
 */
abstract class AbstractFieldAnnotation extends AbstractFormAnnotation
{
  /**
   * The desired view display of this field.
   *
   * Defaults to `auto` which will selected the default view display
   * suitable for the current field. Set to `none` if you wish to hide
   * the field. Individual fields may introduce additional view modes.
   *
   * @var string
   */
  protected $viewType = 'auto';

  /**
   * Set whether this field can be edited in the `Manage display` section
   * of the backend console or not.
   *
   * @var bool
   */
  protected $viewConfigurable = true;

  /**
   * The desired form display of this field.
   *
   * Defaults to `auto` which will selected the default form display
   * suitable for the current field. Set to `none` if you wish to omit
   * the field. Set to `hidden` if you wish to create a hidden form field.
   * Individual fields may introduce additional form modes.
   *
   * @var string
   */
  protected $formType = 'auto';

  /**
   * Set whether this field can be edited in the `Manage form` section
   * of the backend console or not.
   *
   * @var bool
   */
  protected $formConfigurable = true;

  /**
   * Should this field be stored within the payload of the entity?
   *
   * @var bool
   */
  protected $isPayload;

  /**
   * Mark this field as being read only.
   *
   * @var bool
   */
  protected $isReadOnly;

  /**
   * Mark this field as being manadatory in forms.
   *
   * @var bool
   */
  protected $isRequired;

  /**
   * Mark this field as being able to be translated.
   *
   * @var bool
   */
  protected $isTranslatable;

  /**
   * @varMark this field to be stored with each revision.
   *
   * @var bool
   */
  protected $isRevisionable;

  /**
   * The entity key this field defines.
   *
   * Pulled from the `@EntityKey` annotation.
   *
   * @var string
   */
  protected $entityKey;



  /**
   * Allow this field to pull data from the owning fieldable annotation.
   *
   * @param \Drupal\managed\Annotation\AbstractFieldableAnnotation
   * @return $this
   */
  public function pullOwnerData(AbstractFieldableAnnotation $owner) {
    if (!$owner->isTranslatable()) {
      $this->isTranslatable = FALSE;
    }

    if (!$owner->isRevisionable()) {
      $this->isRevisionable = FALSE;
    }

    return $this;
  }


  /**
   * Allow this field to pull data from the final property reflection.
   *
   * @param \Drupal\managed\Vendor\Addendum\ReflectionAnnotatedProperty $reflection
   */
  public function pullReflectionData($reflection) {
    parent::pullReflectionData($reflection);

    if (!isset($this->entityKey)) {
      $this->entityKey = $this->pullEntityKey($reflection);
    }
  }


  /**
   * Try to find an entity key definition on the given reflection.
   *
   * @param \Drupal\managed\Vendor\Addendum\ReflectionAnnotatedProperty $reflection
   * @return string|null
   */
  protected function pullEntityKey($reflection) {
    $entityKey = $reflection->getAnnotation('EntityKey');

    if ($entityKey !== FALSE) {
      return $entityKey->value;
    } else {
      return NULL;
    }
  }


  /**
   * Helper function for quickly setting up a field definition with
   *t he default values already applied.
   *
   * @param string $type
   * @return \Drupal\Core\Field\BaseFieldDefinition
   */
  protected function createBaseDefinition($type) {
    /** @var BaseFieldDefinition $definition */
    $definition = BaseFieldDefinition::create($type);
    $definition->setLabel($this->label);

    if (isset($this->description)) {
      $definition->setDescription($this->description);
    }

    if (isset($this->isReadOnly)) {
      $definition->setReadOnly($this->isReadOnly);
    }

    if (isset($this->isRequired)) {
      $definition->setRequired($this->isRequired);
    }

    if (isset($this->isRevisionable)) {
      $definition->setRevisionable($this->isRevisionable);
    }

    if (isset($this->isTranslatable)) {
      $definition->setTranslatable($this->isTranslatable);
    }

    if (isset($this->isPayload) && $this->isPayload) {
      $definition->setComputed(true);
    }

    return $definition;
  }


  /**
   * Apply the form and view displays settings to the given field definition.
   *
   * @param \Drupal\Core\Field\BaseFieldDefinition $definition
   */
  protected function applyDisplayOptions(BaseFieldDefinition $definition) {
    $defaults = array(
      'weight' => $this->weight
    );

    if ($this->viewType != 'none') {
      $options = $this->getViewDisplayOptions();

      if (!is_null($options)) {
        $definition->setDisplayOptions('view', $options + $defaults);
        $definition->setDisplayConfigurable('view', $this->viewConfigurable);
      }
    }

    if ($this->formType == 'hidden') {
      $options = array(
        'type' => 'hidden'
      );

      $definition->setDisplayOptions('form', $options + $defaults);
    }
    elseif ($this->formType != 'none') {
      $options = $this->getFormDisplayOptions();

      if (!is_null($options)) {
        $definition->setDisplayOptions('form', $options + $defaults);
        $definition->setDisplayConfigurable('form', $this->formConfigurable);
      }
    }
  }


  /**
   * Test whether this field defines an entity key.
   *
   * @return bool
   */
  public function isEntityKey() {
    return !empty($this->entityKey);
  }


  /**
   * Return the definition of this field.
   *
   * @return \Drupal\Core\Field\BaseFieldDefinition
   */
  public function getDefinition() {
    $definition = $this->getBaseDefinition();

    $this->applyDisplayOptions($definition);

    return $definition;
  }


  /**
   * Return the base definition of this field. The base definition should
   * not contain any display or form settings.
   *
   * @return \Drupal\Core\Field\BaseFieldDefinition
   */
  abstract protected function getBaseDefinition();


  /**
   * Return the display options for the `view` display of this field.
   *
   * @return array|null
   */
  protected function getViewDisplayOptions() {
    return NULL;
  }


  /**
   * Return the display options for the `form` display of this field.
   *
   * @return array|null
   */
  protected function getFormDisplayOptions() {
    return NULL;
  }


  /**
   * Return the entity key this field defines.
   *
   * @return string
   */
  public function getEntityKey() {
    return $this->entityKey;
  }
}
