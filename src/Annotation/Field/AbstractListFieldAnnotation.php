<?php

namespace Drupal\managed\Annotation\Field;

use Drupal\managed\Annotation\OptionAnnotation;


/**
 * Base class of all fields defining a list field.
 */
abstract class AbstractListFieldAnnotation extends AbstractFieldAnnotation
{
  /**
   * The type of list field that should be created.
   *
   * Defaults to `auto` which will select the type based upon the given
   * value mapping. Set to `string`, `integer` or `float` to explicitly
   * set the storage field type.
   *
   * @var string
   */
  protected $type = 'auto';

  /**
   * The list of available options.
   *
   * @var array
   */
  protected $allowedValues;

  /**
   * The name of a function that should be called to retrieve the
   * available options.
   *
   * When providing a function you should provide the type setting. The
   * function must have the following signature:
   *   function(FieldStorageDefinitionInterface $definition, FieldableEntityInterface $entity, &$cacheable)
   *
   * @var string
   */
  protected $allowedValuesFunction;



  /**
   * Allow this field to pull data from the final property reflection.
   *
   * @param \Drupal\managed\Vendor\Addendum\ReflectionAnnotatedProperty $reflection
   */
  public function pullReflectionData($reflection) {
    parent::pullReflectionData($reflection);

    $this->pullOptionAnnotations($reflection);
    $this->pullDefaultValuesFunction($reflection);
  }


  /**
   * Search for all option annotations and copy over the defined values.
   *
   * @param \Drupal\managed\Vendor\Addendum\ReflectionAnnotatedProperty $reflection
   */
  protected function pullOptionAnnotations($reflection) {
    $allowedValues = is_array($this->allowedValues) ? $this->allowedValues : array();
    foreach ($reflection->getAllAnnotations() as $annotation) {
      if (!($annotation instanceof OptionAnnotation)) {
        continue;
      }

      $value = $annotation->getValue();
      $label = $annotation->getLabel();
      $allowedValues[$value] = $label;
    }

    if (count($allowedValues) > 0) {
      $this->allowedValues = $allowedValues;
    }
  }


  /**
   * Check whether there is a function named `get{FIELD_NAME}AvailableValues()`
   * in the defining class and if so add it as callback function.
   *
   * @param \Drupal\managed\Vendor\Addendum\ReflectionAnnotatedProperty $reflection
   */
  protected function pullDefaultValuesFunction($reflection) {
    if (isset($this->allowedValuesFunction)) {
      return;
    }

    $class = $reflection->getDeclaringClass();
    $name  = managed_get_available_values_function_name($this->getName());

    if ($class->hasMethod($name)) {
      $method = $class->getMethod($name);

      if ($method->isStatic()) {
        $this->allowedValuesFunction = array($class->getName(), $name);
      } else {
        $this->allowedValuesFunction = 'managed_get_available_values_from_instance';
      }
    }
  }


  /**
   * Return the base definition of this field. The base definition should
   * not contain any display or form settings.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface
   */
  protected function getBaseDefinition() {
    $type = $this->getType();
    $definition = $this->createBaseDefinition($type);

    if (isset($this->allowedValues) && is_array($this->allowedValues)) {
      $definition->setSetting('allowed_values', $this->allowedValues);
    } else if (isset($this->allowedValuesFunction)) {
      $definition->setSetting('allowed_values_function', $this->allowedValuesFunction);
    }

    return $definition;
  }


  /**
   * Return the display options for the `form` display of this field.
   *
   * @return array|null
   */
  protected function getViewDisplayOptions() {
    return array(
      'type' => 'list_default',
      'label' => 'hidden'
    );
  }


  /**
   * Return the desired field type.
   *
   * @return string
   */
  protected function getType() {
    switch ($this->type) {
      case 'string':  return 'list_string';
      case 'integer': return 'list_integer';
      case 'float':   return 'list_float';
    }

    if (isset($this->allowedValues)
      && is_array($this->allowedValues)
      && count($this->allowedValues) > 0)
    {
      $count      = count($this->allowedValues);
      $countInt   = 0;
      $countFloat = 0;

      foreach ($this->allowedValues as $value => $label) {
        if (is_int($value)) {
          $countInt++;
        } else if (is_float($value)) {
          $countFloat++;
        }
      }

      if ($countInt == $count) {
        return 'list_integer';
      } elseif ($countInt + $countFloat == $count) {
        return 'list_float';
      }
    }

    return 'list_string';
  }
}
