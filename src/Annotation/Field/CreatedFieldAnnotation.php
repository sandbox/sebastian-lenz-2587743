<?php

namespace Drupal\managed\Annotation\Field;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\managed\Annotation\AbstractFieldableAnnotation;


/**
 * Defines a field that stores the revision id of an entity.
 *
 * The entity key property of this annotation will be set to 'revision'.
 *
 * @ManagedAnnotation(id="CreatedField")
 */
class CreatedFieldAnnotation extends TimestampFieldAnnotation
{
  /**
   * Return the base definition of this field. The base definition should
   * not contain any display or form settings.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface
   */
  protected function getBaseDefinition() {
    return $this->createBaseDefinition('created');
  }
}
