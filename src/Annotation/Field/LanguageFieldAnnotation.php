<?php

namespace Drupal\managed\Annotation\Field;

use Drupal\managed\Annotation\AbstractFieldableAnnotation;


/**
 * Defines a field used to store the bundle type of an entity.
 *
 * The entity key property of this annotation will be set to 'langcode'.
 *
 * @ManagedAnnotation(id="LanguageField")
 */
class LanguageFieldAnnotation extends AbstractFieldAnnotation
{
  /**
   * Try to find an entity key definition on the given reflection.
   *
   * @param \Drupal\managed\Vendor\Addendum\ReflectionAnnotatedProperty $reflection
   * @return string|null
   */
  protected function pullEntityKey($reflection) {
    return 'langcode';
  }


  /**
   * Allow this field to pull data from the owning fieldable annotation.
   *
   * @param \Drupal\managed\Annotation\AbstractFieldableAnnotation $owner
   * @return $this
   */
  public function pullOwnerData(AbstractFieldableAnnotation $owner) {
    $this->isTranslatable = TRUE;

    if (!isset($this->isRevisionable)) {
      $this->isRevisionable = FALSE;
    }

    return $this;
  }


  /**
   * Return the base definition of this field. The base definition should
   * not contain any display or form settings.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface
   */
  function getBaseDefinition() {
    return $this->createBaseDefinition('language');
  }


  /**
   * Return the display options for the `form` display of this field.
   *
   * @return array|null
   */
  protected function getFormDisplayOptions() {
    return array(
      'type' => 'language_select'
    );
  }
}
