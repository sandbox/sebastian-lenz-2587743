<?php

namespace Drupal\managed\Annotation\Field;

use Drupal\managed\Annotation\AbstractFieldableAnnotation;


/**
 * Defines an image upload field.
 *
 * @ManagedAnnotation(id="ImageField")
 */
class ImageFieldAnnotation extends AbstractFileFieldAnnotation
{
  /**
   * The maximum allowed image size expressed as WIDTH×HEIGHT (e.g. 640×480).
   *
   * Leave blank for no restriction. If a larger image is uploaded, it will be resized
   * to reflect the given width and height.
   *
   * @var string
   */
  protected $maxResolution;

  /**
   * The minimum allowed image size expressed as WIDTH×HEIGHT (e.g. 640×480).
   *
   * Leave blank for no restriction. If a smaller image is uploaded, it will be rejected.
   *
   * @var string
   */
  protected $minResolution;

  /**
   * Whether the image field should expose an alt field.
   *
   * The alt attribute may be used by search engines, screen readers, and when the image cannot be loaded.
   * Enabling this field is recommended. Defaults to TRUE in \Drupal\image\Plugin\Field\FieldType\ImageItem.
   *
   * @var bool
   */
  protected $hasAlt;

  /**
   * Whether the image field should expose a title field.
   *
   * The title attribute is used as a tooltip when the mouse hovers over the image. Enabling this
   * field is not recommended as it can cause problems with screen readers. Defaults to FALSE
   * in \Drupal\image\Plugin\Field\FieldType\ImageItem.
   *
   * @var bool
   */
  protected $hasTitle;

  /**
   * Whether the field `Alt` should be required.
   *
   * Making this field required is recommended. Defaults to TRUE in \Drupal\image\Plugin\Field\FieldType\ImageItem.
   *
   * @var bool
   */
  protected $isAltRequired;


  /**
   * Whether the field `Title` should be required.
   *
   * Defaults to FALSE in \Drupal\image\Plugin\Field\FieldType\ImageItem.
   *
   * @var bool
   */
  protected $isTitleRequired;



  /**
   * Return the base definition of this field. The base definition should
   * not contain any display or form settings.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface
   */
  protected function getBaseDefinition() {
    $definition = parent::getBaseDefinition('image');

    if (isset($this->hasAlt)) {
      $definition->setSetting('alt_field', $this->hasAlt);
    }

    if (isset($this->isAltRequired)) {
      $definition->setSetting('alt_field_required', $this->isAltRequired);
    }

    if (isset($this->hasTitle)) {
      $definition->setSetting('title_field', $this->hasTitle);
    }

    if (isset($this->isTitleRequired)) {
      $definition->setSetting('title_field_required', $this->isTitleRequired);
    }

    if (isset($this->maxResolution)) {
      $definition->setSetting('max_resolution', $this->maxResolution);
    }

    if (isset($this->minResolution)) {
      $definition->setSetting('min_resolution', $this->minResolution);
    }

    return $definition;
  }


  /**
   * Return the display options for the `view` display of this field.
   *
   * @return array|null
   */
  protected function getViewDisplayOptions() {
    return array(
      'type' => 'image'
    );
  }


  /**
   * Return the display options for the `form` display of this field.
   *
   * @return array|null
   */
  protected function getFormDisplayOptions() {
    return array(
      'type' => 'image_image',
    );
  }
}
