<?php

namespace Drupal\managed\Annotation\Field;

use Drupal\managed\Annotation\AbstractFieldableAnnotation;


/**
 * Base class of all file upload fields.
 */
class AbstractFileFieldAnnotation extends AbstractFieldAnnotation
{
  /**
   * A space separated string containing the allowed file extensions.
   *
   * Defaults to "txt" in \Drupal\file\Plugin\Field\FieldType\FileItem.
   *
   * @var string
   */
  protected $extensions;

  /**
   * The stream wrapper used to store the files.
   *
   * @var string
   */
  protected $uriScheme;

  /**
   * The directory the uploaded files should be placed in.
   *
   * @var string
   */
  protected $directory;

  /**
   * The maximum allowed file size.
   *
   * @var string
   */
  protected $maxFileSize;



  /**
   * Allow this field to pull data from the owning fieldable annotation.
   *
   * @param \Drupal\managed\Annotation\AbstractFieldableAnnotation $owner
   * @return $this
   */
  public function pullOwnerData(AbstractFieldableAnnotation $owner) {
    if (!isset($this->isRevisionable)) {
      $this->isRevisionable = TRUE;
    }

    if (!isset($this->isTranslatable)) {
      $this->isTranslatable = TRUE;
    }

    return $this;
  }


  /**
   * Return the base definition of this field. The base definition should
   * not contain any display or form settings.
   *
   * @param string $type
   * @return \Drupal\Core\Field\FieldDefinitionInterface
   */
  protected function getBaseDefinition($type = 'file') {
    $definition = $this->createBaseDefinition($type);

    if (isset($this->extensions)) {
      $definition->setSetting('file_extensions', $this->extensions);
    }

    if (isset($this->uriScheme)) {
      $definition->setSetting('uri_scheme', $this->uriScheme);
    }

    if (isset($this->directory)) {
      $definition->setSetting('file_directory', $this->directory);
    }

    if (isset($this->maxFileSize)) {
      $definition->setSetting('max_filesize', $this->maxFileSize);
    }

    return $definition;
  }
}
