<?php

namespace Drupal\managed\Annotation\Field;


/**
 * Defines a file upload field.
 *
 * @ManagedAnnotation(id="FileField")
 */
class FileFieldAnnotation extends AbstractFileFieldAnnotation
{
  /**
   * Whether the field should display a description field.
   *
   * @var bool
   */
  protected $hasDescription;



  /**
   * Return the base definition of this field. The base definition should
   * not contain any display or form settings.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface
   */
  protected function getBaseDefinition() {
    $definition = $this->createBaseDefinition('file');

    if (isset($this->description)) {
      $definition->setSetting('description_field', $this->hasDescription);
    }

    return $definition;
  }


  /**
   * Return the display options for the `view` display of this field.
   *
   * @return array|null
   */
  protected function getViewDisplayOptions() {
    return array(
      'type' => 'file_default'
    );
  }


  /**
   * Return the display options for the `form` display of this field.
   *
   * @return array|null
   */
  protected function getFormDisplayOptions() {
    return array(
      'type' => 'file_generic',
    );
  }
}
