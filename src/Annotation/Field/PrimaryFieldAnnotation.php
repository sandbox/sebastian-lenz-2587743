<?php

namespace Drupal\managed\Annotation\Field;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\managed\Annotation\AbstractFieldableAnnotation;


/**
 * Defines a field used as the primary id of an entity.
 *
 * The entity key property of this annotation will be set to 'id'.
 *
 * @ManagedAnnotation(id="PrimaryField")
 */
class PrimaryFieldAnnotation extends AbstractFieldAnnotation
{
  /**
   * Try to find an entity key definition on the given reflection.
   *
   * @param \Drupal\managed\Vendor\Addendum\ReflectionAnnotatedProperty $reflection
   * @return string|null
   */
  protected function pullEntityKey($reflection) {
    return 'id';
  }


  /**
   * Allow this field to pull data from the owning fieldable annotation.
   *
   * @param \Drupal\managed\Annotation\AbstractFieldableAnnotation $owner
   * @return $this
   */
  public function pullOwnerData(AbstractFieldableAnnotation $owner) {
    $this->isReadOnly = TRUE;
    $this->isRequired = TRUE;
    $this->isTranslatable = FALSE;
    $this->isRevisionable = FALSE;

    return $this;
  }


  /**
   * Return the base definition of this field. The base definition should
   * not contain any display or form settings.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface
   */
  function getBaseDefinition() {
    return BaseFieldDefinition::create('integer')
      ->setSetting('unsigned', TRUE);
  }
}
