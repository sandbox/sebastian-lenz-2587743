<?php

namespace Drupal\managed\Annotation\Field;

use Drupal\managed\Annotation\AbstractFieldableAnnotation;


/**
 * Defines an entity reference field.
 *
 * @ManagedAnnotation(id="ReferenceField")
 */
class ReferenceFieldAnnotation extends AbstractFieldAnnotation
{
  /**
   * The entity type if this field should point to.
   *
   * This is a mandatory option.
   *
   * @var string
   */
  protected $entity;



  /**
   * Initialize this annotation. Called by the finalized constructor of the
   * base class `Drupal\managed\Vendor\Addendum\Annotation`.
   *
   * @param \Drupal\managed\Vendor\Addendum\ReflectionAnnotatedProperty $reflection
   * @throws \Exception
   */
  protected function checkConstraints($reflection) {
    parent::checkConstraints($reflection);

    if (!isset($this->entity)) {
      throw new \Exception('A reference filed must have the `entity` option set.');
    }
  }


  /**
   * Return the base definition of this field. The base definition should
   * not contain any display or form settings.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface
   */
  protected function getBaseDefinition() {
    $definition = $this->createBaseDefinition('entity_reference');
    $definition->setSetting('target_type', $this->entity);

    return $definition;
  }


  /**
   * Return the display options for the `view` display of this field.
   *
   * @return array|null
   */
  protected function getViewDisplayOptions() {
    return array(
      'type'  => 'entity_reference_label',
      'label' => 'hidden'
    );
  }


  /**
   * Return the display options for the `form` display of this field.
   *
   * @return array|null
   */
  protected function getFormDisplayOptions() {
    return array(
      'type'     => 'entity_reference_autocomplete',
      'settings' => array(
        'match_operator' => 'CONTAINS',
        'size'           => '60',
        'placeholder'    => '',
      ),
    );
  }


  /**
   * Return the entity type if this field should point to.
   *
   * @return string
   */
  public function getTargetEntityTypeID() {
    return $this->entity;
  }
}
