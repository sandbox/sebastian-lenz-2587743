<?php

namespace Drupal\managed\Annotation\Field;

use Drupal\managed\Annotation\AbstractFieldableAnnotation;


/**
 * Defines a select field.
 *
 * @ManagedAnnotation(id="SelectField")
 */
class SelectFieldAnnotation extends AbstractListFieldAnnotation
{
  /**
   * Allow this field to pull data from the owning fieldable annotation.
   *
   * @param \Drupal\managed\Annotation\AbstractFieldableAnnotation $owner
   * @return $this
   */
  public function pullOwnerData(AbstractFieldableAnnotation $owner) {
    if (!isset($this->isRevisionable)) {
      $this->isRevisionable = TRUE;
    }

    return $this;
  }


  /**
   * Return the display options for the `view` display of this field.
   *
   * @return array|null
   */
  protected function getFormDisplayOptions() {
    return array(
      'type'  => 'options_select'
    );
  }
}
