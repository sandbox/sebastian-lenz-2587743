<?php

namespace Drupal\managed\Annotation\Field;

use Drupal\managed\Annotation\AbstractFieldableAnnotation;


/**
 * Defines a simple text field.
 *
 * @ManagedAnnotation(id="TextField")
 */
class TextFieldAnnotation extends AbstractFieldAnnotation
{
  /**
   * The allowed maximum length of the stored string.
   *
   * Defaults to 255.
   *
   * @var integer
   */
  protected $maxLength = 255;



  /**
   * Allow this field to pull data from the owning fieldable annotation.
   *
   * @param \Drupal\managed\Annotation\AbstractFieldableAnnotation $owner
   * @return $this
   */
  public function pullOwnerData(AbstractFieldableAnnotation $owner) {
    if (!isset($this->isRevisionable)) {
      $this->isRevisionable = TRUE;
    }

    if (!isset($this->isTranslatable)) {
      $this->isTranslatable = TRUE;
    }

    return $this;
  }


  /**
   * Return the base definition of this field. The base definition should
   * not contain any display or form settings.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface
   */
  protected function getBaseDefinition() {
    $definition = $this->createBaseDefinition('string');
    $definition->setSetting('max_length', $this->maxLength);

    return $definition;
  }


  /**
   * Return the display options for the `view` display of this field.
   *
   * @return array|null
   */
  protected function getViewDisplayOptions() {
    return array(
      'type'  => 'string',
      'label' => 'hidden'
    );
  }


  /**
   * Return the display options for the `form` display of this field.
   *
   * @return array|null
   */
  protected function getFormDisplayOptions() {
    return array(
      'type' => 'string_textfield',
    );
  }
}
