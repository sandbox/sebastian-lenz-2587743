<?php

namespace Drupal\managed\Annotation\Field;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\managed\Annotation\AbstractFieldableAnnotation;


/**
 * Defines a field that stores a timestamp.
 *
 * @ManagedAnnotation(id="TimestampField")
 */
class TimestampFieldAnnotation extends AbstractFieldAnnotation
{
  /**
   * Allow this field to pull data from the owning fieldable annotation.
   *
   * @param \Drupal\managed\Annotation\AbstractFieldableAnnotation $owner
   * @return $this
   */
  public function pullOwnerData(AbstractFieldableAnnotation $owner) {
    if (!isset($this->isRevisionable)) {
      $this->isRevisionable = TRUE;
    }

    if (!isset($this->isTranslatable)) {
      $this->isTranslatable = TRUE;
    }

    return $this;
  }


  /**
   * Return the base definition of this field. The base definition should
   * not contain any display or form settings.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface
   */
  protected function getBaseDefinition() {
    return $this->createBaseDefinition('timestamp');
  }


  /**
   * Return the display options for the `view` display of this field.
   *
   * @return array|null
   */
  protected function getViewDisplayOptions() {
    return array(
      'type' => 'timestamp',
    );
  }


  /**
   * Return the display options for the `form` display of this field.
   *
   * @return array|null
   */
  protected function getFormDisplayOptions() {
    return array(
      'type' => 'datetime_default',
    );
  }
}
