<?php

namespace Drupal\managed\Annotation;

use Drupal\managed\Vendor\Addendum\Annotation;


/**
 * A decorator for fields that define an entity key.
 *
 * This annotation must always be used in combination with a field annotation.
 *
 * @ManagedAnnotation(id="EntityKey")
 */
class EntityKeyAnnotation extends Annotation
{

}
