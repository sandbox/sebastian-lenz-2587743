<?php

namespace Drupal\managed\Annotation\Relation;

use Drupal\managed\Annotation\AbstractFormAnnotation;
use Drupal\managed\Behaviour\ListBuilder\ColumnDefinitionInterface;
use Drupal\managed\Entity;


/**
 * Base class of all decorators emitting a relation.
 */
abstract class AbstractRelationAnnotation extends AbstractFormAnnotation implements ColumnDefinitionInterface
{
  /**
   * A list of fields that should be displayed as columns by the list builder.
   *
   * @var string[]
   */
  protected $listColumns = array('@label');



  abstract function createCollection(Entity $owner);


  /**
   * Return a list of fields that should be displayed as columns by
   * the list builder.
   *
   * @return string[]
   */
  public function getColumnDefinitions() {
    return $this->listColumns;
  }


  public function getExtraFieldData() {
    $name = $this->getName();
    $data = array(
      'label'       => $this->label,
      'description' => $this->description,
      'weight'      => $this->weight,
    );

    $extra = array();
    $extra['form'][$name] = $data;
    $extra['display'][$name] = $data;

    return $extra;
  }
}
