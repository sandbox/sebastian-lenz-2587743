<?php

namespace Drupal\managed\Annotation\Relation;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\managed\Core\ManagedEntityType;
use Drupal\managed\Core\Relation\Collection\OneToManyCollection;
use Drupal\managed\Entity;


/**
 * Defines a relation between one parent entity and many child entities.
 *
 * @ManagedAnnotation(id="OneToManyRelation")
 */
class OneToManyRelationAnnotation extends AbstractRelationAnnotation
{
  /**
   * The id of the entity type this relation should point to.
   *
   * This option must be set.
   *
   * @var string
   */
  protected $entity;

  /**
   * The name of the field of the child entity that stores the id of the owner.
   *
   * The field must be a ReferenceField pointing to the entity type of the
   * owner. If this option is omitted we'll use the first field of the child
   * entity containing an entity reference to the owner.
   *
   * @var string
   */
  protected $field;

  /**
   * The name of the field of the child entity that stores the sort order.
   *
   * If the given field is a WeightField the relation editor will allow the
   * user to reorder relations. Defaults to the label field of the child.
   *
   * @var string
   */
  protected $sort;



  /**
   * Create a collection instance of this relation for the given owner.
   *
   * @param \Drupal\managed\Entity $owner
   * @return \Drupal\managed\Core\Relation\Collection\OneToManyCollection
   * @throws \Exception
   */
  public function createCollection(Entity $owner) {
    try {
      return new OneToManyCollection($owner, $this);
    }
    catch (\Exception $e) {
      throw new \Exception(t(
        'The relation `@name` between `@owner` and `@target` could not be established: @message',
        array(
          '@name'    => $this->getName(),
          '@target'  => $this->entity,
          '@owner'   => $owner->getEntityTypeId(),
          '@message' => $e->getMessage()
        )
      ));
    }
  }


  /**
   * Test whether the given field is a valid reference field.
   *
   * @param \Drupal\managed\Entity $owner
   * @param \Drupal\Core\Field\BaseFieldDefinition $field
   * @return bool
   */
  private function isValidField(Entity $owner, BaseFieldDefinition $field) {
    if ($field->getType() != 'entity_reference') {
      return FALSE;
    }

    $targetType = $field->getSetting('target_type');
    if ($targetType != $owner->getEntityTypeId()) {
      return FALSE;
    }

    return TRUE;
  }


  /**
   * Return the name of the field of the child entity that stores the
   * id of the owner.
   *
   * @param \Drupal\managed\Entity $owner
   * @return string
   * @throws \Exception
   */
  public function getField(Entity $owner) {
    $fields = $this
      ->getEntityManager()
      ->getBaseFieldDefinitions($this->entity);

    if (isset($this->field)) {
      if (!isset($fields[$this->field])) {
        throw new \Exception(t(
          'The given field `@field` does not exist.',
          array('@field' => $this->field)
        ));
      }
      elseif (!$this->isValidField($owner, $fields[$this->field])) {
        throw new \Exception(t(
          'The field `@field` must be a reference field pointing to `@owner`.',
          array(
            '@field' => $this->field,
            '@owner' => $owner->getEntityTypeId(),
          )
        ));
      }

      return $this->field;
    }

    foreach ($fields as $name => $field) {
      if ($this->isValidField($owner, $field)) {
        return $name;
      }
    }

    throw new \Exception(t(
      '`@target` must contain a reference field with the target type `@owner`.',
      array(
        '@owner'  => $owner->getEntityTypeId(),
        '@target' => $this->entity
      )
    ));
  }


  /**
   * Return the field that should be sorted on.
   *
   * @return string
   * @throws \Exception
   */
  public function getSort() {
    if (isset($this->sort)) {
      $fields = $this
        ->getEntityManager()
        ->getBaseFieldDefinitions($this->entity);

      if (!isset($fields[$this->sort])) {
        throw new \Exception(t(
          'The given sort field `@field` does not exist.',
          array('@field' => $this->sort)
        ));
      }

      return $this->sort;
    }
    else {
      $entityType = $this->getEntityType();
      if ($entityType->hasKey('label')) {
        return $entityType->getKey('label');
      } elseif ($entityType->hasKey('id')) {
        return $entityType->getKey('id');
      }
    }

    return NULL;
  }


  /**
   * Return the target entity type.
   *
   * @return \Drupal\managed\Core\ManagedEntityType
   * @throws \Exception
   */
  public function getEntityType() {
    $entityManager = $this->getEntityManager();
    $entityType = $entityManager->getDefinition($this->entity, FALSE);
    if (is_null($entityType)) {
      throw new \Exception(t(
        'The entity `@entity` does not exist.',
        array('@entity' => $this->entity)
      ));
    }

    if (!($entityType instanceof ManagedEntityType)) {
      throw new \Exception(t(
        'The entity `@entity` is not a managed entity type.',
        array('@entity' => $this->entity)
      ));
    }

    return $entityType;
  }


  /**
   * Return the entity manager.
   *
   * @return \Drupal\Core\Entity\EntityManager
   */
  private function getEntityManager() {
    return \Drupal::service('entity.manager');
  }
}
