<?php

namespace Drupal\managed\Behaviour\Feature;

use Drupal\managed\Core\ManagedEntityType;
use Drupal\managed\Behaviour\AbstractBehaviour;
use Symfony\Component\Routing\RouteCollection;


abstract class AbstractFeature
{
  /**
   * The behaviour this feature belongs to.
   *
   * @var \Drupal\managed\Behaviour\AbstractBehaviour
   */
  protected $behaviour;



  /**
   * Create a new AbstractFeature instance.
   *
   * @param \Drupal\managed\Behaviour\AbstractBehaviour $behaviour
   */
  public function __construct(AbstractBehaviour $behaviour) {
    $this->behaviour = $behaviour;
  }


  /**
   * Allow this feature to set data on the related entity type.
   *
   * @param \Drupal\managed\Core\ManagedEntityType $entityType
   */
  public function initializeEntityType(ManagedEntityType $entityType) { }


  /**
   * Return the entity type this feature should handle.
   *
   * @return \Drupal\managed\Core\ManagedEntityType
   */
  public function getEntityType() {
    return $this->behaviour->getEntityType();
  }


  /**
   * Return the action links of this feature.
   *
   * @param string $baseName
   * @return array
   */
  public function getActionLinks($baseName) {
    return array();
  }


  /**
   * Return the menu links of this feature.
   *
   * @param string $baseName
   * @return array
   */
  public function getMenuLinks($baseName) {
    return array();
  }


  /**
   * Return the task links of this feature.
   *
   * @param string $baseName
   * @return array
   */
  public function getTaskLinks($baseName) {
    return array();
  }


  /**
   * Return the contextual links of this feature.
   *
   * @param string $baseName
   * @return array
   */
  public function getContextualLinks($baseName) {
    return array();
  }


  /**
   * Allow this behaviour to modify the route collection.
   *
   * @param \Symfony\Component\Routing\RouteCollection $collection
   */
  public function onAlterRoutes(RouteCollection $collection) { }
}
