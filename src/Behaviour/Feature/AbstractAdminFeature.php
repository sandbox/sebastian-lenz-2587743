<?php

namespace Drupal\managed\Behaviour\Feature;


abstract class AbstractAdminFeature extends AbstractFeature
{
  /**
   * @return string
   */
  abstract public function getRouteName();


  abstract public function getPath();
}
