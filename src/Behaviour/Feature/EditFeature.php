<?php

namespace Drupal\managed\Behaviour\Feature;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\managed\Core\ManagedEntityType;
use Drupal\managed\Behaviour\AbstractBehaviour;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;


class EditFeature extends AbstractFeature
{
  /**
   * Allow this feature to set data on the related entity type.
   *
   * @param \Drupal\managed\Core\ManagedEntityType $entityType
   */
  public function initializeEntityType(ManagedEntityType $entityType) {
    $entityType->setLinkTemplate('edit-form', $this->getPath());

    if (!$entityType->hasHandlerClass('form', 'default')) {
      $entityType->setFormClass('default', 'Drupal\managed\Behaviour\Form\EditForm');
    }
  }


  /**
   * @return string
   */
  public function getRouteName() {
    return $this->behaviour->getBaseRouteName() . '.edit_form';
  }


  public function getPath() {
    return $this->behaviour->getBaseAdminPath() . '/{' . $this->getEntityType()->id() . '}/edit';
  }


  protected function getTitle() {
    $name = $this->getEntityType()->getLabel();

    return t(
      'Create @name',
      array('@name' => $name->render())
    )->render();
  }


  /**
   * Return the task links of this feature.
   *
   * @param string $baseName
   * @return array
   */
  public function getTaskLinks($baseName) {
    $canonical = $this->behaviour->getCanonicalFeature();
    if (is_null($canonical)) {
      return array();
    }

    return array(
      "$baseName.edit" => array(
        'title'      => new TranslatableMarkup('Edit'),
        'route_name' => $this->getRouteName(),
        'base_route' => $canonical->getRouteName()
      )
    );
  }


  /**
   * Allow this behaviour to modify the route collection.
   *
   * @param \Symfony\Component\Routing\RouteCollection $collection
   */
  public function onAlterRoutes(RouteCollection $collection) {
    $collection->add($this->getRouteName(), new Route(
      $this->getPath(),
      array(
        '_title' => $this->getTitle(),
        '_entity_form' => $this->getEntityType()->id()
      ), array(
        '_permission' => $this->behaviour->getPermission(AbstractBehaviour::PERMISSION_EDIT),
      ), array(
        '_admin_route' => TRUE,
        'redirect_target' => 'collection'
      )
    ));
  }
}
