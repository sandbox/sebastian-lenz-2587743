<?php

namespace Drupal\managed\Behaviour\Feature;

use Drupal\managed\Core\ManagedEntityType;


abstract class AbstractCanonicalFeature extends AbstractFeature
{
  /**
   * Allow this feature to set data on the related entity type.
   *
   * @param \Drupal\managed\Core\ManagedEntityType $entityType
   */
  public function initializeEntityType(ManagedEntityType $entityType) {
    $entityType->setLinkTemplate('canonical', $this->getPath());
  }


  /**
   * @return string
   */
  public function getRouteName() {
    return $this->behaviour->getBaseRouteName() . '.canonical';
  }


  abstract public function getPath();
}
