<?php

namespace Drupal\managed\Behaviour\Feature;

use Drupal\managed\Behaviour\AbstractBehaviour;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;


class AddFeature extends AbstractAddFeature
{
  /**
   * Allow this behaviour to modify the route collection.
   *
   * @param \Symfony\Component\Routing\RouteCollection $collection
   */
  public function onAlterRoutes(RouteCollection $collection) {
    $collection->add($this->getRouteName(), new Route(
      $this->getPath(),
      array(
        '_title' => $this->getTitle(),
        '_entity_form' => $this->getEntityType()->id()
      ), array(
        '_permission' => $this->behaviour->getPermission(AbstractBehaviour::PERMISSION_ADD),
      ), array(
        '_admin_route' => TRUE,
        'redirect_target' => 'collection'
      )
    ));
  }
}
