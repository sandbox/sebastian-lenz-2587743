<?php

namespace Drupal\managed\Behaviour\Feature;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\managed\Core\ManagedEntityType;
use Drupal\managed\Behaviour\AbstractBehaviour;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;


class DeleteFeature extends AbstractFeature
{
  /**
   * Allow this feature to set data on the related entity type.
   *
   * @param \Drupal\managed\Core\ManagedEntityType $entityType
   */
  public function initializeEntityType(ManagedEntityType $entityType) {
    $entityType->setLinkTemplate('delete-form', $this->getPath());

    if (!$entityType->hasHandlerClass('form', 'delete')) {
      $entityType->setFormClass('delete', 'Drupal\managed\Behaviour\Form\DeleteForm');
    }
  }


  /**
   * @return string
   */
  public function getRouteName() {
    return $this->behaviour->getBaseRouteName() . '.delete_form';
  }


  public function getPath() {
    return $this->behaviour->getBaseAdminPath() . '/{' . $this->getEntityType()->id() . '}/delete';
  }


  protected function getTitle() {
    $name = $this->getEntityType()->getLabel();

    return t(
      'Create @name',
      array('@name' => $name->render())
    )->render();
  }


  /**
   * Return the task links of this feature.
   *
   * @param string $baseName
   * @return array
   */
  public function getTaskLinks($baseName) {
    $canonical = $this->behaviour->getCanonicalFeature();
    if (is_null($canonical)) {
      return array();
    }

    return array(
      "$baseName.delete" => array(
        'title'      => new TranslatableMarkup('Delete'),
        'route_name' => $this->getRouteName(),
        'base_route' => $canonical->getRouteName()
      )
    );
  }


  /**
   * Allow this behaviour to modify the route collection.
   *
   * @param \Symfony\Component\Routing\RouteCollection $collection
   */
  public function onAlterRoutes(RouteCollection $collection) {
    $collection->add($this->getRouteName(), new Route(
      $this->getPath(),
      array(
        '_title' => $this->getTitle(),
        '_entity_form' => $this->getEntityType()->id() . '.delete'
      ), array(
        '_permission' => $this->behaviour->getPermission(AbstractBehaviour::PERMISSION_DELETE),
      ), array(
        '_admin_route' => TRUE
      )
    ));
  }
}
