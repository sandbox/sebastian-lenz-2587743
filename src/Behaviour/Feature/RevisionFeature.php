<?php

namespace Drupal\managed\Behaviour\Feature;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\managed\Core\ManagedEntityType;
use Drupal\managed\Behaviour\AbstractBehaviour;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;


class RevisionFeature extends AbstractFeature
{
  /**
   * Allow this feature to set data on the related entity type.
   *
   * @param \Drupal\managed\Core\ManagedEntityType $entityType
   */
  public function initializeEntityType(ManagedEntityType $entityType) {
    $entityType->setLinkTemplate('version-history', $this->getPath());
  }


  /**
   * @param string $action
   * @return string
   */
  public function getRouteName($action = 'version_history') {
    return $this->behaviour->getBaseRouteName() . '.' . $action;
  }


  /**
   * @param string $action
   * @return string
   */
  public function getPath($action = 'versions') {
    return $this->behaviour->getBaseAdminPath() . '/{' . $this->getEntityType()->id() . '}/' . $action;
  }


  /**
   * Return the task links of this feature.
   *
   * @param string $baseName
   * @return array
   */
  public function getTaskLinks($baseName) {
    $canonical = $this->behaviour->getCanonicalFeature();
    if (is_null($canonical)) {
      return array();
    }

    return array(
      "$baseName.version_history" => array(
        'title'      => new TranslatableMarkup('Versions'),
        'route_name' => $this->getRouteName(),
        'base_route' => $canonical->getRouteName()
      )
    );
  }


  /**
   * Allow this behaviour to modify the route collection.
   *
   * @param \Symfony\Component\Routing\RouteCollection $collection
   */
  public function onAlterRoutes(RouteCollection $collection) {
    $entityTypeID = $this->getEntityType()->id();
    $params = array(
      '_admin_route' => TRUE,
      'parameters' => array(
        "$entityTypeID" => array(
          'type' => "entity:$entityTypeID"
        )
      )
    );

    $collection->add($this->getRouteName(), new Route(
      $this->getPath(),
      array(
        '_title'      => 'Revisions',
        '_controller' => '\Drupal\managed\Behaviour\Controller\RevisionListController::versions'
      ), array(
        '_permission' => $this->behaviour->getPermission(AbstractBehaviour::PERMISSION_REVISION_LIST),
      ), $params
    ));

    $collection->add($this->getRouteName('revision_delete_confirm'), new Route(
      $this->getPath('{revision}/delete'),
      array(
        '_title'      => 'Delete revision',
        '_form'       => '\Drupal\managed\Behaviour\Form\RevisionDeleteForm'
      ), array(
        '_permission' => $this->behaviour->getPermission(AbstractBehaviour::PERMISSION_REVISION_DELETE),
      ), $params
    ));

    $collection->add($this->getRouteName('revision_revert_confirm'), new Route(
      $this->getPath('{revision}/revert'),
      array(
        '_title'      => 'Revert revision',
        '_form'       => '\Drupal\managed\Behaviour\Form\RevisionRevertForm'
      ), array(
        '_permission' => $this->behaviour->getPermission(AbstractBehaviour::PERMISSION_REVISION_REVERT),
      ), $params
    ));

    $collection->add($this->getRouteName('revision_revert_translation_confirm'), new Route(
      $this->getPath('{revision}/revert/{langcode}'),
      array(
        '_title'      => 'Revert revision',
        '_form'       => '\Drupal\managed\Behaviour\Form\RevisionRevertTranslationForm'
      ), array(
        '_permission' => $this->behaviour->getPermission(AbstractBehaviour::PERMISSION_REVISION_REVERT),
      ), $params
    ));
  }
}
