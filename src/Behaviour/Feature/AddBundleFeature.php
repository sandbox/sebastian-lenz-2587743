<?php

namespace Drupal\managed\Behaviour\Feature;

use Drupal\managed\Behaviour\AbstractBehaviour;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;


class AddBundleFeature extends AbstractAddFeature
{
  /**
   * Allow this behaviour to modify the route collection.
   *
   * @param \Symfony\Component\Routing\RouteCollection $collection
   */
  public function onAlterRoutes(RouteCollection $collection) {
    $routeName  = $this->getRouteName();
    $path       = $this->getPath();
    $permission = $this->behaviour->getPermission(AbstractBehaviour::PERMISSION_ADD);

    $collection->add($routeName, new Route(
      $path,
      array(
        '_title'       => $this->getTitle(),
        '_controller'  => 'Drupal\managed\Behaviour\Controller\AddBundleController::bundle',
        'entityTypeID' => $this->getEntityType()->id(),
        'bundleUrl'    => $routeName . '.bundle'
      ), array(
        '_permission'  => $permission,
      ), array(
        '_admin_route' => TRUE
      )
    ));

    $collection->add($routeName . '.bundle', new Route(
      $path . '/{bundle}',
      array(
        '_title_callback' => 'Drupal\managed\Behaviour\Controller\AddBundleController::getAddTitle',
        '_controller'     => 'Drupal\managed\Behaviour\Controller\AddBundleController::add',
        'entity_type_id'  => $this->getEntityType()->id(),
      ), array(
        '_permission'     => $permission,
      ), array(
        '_admin_route'    => TRUE,
        'redirect_target' => 'collection'
      )
    ));
  }
}
