<?php

namespace Drupal\managed\Behaviour\Feature;

use Drupal\managed\Core\ManagedEntityType;


abstract class AbstractAddFeature extends AbstractFeature
{
  /**
   * Allow this feature to set data on the related entity type.
   *
   * @param \Drupal\managed\Core\ManagedEntityType $entityType
   */
  public function initializeEntityType(ManagedEntityType $entityType) {
    $entityType->setLinkTemplate('add-form', $this->getPath());

    if (!$entityType->hasHandlerClass('form', 'default')) {
      $entityType->setFormClass('default', 'Drupal\Core\Entity\ContentEntityForm');
    }
  }


  /**
   * @return string
   */
  public function getRouteName() {
    return $this->behaviour->getBaseRouteName() . '.add';
  }


  public function getPath() {
    return $this->behaviour->getBaseAdminPath() . '/add';
  }


  protected function getTitle() {
    $name = $this->getEntityType()->getLabel();

    return t(
      'Create @name',
      array('@name' => $name->render())
    )->render();
  }


  /**
   * Return the action links of this feature.
   *
   * @param string $baseName
   * @return array
   */
  public function getActionLinks($baseName) {
    $admin = $this->behaviour->getAdminFeature();
    if (is_null($admin)) {
      return array();
    }

    return array(
      "$baseName.add" => array(
        'title'      => $this->getTitle(),
        'route_name' => $this->getRouteName(),
        'appears_on' => array(
          $admin->getRouteName()
        )
      )
    );
  }
}
