<?php

namespace Drupal\managed\Behaviour\Feature;

use Drupal\managed\Behaviour\AbstractBehaviour;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;


class RedirectViewFeature extends AbstractCanonicalFeature
{
  public function getPath() {
    return $this->behaviour->getBaseAdminPath() . '/{' . $this->getEntityType()->id() . '}';
  }


  /**
   * Allow this behaviour to modify the route collection.
   *
   * @param \Symfony\Component\Routing\RouteCollection $collection
   */
  public function onAlterRoutes(RouteCollection $collection) {
    $entityTypeID = $this->getEntityType()->id();
    $collection->add($this->getRouteName(), new Route(
      $this->getPath(),
      array(
        '_controller' => '\Drupal\managed\Behaviour\Controller\RedirectViewController::redirect',
        '_title_callback' => '\Drupal\managed\Behaviour\Controller\RedirectViewController::title',
      ), array(
        '_permission' => $this->behaviour->getPermission(AbstractBehaviour::PERMISSION_VIEW),
      ), array(
        'parameters' => array(
          "$entityTypeID" => array(
            'type' => "entity:$entityTypeID"
          )
        )
      )
    ));
  }
}
