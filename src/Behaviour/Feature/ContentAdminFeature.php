<?php

namespace Drupal\managed\Behaviour\Feature;

use Drupal\managed\Core\ManagedEntityType;
use Drupal\managed\Behaviour\AbstractBehaviour;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;


class ContentAdminFeature extends AbstractAdminFeature
{
  /**
   * Allow this feature to set data on the related entity type.
   *
   * @param \Drupal\managed\Core\ManagedEntityType $entityType
   */
  public function initializeEntityType(ManagedEntityType $entityType) {
    $entityType->setLinkTemplate('collection', $this->getPath());

    if (!$entityType->hasHandlerClass('list_builder')) {
      $entityType->setHandlerClass('list_builder', 'Drupal\managed\Behaviour\ListBuilder\ListBuilder');
    }
  }


  /**
   * @return string
   */
  public function getRouteName() {
    return $this->behaviour->getBaseRouteName() . '.collection';
  }


  public function getPath() {
    return $this->behaviour->getBaseAdminPath();
  }


  private function getTitle() {
    if (isset($this->title)) {
      return $this->title;
    } else {
      return $this->getEntityType()->getPluralLabel();
    }
  }


  /**
   * Return the task links of this feature.
   *
   * @param string $baseName
   * @return array
   */
  public function getTaskLinks($baseName) {
    return array(
      "$baseName" => array(
        'title'      => $this->getTitle(),
        'route_name' => $this->getRouteName(),
        'base_route' => 'system.admin_content'
      )
    );
  }


  /**
   * Allow this behaviour to modify the route collection.
   *
   * @param \Symfony\Component\Routing\RouteCollection $collection
   */
  public function onAlterRoutes(RouteCollection $collection) {
    $route = new Route($this->getPath(),
      array(
        '_title' => $this->getTitle(),
        '_entity_list' => $this->getEntityType()->id()
      ), array(
        '_permission' => $this->behaviour->getPermission(AbstractBehaviour::PERMISSION_COLLECTION),
      ), array(
        '_admin_route' => TRUE
      )
    );

    $collection->add($this->getRouteName(), $route);
  }
}
