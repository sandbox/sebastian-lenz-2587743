<?php

namespace Drupal\managed\Behaviour\ListBuilder;


interface ColumnHostInterface
{
  /**
   * Return the entity type displayed by this list.
   *
   * @return \Drupal\managed\Core\ManagedEntityType
   */
  public function getEntityType();


  /**
   * Return the field storage definitions of the entity type.
   *
   * @param string $name
   * @return \Drupal\Core\Field\FieldDefinitionInterface
   */
  public function getFieldStorageDefinition($name);
}
