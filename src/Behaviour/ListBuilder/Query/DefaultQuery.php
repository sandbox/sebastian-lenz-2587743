<?php

namespace Drupal\managed\Behaviour\ListBuilder\Query;

use Drupal\managed\Behaviour\ListBuilder\Column\AbstractColumn;


class DefaultQuery extends AbstractQuery
{
  /**
   * Return an array of all entity ids to be loaded.
   *
   * Each row must contain an array defining 'id' and optional 'langcode'.
   *
   * @return array
   */
  protected function getEntityIds() {
    $entityType = $this->builder->getEntityType();
    $baseTable  = $entityType->getBaseTable();
    $idKey      = $entityType->getKey('id');

    $query = $this->getConnection()
      ->select($baseTable)
      ->fields($baseTable, array($idKey));

    $this->initializeQuery($query);
    $this->initializePager($query);

    $result = array();
    foreach ($query->execute() as $row) {
      $result[] = array(
        'id' => $row->$idKey
      );
    }

    return $result;
  }


  /**
   * Return the field name of the given property. Used to map sort/filter
   * properties to fields within the database.
   *
   * @param string $property
   * @return string|null
   */
  protected function getFieldName($property) {
    $definition = $this->builder->getFieldStorageDefinition($property);
    if (is_null($definition)) {
      return NULL;
    }

    if (!in_array($definition->getType(), AbstractColumn::$ALLOWED_SORT_FIELD_TYPES)) {
      return NULL;
    }

    $table = $this->builder->getEntityType()->getBaseTable();
    return $table . '.' . $definition->getName();
  }
}
