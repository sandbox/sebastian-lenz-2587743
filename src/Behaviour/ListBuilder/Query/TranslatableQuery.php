<?php

namespace Drupal\managed\Behaviour\ListBuilder\Query;


use Drupal\managed\Behaviour\ListBuilder\Column\AbstractColumn;

class TranslatableQuery extends DefaultQuery
{
  /**
   * Return an array of all entity ids to be loaded.
   *
   * Each row must contain an array defining 'id' and optional 'langcode'.
   *
   * @return array
   */
  protected function getEntityIds() {
    $entityType  = $this->builder->getEntityType();
    $dataTable   = $entityType->getDataTable();
    $idKey       = $entityType->getKey('id');
    $langCodeKey = $entityType->getKey('langcode');

    $query = $this->getConnection()
      ->select($dataTable)
      ->fields($dataTable, array($idKey, $langCodeKey));

    if (FALSE) {
      $baseTable = $entityType->getBaseTable();
      $query->innerJoin($baseTable, $baseTable, "$baseTable.$idKey = $dataTable.$idKey");
    }

    $this->initializeQuery($query);
    $this->initializePager($query);

    $result = array();
    foreach ($query->execute() as $row) {
      $result[] = array(
        'id'       => $row->$idKey,
        'langcode' => $row->$langCodeKey
      );
    }

    return $result;
  }


  /**
   * Return the field name of the given property. Used to map sort/filter
   * properties to fields within the database.
   *
   * @param string $property
   * @return string|null
   */
  protected function getFieldName($property) {
    $definition = $this->builder->getFieldStorageDefinition($property);
    if (is_null($definition)) {
      return NULL;
    }

    if (!in_array($definition->getType(), AbstractColumn::$ALLOWED_SORT_FIELD_TYPES)) {
      return NULL;
    }

    if ($definition->isTranslatable()) {
      $table = $this->builder->getEntityType()->getDataTable();
    } else {
      $table = $this->builder->getEntityType()->getBaseTable();
    }

    return $table . '.' . $definition->getName();
  }
}
