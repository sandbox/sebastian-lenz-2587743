<?php

namespace Drupal\managed\Behaviour\ListBuilder\Query;

use Drupal\Core\Database\Query\PagerSelectExtender;
use Drupal\managed\Behaviour\ListBuilder\ListBuilder;
use Drupal\managed\Entity;


abstract class AbstractQuery
{
  /**
   * @var \Drupal\managed\Behaviour\ListBuilder\ListBuilder
   */
  protected $builder;

  /**
   * @var array
   */
  protected $sort;

  /**
   * @var array
   */
  protected $pager;



  /**
   * Create a new AbstractQuery instance.
   *
   * @param \Drupal\managed\Behaviour\ListBuilder\ListBuilder $builder
   */
  public function __construct(ListBuilder $builder) {
    $this->builder = $builder;
  }


  /**
   * Setup the properties of the pager.
   *
   * @param int $limit
   * @param int|null $element
   * @return $this
   */
  public function setPager($limit = 10, $element = NULL) {
    if (!isset($element)) {
      $element = PagerSelectExtender::$maxElement++;
    } elseif ($element >= PagerSelectExtender::$maxElement) {
      PagerSelectExtender::$maxElement = $element + 1;
    }

    $this->pager = array(
      'limit' => $limit,
      'element' => $element,
    );

    return $this;
  }


  /**
   * Set the sort field and order.
   *
   * @param string $property
   * @param string $order
   */
  public function setSort($property, $order) {
    $field = $this->getFieldName($property);
    if (!is_null($field)) {
      if (!in_array($order, array('asc', 'desc'))) {
        $order = 'asc';
      }

      $this->sort = array(
        'field' => $field,
        'order' => $order
      );
    }
  }


  /**
   * Load the entities.
   *
   * @return \Drupal\managed\Entity[]
   */
  public function load() {
    $rows = $this->getEntityIds();
    if (!is_array($rows) || count($rows) == 0) {
      return array();
    }

    $ids = array();
    foreach ($rows as $row) {
      if (!in_array($row['id'], $ids)) {
        $ids[] = $row['id'];
      }
    }

    $entities = $this->builder->getStorage()->loadMultiple($ids);
    foreach ($rows as $index => $row) {
      if (!isset($entities[$row['id']])) {
        continue;
      }

      $entity = $entities[$row['id']];
      if (!($entity instanceof Entity)) {
        continue;
      }

      if (isset($row['langcode'])) {
        $entity = $entity->getTranslation($row['langcode']);
      }

      $rows[$index] = $entity;
    }

    return array_filter($rows);
  }


  /**
   * Apply the desired filter and sorting settings.
   *
   * @param \Drupal\Core\Database\Query\SelectInterface $query
   */
  protected function initializeQuery($query) {
    if (isset($this->sort)) {
      $query->orderBy($this->sort['field'], $this->sort['order']);
    }
  }


  /**
   * Gets the total number of results and initialize a pager for the query.
   *
   * The pager can be disabled by either setting the pager limit to 0, or by
   * setting this query to be a count query.
   *
   * @param \Drupal\Core\Database\Query\SelectInterface $query
   */
  protected function initializePager($query) {
    if (!isset($this->pager) || empty($this->pager['limit'])) {
      return;
    }

    $pager = $this->pager;
    $page  = pager_find_page($pager['element']);
    $count = clone $query;

    $pager['total'] = $count->countQuery()->execute()->fetchField();
    $pager['start'] = $page * $pager['limit'];
    pager_default_initialize($pager['total'], $pager['limit'], $pager['element']);

    $query->range($pager['start'], $pager['limit']);
    $this->pager = $pager;
  }


  /**
   * Return an array of all entity ids to be loaded.
   *
   * Each row must contain an array defining 'id' and optional 'langcode'.
   *
   * @return array
   */
  abstract protected function getEntityIds();


  /**
   * Return the field name of the given property. Used to map sort/filter
   * properties to fields within the database.
   *
   * @param string $property
   * @return string|null
   */
  abstract protected function getFieldName($property);


  /**
   * Return the database connection.
   *
   * @return \Drupal\Core\Database\Connection
   */
  protected function getConnection() {
    return $this->builder->getService('database');
  }


  /**
   * Create a new query instance for the given list builder.
   *
   * @param \Drupal\managed\Behaviour\ListBuilder\ListBuilder $builder
   * @return \Drupal\managed\Behaviour\ListBuilder\Query\AbstractQuery
   */
  public static function create(ListBuilder $builder) {
    if ($builder->getEntityType()->isTranslatable()) {
      return new TranslatableQuery($builder);
    } else {
      return new DefaultQuery($builder);
    }
  }
}
