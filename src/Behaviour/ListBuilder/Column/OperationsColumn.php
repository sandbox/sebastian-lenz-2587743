<?php

namespace Drupal\managed\Behaviour\ListBuilder\Column;

use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\managed\Behaviour\ListBuilder\ColumnHostInterface;
use Drupal\managed\Entity;


/**
 * Defines a column that displays an operation button for the entity.
 */
class OperationsColumn extends AbstractColumn
{
  /**
   * @var \Drupal\Core\Entity\EntityListBuilder
   */
  private $builder;



  /**
   * Create a new AbstractColumn instance.
   *
   * @param \Drupal\Core\Entity\EntityListBuilder $builder
   * @param string $name
   */
  public function __construct(EntityListBuilder $builder, $name) {
    parent::__construct($name);

    $this->builder = $builder;
  }


  /**
   * Return the label of this column.
   *
   * @return string
   */
  public function getLabel() {
    return new TranslatableMarkup('Operations');
  }


  /**
   * Return the contents of this column for the given entity.
   *
   * @param \Drupal\managed\Entity $entity
   * @return mixed
   */
  public function getContent(Entity $entity) {
    return array(
      'data' => $this->builder->buildOperations($entity)
    );
  }


  /**
   * Test whether this column mapping applies to the given column name
   * and entity type and return an instance if it matches.
   *
   * @param \Drupal\managed\Behaviour\ListBuilder\ColumnHostInterface $host
   * @param string $name
   * @return null|static
   */
  static function tryCreate(ColumnHostInterface $host, $name) {
    if ($name != '@operations') {
      return NULL;
    }

    if (!($host instanceof EntityListBuilder)) {
      return NULL;
    }

    return new static($host, $name);
  }
}
