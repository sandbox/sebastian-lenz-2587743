<?php

namespace Drupal\managed\Behaviour\ListBuilder\Column;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\managed\Behaviour\ListBuilder\ColumnHostInterface;
use Drupal\managed\Core\ManagedEntityType;
use Drupal\managed\Entity;


/**
 * Defines a column that displays the label of the entity.
 */
class LanguageColumn extends AbstractColumn
{
  /**
   * Create a new AbstractColumn instance.
   *
   * @param \Drupal\managed\Behaviour\ListBuilder\ColumnHostInterface $host
   * @param string $name
   */
  public function __construct(ColumnHostInterface $host, $name) {
    parent::__construct($name);

    $key = $host->getEntityType()->getKey('langcode');
    if ($key !== FALSE) {
      $this->field = $host->getFieldStorageDefinition($key);
    }
  }


  /**
   * Return the label of this column.
   *
   * @return string
   */
  public function getLabel() {
    return new TranslatableMarkup('Language');
  }


  /**
   * Return the contents of this column for the given entity.
   *
   * @param \Drupal\managed\Entity $entity
   * @return mixed
   */
  public function getContent(Entity $entity) {
    $language = $entity->language();

    if (is_null($language)) {
      return '-';
    } else {
      return $language->getName();
    }
  }


  /**
   * Test whether this column mapping applies to the given column name
   * and entity type and return an instance if it matches.
   *
   * @param \Drupal\managed\Behaviour\ListBuilder\ColumnHostInterface $host
   * @param string $name
   * @return null|static
   */
  static function tryCreate(ColumnHostInterface $host, $name) {
    $entityType = $host->getEntityType();
    if (!in_array($name, array('@langcode', '@language')) || !$entityType->isTranslatable()) {
      return NULL;
    }

    return new static($host, $name);
  }
}
