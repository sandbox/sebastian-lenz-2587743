<?php

namespace Drupal\managed\Behaviour\ListBuilder\Column;

use Drupal\managed\Behaviour\ListBuilder\ColumnHostInterface;
use Drupal\managed\Core\ManagedEntityType;
use Drupal\managed\Entity;
use Drupal\managed\Annotation\Behaviour\ListBuilderConfigurationInterface;


/**
 * Base class of all column mappings.
 */
abstract class AbstractColumn
{
  /**
   * The internal name of this column.
   *
   * @var string
   */
  protected $name;

  /**
   * The storage definition of the related field.
   *
   * @var \Drupal\Core\Field\FieldDefinitionInterface
   */
  protected $field;

  /**
   * A list of a field types we can sort on.
   *
   * @var string[]
   */
  public static $ALLOWED_SORT_FIELD_TYPES = array(
    'string'
  );

  /**
   * A list of all known column classes.
   *
   * @var string[]
   */
  static private $COLUMN_CLASSES = array(
    'Drupal\managed\Behaviour\ListBuilder\Column\LabelColumn',
    'Drupal\managed\Behaviour\ListBuilder\Column\OperationsColumn',
    'Drupal\managed\Behaviour\ListBuilder\Column\BundleColumn',
    'Drupal\managed\Behaviour\ListBuilder\Column\LanguageColumn',
    'Drupal\managed\Behaviour\ListBuilder\Column\FieldColumn'
  );



  /**
   * Create a new AbstractColumn instance.
   *
   * @param string $name
   */
  public function __construct($name) {
    $this->name = $name;
  }


  /**
   * Test whether this column is sortable.
   *
   * @return bool
   */
  private function isSortable() {
    if (!isset($this->field)) {
      return false;
    }

    if (!in_array($this->field->getType(), self::$ALLOWED_SORT_FIELD_TYPES)) {
      return false;
    }

    return true;
  }


  /**
   * Return the internal name of this column.
   *
   * @return string
   */
  public function getName() {
    return $this->name;
  }


  /**
   * Return the label of this column.
   *
   * @return string
   */
  abstract public function getLabel();


  /**
   * Return the contents of this column for the given entity.
   *
   * @param \Drupal\managed\Entity $entity
   * @return mixed
   */
  abstract public function getContent(Entity $entity);


  /**
   * Return the table header definition.
   *
   * @return array
   */
  public function getTableHeader() {
    $header = array(
      'data' => $this->getLabel()
    );

    if ($this->isSortable()) {
      $header['field'] = $this->field->getName();
    }

    return $header;
  }


  /**
   * Build the column mapping.
   *
   * @param \Drupal\managed\Behaviour\ListBuilder\ColumnHostInterface $host
   * @param string[] $definitions
   * @return AbstractColumn[]
   */
  static public function buildColumns(ColumnHostInterface $host, $definitions) {
    $entityType = $host->getEntityType();
    if (!$entityType instanceof ManagedEntityType) {
      throw new \InvalidArgumentException();
    }

    $columns = array();
    foreach ($definitions as $definition) {
      foreach (self::$COLUMN_CLASSES as $columnClass) {
        $mapping = call_user_func_array(array($columnClass, 'tryCreate'), array($host, $definition));

        if (!is_null($mapping)) {
          $columns[] = $mapping;
          continue 2;
        }
      }
    }

    return $columns;
  }


  /**
   * Test whether this column mapping applies to the given column name
   * and entity type and return an instance if it matches.
   *
   * @param \Drupal\managed\Behaviour\ListBuilder\ColumnHostInterface $host
   * @param string $name
   * @return null|static
   */
  static public function tryCreate(ColumnHostInterface $host, $name) {
    return NULL;
  }
}
