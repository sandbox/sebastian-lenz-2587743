<?php

namespace Drupal\managed\Behaviour\ListBuilder\Column;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\managed\Behaviour\ListBuilder\ColumnHostInterface;
use Drupal\managed\Entity;


/**
 * A column that displays the contents of a field.
 */
class FieldColumn extends AbstractColumn
{
  /**
   * Create a new FieldColumn instance.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field
   * @param string $name
   */
  public function __construct(FieldDefinitionInterface $field, $name) {
    parent::__construct($name);

    $this->field = $field;
  }


  /**
   * Return the label of this column.
   *
   * @return string
   */
  public function getLabel() {
    return $this->field->getLabel();
  }


  /**
   * Return the contents of this column for the given entity.
   *
   * @param \Drupal\managed\Entity $entity
   * @return mixed
   */
  public function getContent(Entity $entity) {
    return array(
      'data' => $entity->get($this->field->getName())->view(array(
        'label' => 'hidden'
      ))
    );
  }


  /**
   * Test whether this column mapping applies to the given column name
   * and entity type and return an instance if it matches.
   *
   * @param \Drupal\managed\Behaviour\ListBuilder\ColumnHostInterface $host
   * @param string $name
   * @return null|static
   */
  static function tryCreate(ColumnHostInterface $host, $name) {
    $field = $host->getFieldStorageDefinition($name);
    if (is_null($field)) {
      return NULL;
    }

    return new static($field, $name);
  }
}
