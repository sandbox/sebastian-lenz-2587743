<?php

namespace Drupal\managed\Behaviour\ListBuilder\Column;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\managed\Behaviour\ListBuilder\ColumnHostInterface;
use Drupal\managed\Entity;


/**
 * Defines a column that displays the label of the entity.
 */
class LabelColumn extends AbstractColumn
{
  /**
   * Create a new LabelColumn instance.
   *
   * @param \Drupal\managed\Behaviour\ListBuilder\ColumnHostInterface $host
   * @param string $name
   */
  public function __construct(ColumnHostInterface $host, $name) {
    parent::__construct($name);

    $key = $host->getEntityType()->getKey('label');
    if ($key !== FALSE) {
      $this->field = $host->getFieldStorageDefinition($key);
    }
  }


  /**
   * Return the label of this column.
   *
   * @return string
   */
  public function getLabel() {
    return new TranslatableMarkup('Label');
  }


  /**
   * Return the contents of this column for the given entity.
   *
   * @param \Drupal\managed\Entity $entity
   * @return mixed
   */
  public function getContent(Entity $entity) {
    return $entity->label();
  }


  /**
   * Test whether this column mapping applies to the given column name
   * and entity type and return an instance if it matches.
   *
   * @param \Drupal\managed\Behaviour\ListBuilder\ColumnHostInterface $host
   * @param string $name
   * @return null|static
   */
  static function tryCreate(ColumnHostInterface $host, $name) {
    if ($name != '@label') {
      return NULL;
    }

    return new static($host, $name);
  }
}
