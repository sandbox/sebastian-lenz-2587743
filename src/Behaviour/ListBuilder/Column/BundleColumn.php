<?php

namespace Drupal\managed\Behaviour\ListBuilder\Column;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\managed\Behaviour\ListBuilder\ColumnHostInterface;
use Drupal\managed\Core\ManagedEntityType;
use Drupal\managed\Entity;


/**
 * Defines a column that displays the bundle of the entity.
 */
class BundleColumn extends AbstractColumn
{
  /**
   * The label of the column.
   *
   * @var string
   */
  private $label;

  /**
   * A list of all bundle labels keyed by the bundle ids.
   *
   * @var string[]
   */
  private $mapping;



  /**
   * Create a new BundleColumn instance.
   *
   * @param \Drupal\managed\Behaviour\ListBuilder\ColumnHostInterface $host
   * @param \Drupal\managed\Core\ManagedEntityType $entityType
   * @param string $name
   */
  public function __construct(ColumnHostInterface $host, ManagedEntityType $entityType, $name) {
    parent::__construct($name);

    $this->label = $entityType->getBundleLabel();
    $this->field = $host->getFieldStorageDefinition($entityType->getKey('bundle'));

    $this->mapping = array();
    foreach ($entityType->getBundles() as $bundle) {
      $this->mapping[$bundle->id()] = $bundle->getLabel();
    }
  }


  /**
   * Return the label of this column.
   *
   * @return string
   */
  public function getLabel() {
    return $this->label;
  }


  /**
   * Return the contents of this column for the given entity.
   *
   * @param \Drupal\managed\Entity $entity
   * @return mixed
   */
  public function getContent(Entity $entity) {
    $id = $entity->bundle();

    if (isset($this->mapping[$id])) {
      return $this->mapping[$id];
    } else {
      return new TranslatableMarkup('Unknown');
    }
  }


  /**
   * Test whether this column mapping applies to the given column name
   * and entity type and return an instance if it matches.
   *
   * @param \Drupal\managed\Behaviour\ListBuilder\ColumnHostInterface $host
   * @param string $name
   * @return null|static
   */
  static function tryCreate(ColumnHostInterface $host, $name) {
    $entityType = $host->getEntityType();
    if ($name != '@bundle' || !$entityType->hasBundles()) {
      return NULL;
    }

    return new static($host, $entityType, $name);
  }
}
