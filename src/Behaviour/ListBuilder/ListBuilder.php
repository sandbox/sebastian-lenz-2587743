<?php

namespace Drupal\managed\Behaviour\ListBuilder;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\managed\Behaviour\ListBuilder\Column\AbstractColumn;
use Drupal\managed\Behaviour\ListBuilder\Query\AbstractQuery;
use Drupal\managed\Entity;
use Drupal\managed\Utils\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;


class ListBuilder extends EntityListBuilder implements ColumnHostInterface
{
  use ContainerAwareTrait;

  /**
   * @var \Drupal\Core\Field\FieldDefinitionInterface[]
   */
  private $fieldStorageDefinitions;

  /**
   * The desired column mapping.
   *
   * @var \Drupal\managed\Behaviour\ListBuilder\Column\AbstractColumn[]
   */
  private $columns;

  /**
   * The current sort field.
   *
   * @var array
   */
  private $sortField;

  /**
   * The current sort order.
   *
   * @var string
   */
  private $sortOrder;



  /**
   * Constructs a new EntityListBuilder object.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   */
  public function __construct(ContainerInterface $container, EntityTypeInterface $entity_type) {
    $storage = $container->get('entity.manager')->getStorage($entity_type->id());
    $this->setContainer($container);

    parent::__construct($entity_type, $storage);
  }


  /**
   * Instantiates a new instance of this entity handler.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   * @return static
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static($container, $entity_type);
  }


  /**
   * Loads entities of this type from storage for listing.
   *
   * This allows the implementation to manipulate the listing, like filtering or
   * sorting the loaded entities.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   An array of entities implementing \Drupal\Core\Entity\EntityInterface.
   */
  public function load() {
    $query = AbstractQuery::create($this);

    if ($this->sortField) {
      $query->setSort($this->sortField['sql'], $this->sortOrder);
    }

    if ($this->limit) {
      $query->setPager($this->limit);
    }

    return $query->load();
  }


  /**
   * Return the entity type displayed by this list.
   *
   * @return \Drupal\managed\Core\ManagedEntityType
   */
  public function getEntityType() {
    return $this->entityType;
  }


  /**
   * Return the field storage definitions of the entity type.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   */
  public function getFieldStorageDefinitions() {
    if (!isset($this->fieldStorageDefinitions)) {
      $storage = $this->getStorage();
      if (!($storage instanceof SqlContentEntityStorage)) {
        $this->fieldStorageDefinitions = array();
      } else {
        $this->fieldStorageDefinitions = $storage->getFieldStorageDefinitions();
      }
    }

    return $this->fieldStorageDefinitions;
  }


  /**
   * Return the field storage definitions of the entity type.
   *
   * @param string $name
   * @return \Drupal\Core\Field\FieldDefinitionInterface
   */
  public function getFieldStorageDefinition($name) {
    if (!isset($this->fieldStorageDefinitions)) {
      $this->getFieldStorageDefinitions();
    }

    if (isset($this->fieldStorageDefinitions[$name])) {
      return $this->fieldStorageDefinitions[$name];
    } else {
      return NULL;
    }
  }


  /**
   * Return the column mapping.
   *
   * @return \Drupal\managed\Behaviour\ListBuilder\Column\AbstractColumn[]
   */
  public function getColumns() {
    if (!isset($this->columns)) {
      $behaviour = $this->getEntityType()->getBehaviour();
      if (!($behaviour instanceof ColumnDefinitionInterface)) {
        throw new \InvalidArgumentException();
      }

      $definitions = $behaviour->getColumnDefinitions();
      $this->columns = AbstractColumn::buildColumns($this, $definitions);
    }

    return $this->columns;
  }


  /**
   * Builds the header row for the entity listing.
   *
   * @return array
   *   A render array structure of header strings.
   *
   * @see \Drupal\Core\Entity\EntityListBuilder::render()
   */
  public function buildHeader() {
    $row = array();

    foreach ($this->getColumns() as $column) {
      $row[$column->getName()] = $column->getTableHeader();
    }

    $this->sortField = tablesort_get_order($row);
    $this->sortOrder = tablesort_get_sort($row);
    return $row;
  }


  /**
   * Builds a row for an entity in the entity listing.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity for this row of the list.
   *
   * @return array
   *   A render array structure of fields for this entity.
   *
   * @see \Drupal\Core\Entity\EntityListBuilder::render()
   */
  public function buildRow(EntityInterface $entity) {
    if (!($entity instanceof Entity)) {
      throw new \InvalidArgumentException();
    }

    $row = array();
    foreach ($this->getColumns() as $column) {
      $row[$column->getName()] = $column->getContent($entity);
    }

    return $row;
  }


  /**
   * Builds a listing of entities for the given entity type.
   *
   * @return array
   *   A render array as expected by drupal_render().
   */
  public function render() {
    $build['table'] = array(
      '#type'   => 'table',
      '#header' => $this->buildHeader(),
      '#title'  => $this->getTitle(),
      '#rows'   => array(),
      '#empty'  => $this->t('There is no @label yet.', array('@label' => $this->entityType->getLabel())),
      '#cache'  => [
        'contexts' => $this->entityType->getListCacheContexts(),
        'tags'     => $this->entityType->getListCacheTags(),
      ],
    );

    foreach ($this->load() as $entity) {
      if ($row = $this->buildRow($entity)) {
        $build['table']['#rows'][] = $row;
      }
    }

    if ($this->limit) {
      $build['pager'] = array(
        '#type' => 'pager',
      );
    }

    return $build;
  }
}
