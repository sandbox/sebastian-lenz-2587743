<?php

namespace Drupal\managed\Behaviour\ListBuilder;


/**
 * Base class of all annotations describing entity behaviours.
 */
interface ColumnDefinitionInterface
{
  /**
   * Return a list of fields that should be displayed as columns by
   * the list builder.
   *
   * @return string[]
   */
  public function getColumnDefinitions();
}
