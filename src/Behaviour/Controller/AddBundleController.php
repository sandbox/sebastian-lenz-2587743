<?php

namespace Drupal\managed\Behaviour\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\managed\Core\ManagedEntityTypeManager;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Displays add pages for bundleable entities.
 */
class AddBundleController extends ControllerBase implements ContainerInjectionInterface
{
  /**
   * @var \Drupal\managed\Core\ManagedEntityTypeManager
   */
  private $manager;


  /**
   * Create a new AddBundleController instance.
   *
   * @param \Drupal\managed\Core\ManagedEntityTypeManager $manager
   */
  public function __construct(ManagedEntityTypeManager $manager) {
    $this->manager = $manager;
  }


  /**
   * Instantiates a new instance of this class.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('managed.entity_manager'));
  }


  /**
   * Displays all available bundles.
   *
   * @param string $entityTypeID
   * @param string $bundleUrl
   * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function bundle($entityTypeID, $bundleUrl) {
    $entityType = $this->manager->getManagedType($entityTypeID);

    $bundles = $entityType->getBundles();
    if (count($bundles) == 1) {
      $bundle = reset($bundles);
      return $this->redirect($bundleUrl, array('bundle' => $bundle->id()));
    }

    $content = array();
    foreach ($bundles as $bundle) {
      $label = $bundle->getLabel();
      $url   = new Url($bundleUrl, array('bundle' => $bundle->id()));

      $content[$bundle->id()] = array(
        'label'       => $label,
        'url'         => $url->toString(),
        'add_link'    => $this->l($label, $url),
        'description' => array(
          '#markup'   => $bundle->getDescription(),
        ),
      );
    }

    return array(
      '#theme'   => 'managed_entity_add_list',
      '#types'   => $content,
      '#content' => count($content) > 0
    );
  }


  /**
   * Returns the add form of a bundle.
   *
   * @param string $entity_type_id
   * @param string $bundle
   * @return array
   *
   */
  public function add($entity_type_id, $bundle) {
    $entityType = $this->manager->getManagedType($entity_type_id);
    $bundleKey  = $entityType->getKey('bundle');

    $entityManager = $this->manager->getEntityManager();
    $storage = $entityManager->getStorage($entity_type_id);

    $values = array();
    $values[$bundleKey] = $bundle;
    $entity = $storage->create($values);

    return $this->entityFormBuilder()->getForm($entity);
  }


  /**
   * The title callback for the add route.
   *
   * @param string $entity_type_id
   * @param string $bundle
   * @return string
   */
  public function getAddTitle($entity_type_id, $bundle) {
    $bundleInfo = $this->manager->getBundle($entity_type_id, $bundle);

    return $this->t(
      'Create @name',
      array('@name' => $bundleInfo->getLabel())
    );
  }
}
