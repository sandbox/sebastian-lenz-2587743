<?php

namespace Drupal\managed\Behaviour\Controller;

use Drupal\Core\Entity\Controller\EntityViewController;
use Drupal\managed\Utils\EntityAwareRouteTrait;


/**
 * Defines a controller to render a single node.
 */
class ViewController extends EntityViewController
{
  use EntityAwareRouteTrait;


  /**
   * Provides a page to render a single entity.
   *
   * @param string $view_mode
   *   (optional) The view mode that should be used to display the entity.
   *   Defaults to 'full'.
   * @return array
   *   A render array as expected by drupal_render().
   */
  public function view($view_mode = 'full') {
    return parent::view($this->getEntity(), $view_mode);
  }


  /**
   * The _title_callback for the page that renders a single node.
   *
   * @return string
   *   The page title.
   */
  public function title() {
    return $this->getEntity()->label();
  }
}
