<?php

namespace Drupal\managed\Behaviour\Controller;

use Drupal\Core\Entity\Controller\EntityViewController;
use Drupal\managed\Utils\EntityAwareRouteTrait;
use Symfony\Component\HttpFoundation\RedirectResponse;


/**
 * Defines a controller to render a single node.
 */
class RedirectViewController extends EntityViewController
{
  use EntityAwareRouteTrait;


  /**
   * Provides a page to render a single entity.
   *
   * @param string $target
   * @return RedirectResponse
   */
  public function redirect($target = 'edit-form') {
    return new RedirectResponse($this->getEntity()->url($target));
  }


  /**
   * The _title_callback for the page that renders a single node.
   *
   * @return string
   *   The page title.
   */
  public function title() {
    return $this->getEntity()->label();
  }
}
