<?php

namespace Drupal\managed\Behaviour\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Url;
use Drupal\managed\Core\ManagedEntityType;
use Drupal\managed\Entity;
use Drupal\managed\Utils\ContainerAwareTrait;
use Drupal\managed\Utils\EntityAwareRouteTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;


class RevisionListController extends ControllerBase
{
  use ContainerAwareTrait;
  use EntityAwareRouteTrait;

  /**
   * @var string
   */
  protected $langCode;

  /**
   * @var string
   */
  protected $langName;

  /**
   * @var bool
   */
  protected $hasTranslations;

  /**
   * @var string[]
   */
  protected $routes;

  /**
   * @var string
   */
  protected $revisionUid;

  /**
   * @var string
   */
  protected $revisionTimestamp;

  /**
   * @var string
   */
  protected $revisionLog;

  /**
   * @var bool
   */
  private $allowRevertRevision;

  /**
   * @var bool
   */
  private $allowDeleteRevision;

  /**
   * @var string[]
   */
  static $ROUTE_TEMPLATES = array(
    'view'   => 'revision',
    'delete' => 'revision_delete_confirm',
    'revert' => 'revision_revert_confirm',
    'revertTranslation' => 'revision_revert_translation_confirm'
  );

  /**
   * @var string[]
   */
  static $LABEL_TEMPLATES = array(
    0 => '{% trans %}Revision {{ id }}{% endtrans %}',
    1 => '{% trans %}Revision {{ id }} from {{ date }}{% endtrans %}',
    2 => '{% trans %}Revision {{ id }} by {{ username }}{% endtrans %}',
    3 => '{% trans %}{{ date }} by {{ username }}{% endtrans %}'
  );



  /**
   * Create a new RevisionController instance.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   */
  public function __construct(ContainerInterface $container) {
    $this->setContainer($container);

    $manager = $this->languageManager();
    $language = $manager->getCurrentLanguage(LanguageInterface::TYPE_CONTENT);

    $this->langCode = $language->getId();
    $this->langName = $language->getName();
  }


  /**
   * Instantiates a new instance of this class.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static($container);
  }


  /**
   * Generates an overview table of older revisions of an entity.
   *
   * @return array
   */
  public function versions() {
    $entity = $this->resolveEntityFormRoute();
    if (is_null($entity)) {
      throw new \InvalidArgumentException();
    }

    $this->setEntity($entity);

    return array(
      '#title' => $this->buildTitle(),
      'table'  => $this->buildTable(),
    );
  }


  protected function buildTitle() {
    $params = array(
      '%title' => $this->entity->label()
    );

    if ($this->hasTranslations) {
      $params['@langname'] = $this->langName;
      return $this->t('@langname revisions for %title', $params);
    } else {
      return $this->t('Revisions for %title', $params);
    }
  }


  /**
   * @return array
   */
  protected function buildTable() {
    return array(
      '#theme'    => 'table',
      '#header'   => $this->buildHeaders(),
      '#rows'     => $this->buildRows(),
      '#attached' => array(
        'library'   => array('node/drupal.node.admin'),
      )
    );
  }


  /**
   * @return array
   */
  protected function buildHeaders() {
    return array(
      'revision'   => $this->t('Revision'),
      'operations' => $this->t('Operations')
    );
  }


  /**
   * @return array
   */
  protected function buildRows() {
    $langCode  = $this->langCode;
    $rows      = array();
    $isCurrent = TRUE;

    foreach ($this->getRevisions() as $id => $revision) {
      if (!$revision->hasTranslation($langCode)) {
        continue;
      }

      if (!$revision->getTranslation($langCode)->isRevisionTranslationAffected()) {
        continue;
      }

      $row = $this->buildRow($revision, $id, $isCurrent);

      if ($isCurrent) {
        $isCurrent = FALSE;
        foreach ($row as &$column) {
          $column['class'] = ['revision-current'];
        }
      }

      $rows[] = $row;
    }

    return $rows;
  }


  /**
   * @param Entity $revision
   * @param int $revisionID
   * @param bool $isCurrent
   * @return array
   */
  private function buildRow(Entity $revision, $revisionID, $isCurrent) {
    return array(
      'revision'   => array(
        'data'     => $this->buildRowLabel($revision, $revisionID, $isCurrent),
      ),
      'operations' => array(
        'data'     => $this->buildRowOperations($revision, $revisionID, $isCurrent)
      )
    );
  }


  /**
   * @param Entity $revision
   * @param int $revisionID
   * @param bool $isCurrent
   * @return array
   */
  protected function buildRowLabel(Entity $revision, $revisionID, $isCurrent) {
    $templateMask = 0;
    $context = array(
      'id' => $revisionID
    );

    if (isset($this->revisionTimestamp)) {
      $date = $revision->get($this->revisionTimestamp)->value;
      $date = $this->getDateFormatter()->format($date, 'short');

      if ($revisionID != $this->entity->getRevisionId()) {
        $url = $this->getUrl('view', $revisionID);

        if (!is_null($url)) {
          $date = $this->l($date, $url);
        }
      } else {
        $date = $this->entity->link($date);
      }

      $templateMask |= 1;
      $context['date'] = $date;
    }

    if (isset($this->revisionUid)) {
      $data = array(
        '#theme'   => 'username',
        '#account' => $revision->get($this->revisionUid)->entity,
      );

      $templateMask |= 2;
      $context['username'] = $this->getRenderer()->renderPlain($data);
    }

    $template = self::$LABEL_TEMPLATES[$templateMask];

    if (isset($this->revisionLog)) {
      $template .= '{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}';
      $data = $revision->get($this->revisionLog)->view();
      $context['message'] = $this->getRenderer()->renderPlain($data);
    }

    return array(
      '#type'     => 'inline_template',
      '#template' => $template,
      '#context'  => $context
    );
  }


  /**
   * @param Entity $revision
   * @param int $revisionID
   * @param bool $isCurrent
   * @return array
   */
  private function buildRowOperations(Entity $revision, $revisionID, $isCurrent) {
    if ($isCurrent) {
      return array(
        '#prefix' => '<em>',
        '#markup' => $this->t('Current revision'),
        '#suffix' => '</em>',
      );
    }

    $links = array();
    if ($this->allowRevertRevision()) {
      if ($this->hasTranslations) {
        $url = $this->getUrl('revertTranslation', $revisionID, array('langcode' => $this->langCode));
      } else {
        $url = $this->getUrl('revert', $revisionID);
      }

      if (!is_null($url)) {
        $links['revert'] = array(
          'title' => $this->t('Revert'),
          'url'   => $url
        );
      }
    }

    if ($this->allowDeleteRevision()) {
      $url = $this->getUrl('delete', $revisionID);

      if (!is_null($url)) {
        $links['delete'] = array(
          'title' => $this->t('Delete'),
          'url'   => $url
        );
      }
    }

    return array(
      '#type' => 'operations',
      '#links' => $links,
    );
  }


  /**
   * @param Entity $entity
   */
  protected function setEntity(Entity $entity) {
    $this->entity = $entity;
    $entityType   = $this->entity->getEntityType();
    if (!$entityType instanceof ManagedEntityType) {
      throw new \InvalidArgumentException();
    }

    foreach ($entityType->getFields() as $field) {
      switch ($field->getEntityKey()) {
        case 'revision_uid':
          $this->revisionUid = $field->getName();
          break;
        case 'revision_timestamp':
          $this->revisionTimestamp = $field->getName();
          break;
        case 'revision_log':
          $this->revisionLog = $field->getName();
          break;
      }
    }

    $routeProvider = $this->getRouteProvider();
    foreach (self::$ROUTE_TEMPLATES as $key => $routeName) {
      $routeName = implode('.', array('entity', $entityType->id(), $routeName));
      $routes = $routeProvider->getRoutesByNames(array($routeName));

      if (count($routes) > 0) {
        $this->routes[$key] = $routeName;
      } else {
        $this->routes[$key] = NULL;
      }
    }

    $languages = $entity->getTranslationLanguages();
    $this->hasTranslations = (count($languages) > 1);
  }


  /**
   * @return int[]
   */
  protected function getRevisionIDs() {
    $entityType    = $this->entity->getEntityType();
    $idKey         = $entityType->getKey('id');
    $revisionKey   = $entityType->getKey('revision');
    $revisionTable = $entityType->getRevisionTable();

    return $this->getDatabase()->query(
      "SELECT {$revisionKey} FROM {$revisionTable} WHERE {$idKey}=:id ORDER BY {$revisionKey}",
      array(':id' => $this->entity->id())
    )->fetchCol();
  }


  /**
   * @return \Drupal\managed\Entity[]
   */
  protected function getRevisions() {
    $entityTypeID = $this->entity->getEntityTypeId();
    $storage      = $this->entityManager()->getStorage($entityTypeID);
    $ids          = $this->getRevisionIDs();

    $revisions   = array();
    foreach (array_reverse($ids) as $id) {
      $revisions[$id] = $storage->loadRevision($id);
    }

    return $revisions;
  }


  /**
   * @param string $name
   * @param int $revisionID
   * @param array $params
   * @return Url|null
   */
  protected function getUrl($name, $revisionID, $params = array()) {
    if (is_null($this->routes[$name])) {
      return NULL;
    }

    $entityTypeID = $this->entity->getEntityTypeId();
    $params += array(
      $entityTypeID => $this->entity->id(),
      'revision'    => $revisionID
    );

    return new Url($this->routes[$name], $params);
  }


  /**
   * @return \Drupal\Core\Render\Renderer
   */
  protected function getRenderer() {
    return $this->getService('renderer');
  }


  /**
   * @return \Drupal\Core\Routing\RouteProvider
   */
  protected function getRouteProvider() {
    return $this->getService('router.route_provider');
  }


  /**
   * @return \Drupal\Core\Datetime\DateFormatter
   */
  protected function getDateFormatter() {
    return $this->getService('date.formatter');
  }


  /**
   * @return \Drupal\Core\Database\Connection
   */
  protected function getDatabase() {
    return $this->getService('database');
  }


  /**
   * @return bool
   */
  private function allowRevertRevision() {
    if (!isset($this->allowRevertRevision)) {
      $this->allowRevertRevision = $this->entity->access('update');
    }

    return $this->allowRevertRevision;
  }


  /**
   * @return bool
   */
  private function allowDeleteRevision() {
    if (!isset($this->allowDeleteRevision)) {
      $this->allowDeleteRevision = $this->entity->access('delete');
    }

    return $this->allowDeleteRevision;
  }
}
