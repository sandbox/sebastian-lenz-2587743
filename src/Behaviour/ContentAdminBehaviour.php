<?php

namespace Drupal\managed\Behaviour;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\managed\Behaviour\Feature\AddBundleFeature;
use Drupal\managed\Behaviour\Feature\AddFeature;
use Drupal\managed\Behaviour\Feature\ContentAdminFeature;
use Drupal\managed\Behaviour\Feature\DeleteFeature;
use Drupal\managed\Behaviour\Feature\EditFeature;
use Drupal\managed\Behaviour\Feature\RedirectViewFeature;
use Drupal\managed\Behaviour\Feature\RevisionFeature;
use Drupal\managed\Core\ManagedEntityType;


/**
 * Creates an admin interface located in a tab within the `Content` section.
 */
class ContentAdminBehaviour extends AbstractBehaviour
{
  /**
   * Create a new ContentAdminBehaviour instance.
   *
   * @param \Drupal\managed\Core\ManagedEntityType $entityType
   */
  public function __construct(ManagedEntityType $entityType) {
    parent::__construct($entityType);

    $this->features[] = new ContentAdminFeature($this);
    $this->features[] = new RedirectViewFeature($this);
    $this->features[] = new EditFeature($this);
    $this->features[] = new DeleteFeature($this);

    if ($entityType->isRevisionable()) {
      $this->features[] = new RevisionFeature($this);
    }

    if ($entityType->hasBundles()) {
      $this->features[] = new AddBundleFeature($this);
    } else {
      $this->features[] = new AddFeature($this);
    }
  }


  /**
   * Return the behaviour annotation of this behaviour.
   *
   * @return \Drupal\managed\Annotation\Behaviour\ContentAdminBehaviourAnnotation
   */
  public function getBehaviourAnnotation() {
    return $this->entityType->getBehaviour();
  }


  /**
   * Return the required permission for the given action.
   *
   * @param int $action
   * @return null|string
   */
  public function getPermission($action) {
    $annotation  = $this->getBehaviourAnnotation();
    $permission  = $annotation->getAdminPermission($this->entityType->id());

    if (!empty($permission)) {
      return $permission;
    } else {
      return 'administer managed entities';
    }
  }


  /**
   * Return the permissions defined by this behaviour.
   *
   * @return array
   */
  public function onPermissions() {
    $permissions = array();
    $annotation  = $this->getBehaviourAnnotation();
    $permission  = $annotation->getAdminPermission($this->entityType->id());

    if (!empty($permission)) {
      $permissions[$permission] = array(
        'title' => new TranslatableMarkup(
          'Administer @entity',
          array('@entity' => $this->entityType->getPluralLabel())
        )
      );
    }

    return $permissions;
  }
}
