<?php

namespace Drupal\managed\Behaviour;


/**
 * Creates an admin interface located in a tab within the `Content` section
 * and sets up frontend urls for each entity.
 */
class ContentBehaviour extends ContentAdminBehaviour
{
  /**
   * Return the name of the class implementing the behaviour.
   *
   * @return string
   */
  public function getBehaviourClass() {
    return 'Drupal\managed\Behaviour\ContentBehaviour';
  }
}
