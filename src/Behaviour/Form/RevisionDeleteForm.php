<?php

namespace Drupal\managed\Behaviour\Form;

use Drupal\Core\Form\FormStateInterface;


/**
 * Provides a form for deleting a revision.
 */
class RevisionDeleteForm extends AbstractRevisionForm
{
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->getStorage()->deleteRevision($this->revision->getRevisionId());

    drupal_set_message($this->getSuccess());

    $entityTypeID = $this->getEntityTypeID();
    $params = array(
      $entityTypeID => $this->revision->id()
    );

    if ($this->getRemainingRevisions() > 1) {
      $form_state->setRedirect(implode('.', array('entity', $entityTypeID, 'version_history')), $params);
    } else {
      $form_state->setRedirect(implode('.', array('entity', $entityTypeID, 'canonical')), $params);
    }
  }


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return $this->getEntityTypeID() . '_revision_delete_confirm';
  }


  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    $date = $this->getRevisionDate();

    if (is_null($date)) {
      return t(
        'Are you sure you want to delete the revision %revision-id?',
        array('%revision-id' => $this->revision->getRevisionId())
      );
    } else {
      return t(
        'Are you sure you want to delete the revision from %revision-date?',
        array('%revision-date' => $date)
      );
    }
  }


  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return t('Delete');
  }


  protected function getSuccess() {
    $date   = $this->getRevisionDate();
    $params = array(
      '%date'  => $date,
      '%id'    => $this->revision->getRevisionId(),
      '%title' => $this->revision->label(),
      '@type'  => $this->getRevisionTypeLabel()
    );

    if (is_null($date)) {
      return t('Revision %id of @type %title has been deleted.', $params);
    } else {
      return t('Revision from %date of @type %title has been deleted.', $params);
    }
  }
}
