<?php

namespace Drupal\managed\Behaviour\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\managed\Entity;


/**
 * Provides a form for reverting a revision for a single translation.
 */
class RevisionRevertTranslationForm extends RevisionRevertForm
{
  /**
   * The language to be reverted.
   *
   * @var string
   */
  protected $langCode;



  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $revision = NULL, $langcode = NULL) {
    $this->langCode = $langcode;
    $form = parent::buildForm($form, $form_state, $revision);

    $form['revert_untranslated_fields'] = array(
      '#type'  => 'checkbox',
      '#title' => $this->t('Revert content shared among translations'),
      '#default_value' => FALSE,
    );

    return $form;
  }


  /**
   * {@inheritdoc}
   */
  protected function prepareRevertedRevision(Entity $revision, FormStateInterface $formState) {
    $revertUntranslatedFields = $formState->getValue('revert_untranslated_fields');

    /** @var \Drupal\managed\Entity $current */
    $current = $this->getStorage()->load($revision->id());

    /** @var \Drupal\managed\Entity $currentTranslation */
    $currentTranslation = $current->getTranslation($this->langCode);

    /** @var \Drupal\managed\Entity $revisionTranslation */
    $revisionTranslation = $revision->getTranslation($this->langCode);

    foreach ($currentTranslation->getFieldDefinitions() as $fieldName => $definition) {
      if ($definition->isTranslatable() || $revertUntranslatedFields) {
        $currentTranslation->set($fieldName, $revisionTranslation->get($fieldName)->getValue());
      }
    }

    $currentTranslation->setNewRevision();
    $currentTranslation->isDefaultRevision(TRUE);

    return $currentTranslation;
  }


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return $this->getEntityTypeID() . '_revision_revert_translation_confirm';
  }


  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    $date   = $this->getRevisionDate();
    $params = array(
      '@language' => $this->getLanguageManager()->getLanguageName($this->langCode),
      '%id'       => $this->revision->getRevisionId(),
      '%date'     => $date
    );

    if (is_null($date)) {
      return t('Are you sure you want to revert @language translation to the revision %id?', $params);
    } else {
      return t('Are you sure you want to revert @language translation to the revision from %date?', $params);
    }
  }


  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return '';
  }


  /**
   * The language manager.
   *
   * @return \Drupal\Core\Language\LanguageManagerInterface
   */
  protected function getLanguageManager() {
    return $this->getService('language_manager');
  }
}
