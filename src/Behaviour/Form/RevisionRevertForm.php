<?php

namespace Drupal\managed\Behaviour\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\managed\Entity;


/**
 * Provides a form for reverting a revision.
 */
class RevisionRevertForm extends AbstractRevisionForm
{
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // The revision timestamp will be updated when the revision is saved. Keep
    // the original one for the confirmation message.
    $success    = $this->getSuccess();
    $logMessage = $this->getLogMessage();
    $revision   = $this->revision;

    $revision = $this->prepareRevertedRevision($revision, $form_state);
    $revision = $this->trySetLogMessage($revision, $logMessage);
    $revision->save();

    drupal_set_message($success);

    $entityTypeID = $this->getEntityTypeID();
    $routeName    = implode('.', array('entity', $entityTypeID, 'version_history'));
    $params       = array(
      $entityTypeID => $this->getEntity()->id()
    );

    $form_state->setRedirect($routeName, $params);
  }


  /**
   * Prepares a revision to be reverted.
   *
   * @param \Drupal\managed\Entity $revision
   *   The revision to be reverted.
   * @param FormStateInterface $formState
   * @return \Drupal\managed\Entity $revision
   *   The prepared revision ready to be stored.
   */
  protected function prepareRevertedRevision(Entity $revision, FormStateInterface $formState) {
    $revision->setNewRevision();
    $revision->isDefaultRevision(TRUE);

    return $revision;
  }


  /**
   * @param \Drupal\managed\Entity $revision
   * @param string $logMessage
   * @return \Drupal\managed\Entity
   */
  protected function trySetLogMessage(Entity $revision, $logMessage) {
    foreach ($this->getEntityType()->getFields() as $field) {
      if ($field->getEntityKey() != 'revision_log') {
        continue;
      }

      $revision->set($field->getName(), $logMessage);
      break;
    }

    return $revision;
  }


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return $this->getEntityTypeID() . '_revision_revert_confirm';
  }


  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    $date = $this->getRevisionDate();
    if (is_null($date)) {
      return t(
        'Are you sure you want to revert to the revision %id?',
        array('%id' => $this->revision->getRevisionId())
      );
    } else {
      return t(
        'Are you sure you want to revert to the revision from %date?',
        array('%date' => $date)
      );
    }
  }


  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return t('Revert');
  }


  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return '';
  }


  /**
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup|string
   */
  private function getSuccess() {
    $date   = $this->getRevisionDate();
    $params = array(
      '%date'  => $date,
      '%id'    => $this->revision->getRevisionId(),
      '%title' => $this->revision->label(),
      '@type'  => $this->getRevisionTypeLabel()
    );

    if (is_null($date)) {
      return t('@type %title has been reverted to the revision %id.', $params);
    } else {
      return t('@type %title has been reverted to the revision from %date.', $params);
    }
  }


  /**
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup|string
   */
  private function getLogMessage() {
    $date = $this->getRevisionDate();

    if (is_null($date)) {
      return t('Copy of the revision %id.', array('%id' => $this->revision->getRevisionId()));
    } else {
      return t('Copy of the revision from %date.', array('%date' => $date));
    }
  }
}
