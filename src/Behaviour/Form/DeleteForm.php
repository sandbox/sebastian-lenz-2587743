<?php

namespace Drupal\managed\Behaviour\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;


class DeleteForm extends ContentEntityDeleteForm
{
  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $this->getEntity();

    if ($entity->isDefaultTranslation() || !$entity->hasLinkTemplate('canonical')) {
      return $this->traitGetCancelUrl();
    } else {
      return $entity->urlInfo('canonical');
    }
  }
}
