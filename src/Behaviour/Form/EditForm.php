<?php

namespace Drupal\managed\Behaviour\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\managed\Core\ManagedEntityType;
use Drupal\managed\Entity;


class EditForm extends ContentEntityForm
{
  /**
   * Form submission handler for the 'save' action.
   *
   * Normally this method should be overridden to provide specific messages to
   * the user and redirect the form after the entity has been saved.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The current state of the form.
   *
   * @return int
   *   Either SAVED_NEW or SAVED_UPDATED, depending on the operation performed.
   */
  public function save(array $form, FormStateInterface $formState) {
    if($this->entity->isNew()) {
      $this->prepareRevision();
    }

    try {
      $result = $this->entity->save();
    }
    catch (\Exception $exception) {
      $this->raiseSaveErrorMessage($exception);
      $formState->setRebuild();
      return FALSE;
    }

    $this->raiseSaveSuccessMessage($result);

    $route  = $this->getRouteMatch()->getRouteObject();
    $target = $route->getOption('redirect_target');
    if (is_null($target)) {
      $url = $this->entity->urlInfo();
    } else {
      $url = $this->entity->urlInfo($target);
    }

    $formState->setRedirectUrl($url);

    return $result;
  }


  public function prepareRevision() {
    if (!($this->entity instanceof Entity)) {
      throw new \InvalidArgumentException();
    }

    $entityType = $this->entity->getEntityType();
    if (!$entityType instanceof ManagedEntityType) {
      throw new \InvalidArgumentException();
    }

    foreach ($entityType->getFields() as $field) {
      switch ($field->getEntityKey()) {
        case 'revision_uid':
          $this->entity->set($field->getName(), \Drupal::currentUser()->id());
          break;
        case 'revision_timestamp':
          $this->entity->set($field->getName(), REQUEST_TIME);
          break;
      }
    }

    $this->entity->setNewRevision();
  }


  /**
   * Returns an array of supported actions for the current entity form.
   *
   * @param array $form
   * @param FormStateInterface $form_state
   * @return array
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);

    $entityType = $this->entity->getEntityType();
    if ($entityType->isRevisionable() && !$this->entity->isNew()) {
      $actions['submitRevision'] = array(
        '#type'   => 'submit',
        '#value'  => $this->t('Save to new revision'),
        '#submit' => array('::submitForm', '::prepareRevision', '::save')
      );
    }

    return $actions;
  }


  protected function raiseSaveErrorMessage(\Exception $exception) {
    drupal_set_message($this->t(
      '@type %title could not be saved: %message',
      array(
        '@type'    => $this->entity->getEntityType()->getLabel(),
        '%title'   => $this->entity->label(),
        '%message' => $exception->getMessage()
      )
    ), 'error');
  }


  private function raiseSaveSuccessMessage($result) {
    $arguments = array(
      '@type'  => $this->entity->getEntityType()->getLabel(),
      '%title' => $this->entity->label(),
    );

    if ($result == SAVED_NEW) {
      drupal_set_message(t('@type %title has been created.', $arguments));
    } else {
      drupal_set_message(t('@type %title has been updated.', $arguments));
    }
  }
}
