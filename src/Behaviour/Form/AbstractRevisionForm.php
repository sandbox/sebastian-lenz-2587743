<?php

namespace Drupal\managed\Behaviour\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\managed\Utils\ContainerAwareTrait;
use Drupal\managed\Utils\EntityAwareRouteTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Provides a form for deleting a revision.
 */
abstract class AbstractRevisionForm extends ConfirmFormBase
{
  use ContainerAwareTrait;
  use EntityAwareRouteTrait;

  /**
   * The entity revision.
   *
   * @var \Drupal\managed\Entity
   */
  protected $revision;



  /**
   * Create a new RevisionDeleteForm instance.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container this instance should use.
   */
  public function __construct(ContainerInterface $container) {
    $this->setContainer($container);
  }


  /**
   * Instantiates a new instance of this class.
   *
   * @param ContainerInterface $container
   *   The service container this instance should use.
   * @return static
   */
  static function create(ContainerInterface $container) {
    return new static($container);
  }


  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $revision = NULL) {
    $this->revision = $this->getStorage()->loadRevision($revision);

    return parent::buildForm($form, $form_state);
  }


  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    $entityTypeID = $this->getEntityTypeID();
    $routeName    = implode('.', array('entity', $entityTypeID, 'version_history'));

    return new Url($routeName, array(
      $entityTypeID => $this->getEntity()->id()
    ));
  }


  /**
   * @return int
   */
  protected function getRemainingRevisions() {
    $entityType    = $this->getEntityType();
    $revisionTable = $entityType->getRevisionTable();
    $idKey         = $entityType->getKey('id');
    $revisionKey   = $entityType->getKey('revision');

    return $this->getDatabase()
      ->query(
        "SELECT COUNT(DISTINCT {$revisionKey}) FROM {$revisionTable} WHERE {$idKey} = :id",
        array(':id' => $this->revision->id())
      )->fetchField();
  }


  /**
   * @return null|string
   */
  protected function getRevisionDate() {
    foreach ($this->getEntityType()->getFields() as $field) {
      if ($field->getEntityKey() != 'revision_timestamp') {
        continue;
      }

      $date = $this->revision->get($field->getName())->value;
      return $this->getDateFormatter()->format($date);
    }

    return NULL;
  }


  /**
   * @return string
   */
  protected function getRevisionTypeLabel() {
    $entityType = $this->getEntityType();
    $bundle     = $entityType->getBundle($this->revision->bundle());

    if (is_null($bundle)) {
      return $entityType->getLabel();
    } else {
      return $bundle->getLabel();
    }
  }


  /**
   * @return \Drupal\Core\Database\Connection
   */
  protected function getDatabase() {
    return $this->getService('database');
  }


  /**
   * @return \Drupal\Core\Entity\EntityStorageInterface|object
   */
  protected function getStorage() {
    return $this->getEntityManager()->getStorage($this->getEntityTypeID());
  }


  /**
   * @return \Drupal\Core\Entity\EntityManager
   */
  protected function getEntityManager() {
    return $this->getService('entity.manager');
  }


  /**
   * @return \Drupal\Core\Datetime\DateFormatter
   */
  protected function getDateFormatter() {
    return $this->getService('date.formatter');
  }
}
