<?php

namespace Drupal\managed\Behaviour;

use Drupal\managed\Behaviour\Feature\AbstractAdminFeature;
use Drupal\managed\Behaviour\Feature\AbstractCanonicalFeature;
use Drupal\managed\Core\ManagedEntityType;
use Symfony\Component\Routing\RouteCollection;


/**
 * Base class of all behaviours.
 */
abstract class AbstractBehaviour
{
  /**
   * The entity type this behaviour should handle.
   *
   * @var \Drupal\managed\Core\ManagedEntityType
   */
  protected $entityType;

  /**
   * A list of all features attached to this behaviour.
   *
   * @var \Drupal\managed\Behaviour\Feature\AbstractFeature[]
   */
  protected $features = array();

  /**
   * Defines a view permission lookup, used with AbstractBehaviour::getPermission().
   */
  const PERMISSION_VIEW = 'view';

  /**
   * Defines a collection permission lookup, used with AbstractBehaviour::getPermission().
   */
  const PERMISSION_COLLECTION = 'collection';

  /**
   * Defines a add permission lookup, used with AbstractBehaviour::getPermission().
   */
  const PERMISSION_ADD = 'add';

  /**
   * Defines a edit permission lookup, used with AbstractBehaviour::getPermission().
   */
  const PERMISSION_EDIT = 'edit';

  /**
   * Defines a delete permission lookup, used with AbstractBehaviour::getPermission().
   */
  const PERMISSION_DELETE = 'delete';

  const PERMISSION_REVISION_LIST = 'revisionList';

  const PERMISSION_REVISION_REVERT = 'revisionRevert';

  const PERMISSION_REVISION_DELETE = 'revisionDelete';



  /**
   * Create a new AbstractBehaviour instance.
   *
   * @param \Drupal\managed\Core\ManagedEntityType $entityType
   */
  public function __construct(ManagedEntityType $entityType) {
    $this->entityType = $entityType;
  }


  /**
   *
   */
  public function initializeEntityType() {
    $entityType = $this->entityType;

    foreach ($this->features as $feature) {
      $feature->initializeEntityType($entityType);
    }

    if ($entityType->isTranslatable() && !$entityType->hasLinkTemplate('canonical')) {
      $entityType->set('content_translation_ui_skip', TRUE);
    }
  }


  /**
   * Return the base route name for this behaviour.
   *
   * All routes generated should start with this base name to
   * avoid any naming collisions. The base route has no trailing dot.
   *
   * @return string
   */
  public function getBaseRouteName() {
    return 'entity.' . $this->entityType->id();
  }


  /**
   * Return the base path for all admin pages.
   *
   * The base path has no trailing slash.
   *
   * @return string
   */
  public function getBaseAdminPath() {
    return '/admin/content/' . $this->entityType->id();
  }


  /**
   * Return the entity type this behaviour should handle.
   *
   * @return \Drupal\managed\Core\ManagedEntityType
   */
  public function getEntityType() {
    return $this->entityType;
  }


  /**
   * Return the required permission for the given action.
   *
   * @param string $action
   *   The action whose permission should be returned. Must be one of
   *   the `PERMISSION_*` constants.
   * @return null|string
   *   The name of the desired permission.
   */
  abstract public function getPermission($action);


  /**
   * Return the admin feature attached to this behaviour.
   *
   * @return \Drupal\managed\Behaviour\Feature\AbstractAdminFeature
   */
  public function getAdminFeature() {
    foreach ($this->features as $feature) {
      if ($feature instanceof AbstractAdminFeature) {
        return $feature;
      }
    }

    return NULL;
  }


  /**
   * Return the canonical feature attached to this behaviour.
   *
   * @return \Drupal\managed\Behaviour\Feature\AbstractCanonicalFeature
   */
  public function getCanonicalFeature() {
    foreach ($this->features as $feature) {
      if ($feature instanceof AbstractCanonicalFeature) {
        return $feature;
      }
    }

    return NULL;
  }


  /**
   * Return the action links declared by this behaviour.
   *
   * @return array
   */
  public function getActionLinks() {
    $links    = array();
    $baseName = 'managed.action.behaviour.' . $this->entityType->id();

    foreach ($this->features as $feature) {
      $links += $feature->getActionLinks($baseName);
    }

    return $links;
  }


  /**
   * Return the menu links declared by this behaviour.
   *
   * @return array
   */
  public function getMenuLinks() {
    $links    = array();
    $baseName = 'managed.menu.behaviour.' . $this->entityType->id();

    foreach ($this->features as $feature) {
      $links += $feature->getMenuLinks($baseName);
    }

    return $links;
  }


  /**
   * Return the task links declared by this behaviour.
   *
   * @return array
   */
  public function getTaskLinks() {
    $links    = array();
    $baseName = 'managed.task.behaviour.' . $this->entityType->id();

    foreach ($this->features as $feature) {
      $links += $feature->getTaskLinks($baseName);
    }

    return $links;
  }


  /**
   * Return the contextual links declared by this behaviour.
   *
   * @return array
   */
  public function getContextualLinks() {
    $links    = array();
    $baseName = 'managed.contextual.behaviour.' . $this->entityType->id();

    foreach ($this->features as $feature) {
      $links += $feature->getContextualLinks($baseName);
    }

    return $links;
  }


  /**
   * Return the permissions defined by this behaviour.
   *
   * @return array
   */
  public function onPermissions() {
    return array();
  }


  /**
   * Allow this behaviour to modify the route collection.
   *
   * @param \Symfony\Component\Routing\RouteCollection $collection
   */
  public function onAlterRoutes(RouteCollection $collection) {
    foreach ($this->features as $feature) {
      $feature->onAlterRoutes($collection);
    }
  }
}
