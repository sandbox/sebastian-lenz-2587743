<?php

namespace Drupal\managed\Console;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\managed\Console\View\EntityTableView;
use Drupal\managed\Utils\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * The controller that manages the requests for the backend console
 * of the module `managed`.
 *
 * The console is located under `Structure` > `Managed entities`.
 */
class Controller extends ControllerBase
{
  use ContainerAwareTrait;



  /**
   * Create a new Controller instance.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container this instance should use.
   */
  public function __construct(ContainerInterface $container) {
    $this->setContainer($container);
  }


  /**
   * Instantiates a new instance of this class.
   *
   * @param ContainerInterface $container
   *   The service container this instance should use.
   * @return Controller
   */
  static function create(ContainerInterface $container) {
    return new Controller($container);
  }


  /**
   * The index action showing a table of all managed entities.
   *
   * @return array
   */
  public function index() {
    $manager = $this->getManagedEntityTypeManager();

    $this->checkPendingInstalls();
    $this->checkPendingUninstalls();

    $view = new EntityTableView($this->getContainer());
    $view->setEntityTypes($manager->getAllManagedTypes());
    $view->setInstalledBundleIDs(array_keys($this->getPersistentData()->getInstalledBundles()));

    return $view->build();
  }


  public function details() {
    return array();
  }


  /**
   * Perform the install action.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function install() {
    $installer = $this->getInstallerHost();
    $installer->install();

    $markup = $installer->getPerformedActionsMarkup();
    if ($markup == FALSE) {
      $markup = $this->t('Everything is up to date.');
    } else {
      $markup = $this->t('The following operations have been performed:') . $markup;
    }

    drupal_set_message(Markup::create($markup));
    return $this->redirect('managed.console');
  }


  /**
   * Test for pending install actions and create a message if there are any
   * pending operations.
   *
   * @return $this
   */
  private function checkPendingInstalls() {
    $installer = $this->getInstallerHost();

    $install = $installer->getPendingInstallInfo();
    if ($install !== FALSE) {
      $install .= ' ' . $this->t(
        'You should <a href="@update-url">update all managed entities</a>.',
        array('@update-url' => Url::fromRoute('managed.console.install')->toString())
      );

      drupal_set_message(Markup::create($install), 'error');
    }

    return $this;
  }


  /**
   * Test for pending uninstall actions and create a message if there are any
   * pending operations.
   *
   * @return $this
   */
  private function checkPendingUninstalls() {
    $installer = $this->getInstallerHost();

    $uninstall = $installer->getPendingUninstallInfo();
    if ($uninstall !== FALSE) {
      $uninstall .= ' ' . $this->t(
        'You could <a href="@update-url">clean up definitions</a>.',
        array('@update-url' => Url::fromRoute('managed.console.uninstall')->toString())
      );

      drupal_set_message(Markup::create($uninstall), 'warning');
    }

    return $this;
  }


  /**
   * Return an instance of the discovery service.
   *
   * @return \Drupal\managed\Discovery\Discovery
   */
  public function getDiscovery() {
    return $this->getService('managed.discovery');
  }


  /**
   * Return an instance of the discovery service.
   *
   * @return \Drupal\managed\Core\ManagedEntityTypeManager
   */
  public function getManagedEntityTypeManager() {
    return $this->getService('managed.entity_manager');
  }


  /**
   * Return the installer host used to perform install and uninstall
   * operations.
   *
   * @return \Drupal\managed\Core\InstallerHost
   */
  private function getInstallerHost() {
    return $this->getService('managed.installer');
  }


  /**
   * Return the persistent data storage.
   *
   * @return \Drupal\managed\Core\PersistentData
   */
  private function getPersistentData() {
    return $this->getService('managed.persistent');
  }
}
