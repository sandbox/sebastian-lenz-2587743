<?php

namespace Drupal\managed\Console\View;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\managed\Utils\ContainerAware;
use Drupal\node\Entity\NodeType;


/**
 * Render the entity table view.
 */
class EntityTableView extends ContainerAware
{
  use StringTranslationTrait;

  /**
   * A list of all entities that should be displayed by this table.
   *
   * @var \Drupal\managed\Core\ManagedEntityType[]
   */
  private $entityTypes;

  private $installedBundleIDs;



  /**
   * @return \Drupal\Core\Extension\ModuleHandler
   */
  public function getModuleHandler() {
    return $this->getService('module_handler');
  }


  /**
   * @return \Drupal\Core\Entity\EntityManager
   */
  public function getEntityManager() {
    return $this->getService('entity.manager');
  }


  /**
   * Set the list of entities that should be displayed by this table.
   *
   * @param $entityTypes \Drupal\managed\Core\ManagedEntityType[]
   * @return $this
   */
  public function setEntityTypes($entityTypes) {
    $this->entityTypes = $entityTypes;
    return $this;
  }


  public function setInstalledBundleIDs($installedBundleIDs) {
    $this->installedBundleIDs = $installedBundleIDs;
  }


  /**
   * Return the render array of the entity table.
   *
   * @return array
   */
  public function build() {
    return array(
      '#type'   => 'table',
      '#header' => $this->buildTableHeader(),
      '#empty'  => $this->buildEmptyMessage(),
    ) + $this->buildTableRows();
  }


  /**
   * Return the table header definitions.
   *
   * @return array
   */
  protected function buildTableHeader() {
    return array(
      'label'      => $this->t('Label'),
      'name'       => $this->t('Machine name'),
      'operations' => $this->t('Operations')
    );
  }


  /**
   * Return the rows of the table.
   *
   * @return array
   */
  protected function buildTableRows() {
    $rows = array();
    foreach ($this->entityTypes as $entityType) {
      $rows[] = $this->buildEntityTableRow($entityType);

      foreach ($entityType->getBundles() as $bundle) {
        $rows[] = $this->buildBundleTableRow($entityType, $bundle);
      }
    }

    return array_filter($rows);
  }


  /**
   * Return a row for the given entity.
   *
   * @param \Drupal\managed\Core\ManagedEntityType $entityType
   * @return array
   */
  protected function buildEntityTableRow($entityType) {
    return array(
      'label' => array(
        '#plain_text' => $entityType->getLabel(),
      ),
      'name' => array(
        '#plain_text' => $entityType->id(),
      ),
      'operations' => $entityType->hasBundles()
        ? array('#plain_text' => '')
        : $this->buildEntityOperations($entityType)
    );
  }


  /**
   * Return a row for the given bundle.
   *
   * @param \Drupal\managed\Core\ManagedEntityType $entityType
   * @param \Drupal\managed\Annotation\BundleAnnotation $bundle
   * @return array
   */
  private function buildBundleTableRow($entityType, $bundle) {
    $id = $entityType->id() . ':' . $bundle->id();

    if ($entityType->id() == 'node') {
      $storage = $this->getEntityManager()->getStorage('node_type');
      $nodeType = $storage->load($bundle->id());
      if (!($nodeType instanceof NodeType)) {
        return NULL;
      }
    }
    else {
      if (isset($this->installedBundleIDs) && !in_array($id, $this->installedBundleIDs)) {
        return null;
      }
    }

    return array(
      'label' => array(
        '#wrapper_attributes' => array(
          'style' => 'padding-left:32px;',
        ),
        '#plain_text' => $bundle->getLabel()
      ),
      'name' => array(
        '#plain_text' => $id,
      ),
      'operations' => isset($nodeType)
        ? $this->buildNodeOperations($nodeType)
        : $this->buildEntityOperations($entityType, $bundle)
    );
  }


  /**
   * @param \Drupal\managed\Core\ManagedEntityType $entityType
   * @param \Drupal\managed\Annotation\BundleAnnotation $bundle
   * @return array
   */
  private function buildEntityOperations($entityType, $bundle = NULL) {
    $entityTypeID = $entityType->id();

    $parameters = array();
    if (!is_null($bundle)) {
      $parameters['bundle'] = $bundle->id();
    }

    $links = array(
      'manage_fields' => array(
        'title'  => $this->t('Manage fields'),
        'url'    => Url::fromRoute("entity.{$entityTypeID}.field_ui_fields", $parameters),
      ),
      'manage_form_display' => array(
        'title'  => $this->t('Manage form display'),
        'url'    => Url::fromRoute("entity.entity_form_display.{$entityTypeID}.default", $parameters),
      ),
      'manage_view_display' => array(
        'title'  => $this->t('Manage display'),
        'url'    => Url::fromRoute("entity.entity_view_display.{$entityTypeID}.default", $parameters),
      )
    );

    return array(
      '#type'  => 'operations',
      '#links' => $links,
    );
  }


  /**
   * @param \Drupal\node\Entity\NodeType $nodeType
   * @return array
   */
  private function buildNodeOperations(NodeType $nodeType) {
    $moduleHandler = $this->getModuleHandler();
    $links = $moduleHandler->invokeAll('entity_operation', array($nodeType));
    $moduleHandler->alter('entity_operation', $links, $nodeType);
    uasort($links, '\Drupal\Component\Utility\SortArray::sortByWeightElement');

    return array(
      '#type'  => 'operations',
      '#links' => $links,
    );
  }


  /**
   * Return the message that will be displayed if the table is empty.
   *
   * @return string
   */
  protected function buildEmptyMessage() {
    return $this->t(
      'There are no managed entities. <a href="@update-url">Update the schemas.</a>',
      array(
        '@update-url' => Url::fromRoute('managed.console.install'),
      )
    );
  }
}
