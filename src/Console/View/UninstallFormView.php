<?php

namespace Drupal\managed\Console\View;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\managed\Core\InstallerHost;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Display a confirmation message when the user is about to uninstall
 * managed entities.
 */
class UninstallFormView extends ConfirmFormBase
{
  /**
   * The installer that will be used to perform the uninstall operation.
   *
   * @var \Drupal\managed\Core\InstallerHost
   */
  private $installer;



  /**
   * Create a new UninstallFormView instance.
   *
   * @param \Drupal\managed\Core\InstallerHost $installer
   */
  public function __construct(InstallerHost $installer) {
    $this->installer = $installer;
  }


  /**
   * Instantiates a new instance of this class.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('managed.installer'));
  }


  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'managed_console_uninstall_confirm';
  }


  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $formState) {
    $this->installer->uninstall();

    $markup = $this->installer->getPerformedActionsMarkup();
    if ($markup == FALSE) {
      $markup = $this->t('There have been no pending uninstall operations.');
    } else {
      $markup = $this->t('The following uninstall operations have been performed:') . $markup;
    }

    drupal_set_message(Markup::create($markup));
    $formState->setRedirectUrl($this->getCancelUrl());
  }


  /**
   * Returns the question to ask the user.
   *
   * @return string
   *   The form question. The page title will be set to this value.
   */
  public function getQuestion() {
    return t('Are you sure the following changes should be made to the database?');
  }


  /**
   * Returns additional text to display as a description.
   *
   * @return string
   *   The form description.
   */
  public function getDescription() {
    $details = $this->installer->getPendingUninstallDetails();
    if (count($details) == 0) {
      return t('There are now uninstall operations to be performed.');
    } else {
      $markup = t('The following operations will be performed:') . '<ul>';

      foreach ($details as $detail) {
        $markup .= '<li>' . $detail . '</li>';
      }

      return $markup . '</ul>';
    }
  }


  /**
   * Returns the route to go to if the user cancels the action.
   *
   * @return \Drupal\Core\Url
   *   A URL object.
   */
  public function getCancelUrl() {
    return URL::fromRoute('managed.console');
  }
}
