<?php

namespace Drupal\managed\Console;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\managed\Core\ManagedEntityTypeManager;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;


class RouteSubscriber extends RouteSubscriberBase
{
  /**
   * @var \Drupal\managed\Core\ManagedEntityTypeManager
   */
  private $manager;



  /**
   * Create a new RouteSubscriber instance.
   *
   * @param \Drupal\managed\Core\ManagedEntityTypeManager $manager
   */
  public function __construct(ManagedEntityTypeManager $manager) {
    $this->manager = $manager;
  }


  /**
   * Alters existing routes for a specific collection.
   *
   * @param \Symfony\Component\Routing\RouteCollection $collection
   *   The route collection for adding routes.
   */
  protected function alterRoutes(RouteCollection $collection) {
    foreach ($this->manager->getAllManagedTypes() as $entity) {
      $name = self::getEntityRouteName($entity->id());

      $label = $entity->getLabel();
      if ($label instanceof TranslatableMarkup) {
        $label = $label->getUntranslatedString();
      }

      $path = '/admin/structure/managed/' . $entity->id();
      if ($entity->hasBundles()) {
        $path .= '/{bundle}';
      }

      $collection->add($name, new Route(
        $path,
        array(
          '_title'       => $label,
          '_controller'  => 'Drupal\managed\Console\Controller::details'
        ), array(
          '_permission'  => 'administer managed entities',
          '_method'      => 'GET'
        ), array(
          '_admin_route' => TRUE
        )
      ));
    }
  }


  static function getEntityRouteName($entityId) {
    return 'managed.console.entity.' . $entityId;
  }
}
