<?php

namespace Drupal\managed\Scaffold;


trait TranslatableTrait
{
  /**
   * The language code.
   *
   * @LanguageField()
   * @var \Drupal\Core\Field\FieldItemList
   */
  public $langcode;
}
