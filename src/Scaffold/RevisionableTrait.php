<?php

namespace Drupal\managed\Scaffold;


trait RevisionableTrait
{
  /**
   * The revision ID.
   *
   * @RevisionField()
   * @var \Drupal\Core\Field\FieldItemList
   */
  public $revision;

  /**
   * The user ID of the author of the current revision.
   *
   * @ReferenceField(entity="user", isTranslatable=FALSE, isRevisionable=TRUE, formType="none")
   * @EntityKey("revision_uid")
   * @var \Drupal\Core\Field\FieldItemList
   */
  public $revision_uid;

  /**
   * The time that the current revision was created.
   *
   * @CreatedField(isTranslatable=FALSE, isRevisionable=TRUE, formType="none")
   * @EntityKey("revision_timestamp")
   * @var \Drupal\Core\Field\FieldItemList
   */
  public $revision_timestamp;

  /**
   * Briefly describe the changes you have made.
   *
   * @TextareaField(isTranslatable=FALSE, isRevisionable=TRUE)
   * @EntityKey("revision_log")
   * @var \Drupal\Core\Field\FieldItemList
   */
  public $revision_log;
}
