<?php

namespace Drupal\managed\Scaffold;


trait TranslatableRevisionableTrait
{
  use RevisionableTrait;
  use TranslatableTrait;


  /**
   * Indicates if the last edit of a translation belongs to current revision.
   *
   * @CheckboxField(isTranslatable=TRUE, isRevisionable=TRUE, formType="none")
   * @var \Drupal\Core\Field\FieldItemList
   */
  public $revision_translation_affected;
}
