<?php

namespace Drupal\managed\Utils;

use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * A helper class that can generate singular and plural forms
 * of english words.
 */
trait ContainerAwareTrait
{
  /**
   * The container this instance should use.
   *
   * @var \Symfony\Component\DependencyInjection\ContainerInterface
   */
  private $container;



  /**
   * Gets a service.
   *
   * @param string $id
   *   The service identifier
   * @param int $invalidBehavior
   *   The behavior when the service does not exist
   * @return object
   *   The associated service
   */
  public function getService($id, $invalidBehavior = ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE) {
    return $this->getContainer()->get($id, $invalidBehavior);
  }


  /**
   * Return the container this instance should use.
   *
   * @return \Symfony\Component\DependencyInjection\ContainerInterface
   */
  public function getContainer() {
    if (isset($this->container)) {
      return $this->container;
    } else {
      return \Drupal::getContainer();
    }
  }


  /**
   * Set the container this instance should use.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @return $this
   */
  public function setContainer(ContainerInterface $container) {
    if (!$container instanceof ContainerInterface) {
      throw new \InvalidArgumentException();
    }

    $this->container = $container;
    return $this;
  }
}
