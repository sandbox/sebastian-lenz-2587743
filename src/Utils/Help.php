<?php

namespace Drupal\managed\Utils;

use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\managed\Vendor\Markdown\MarkdownExtra;
use org\bovigo\vfs\vfsStream;
use Symfony\Component\HttpFoundation\Request;


class Help extends HelpPage
{
  /**
   * @var string
   */
  private $routeName;

  private $routeParameters;

  /**
   * @var string
   */
  private $currentSlug;



  public function __construct($routeName, RouteMatchInterface $routeMatch) {
    parent::__construct(NULL, 'Help');

    $this->routeName = $routeMatch->getRouteName();
    $this->routeParameters = $routeMatch->getParameters()->all();

    $request = \Drupal::request();
    if ($request->query->has('page')) {
      $this->currentSlug = $request->query->get('page');
    }
  }


  public function addPath($path, HelpPage $scope = NULL, $depth = 0) {
    if (!file_exists($path)) {
      return $this;
    }

    if (is_null($scope)) {
      $scope = $this;
    }

    if (is_dir($path)) {
      if ($depth > 0) {
        $scope = $scope->getChild(basename($path), true);
      }

      foreach (scandir($path) as $child) {
        if ($child{0} == '.') {
          continue;
        }

        $this->addPath(implode(DIRECTORY_SEPARATOR, array($path, $child)), $scope, $depth + 1);
      }
    }
    elseif (is_file($path) && pathinfo($path, PATHINFO_EXTENSION) == 'md') {
      if (basename($path) == 'index') {
        $scope->fileName = $path;
      } else {
        $scope->addChild($path);
      }
    }

    return $this;
  }


  public function render() {
    $this->slugify();

    if (!isset($this->currentSlug)) {
      $page = reset($this->children);
    } else {
      $page = $this->getChildBySlug($this->currentSlug);
    }

    if (is_null($page)) {
      return NULL;
    } else {
      return $page->render($this);
    }
  }


  public function getUrl($page) {
    $url = new Url($this->routeName, $this->routeParameters, array(
      'query' => array('page' => $page->slug)
    ));

    return $url->toString();
  }
}


class HelpPage
{
  /**
   * @var string
   */
  protected $name;

  /**
   * @var string
   */
  protected $fileName;

  /**
   * @var string
   */
  protected $slug;

  /**
   * @var HelpPage
   */
  protected $parent;

  /**
   * @var HelpPage[]
   */
  protected $children;

  /**
   * @var string[]
   */
  protected $childSlugs;



  /**
   * Create a new HelpPage instance.
   */
  public function __construct(HelpPage $parent, $name, $fileName = NULL) {
    $this->parent   = $parent;
    $this->name     = $name;
    $this->fileName = $fileName;
  }


  protected function slugify($slug = NULL) {
    $this->slug = $slug;

    foreach ($this->children as $name => $child) {
      $childSlug = preg_replace('/[^a-z0-9_-]/', '', strtolower(pathinfo($name, PATHINFO_FILENAME)));
      $childBase = $childSlug;
      $index     = 1;

      while (in_array($childSlug, $this->childSlugs)) {
        $index += 1;
        $childSlug = $childBase . '-' . $index;
      }

      $this->childSlugs[] = $childSlug;
      $child->slugify(is_null($slug) ? $childSlug : implode(':', array($slug, $childSlug)));
    }
  }


  protected function addChild($fileName) {
    $name = basename($fileName);
    if (isset($this->children[$name])) {
      $this->children[$name]->fileName = $fileName;
    } else {
      $this->children[$name] = new HelpPage($this, $name, $fileName);
    }
  }


  protected function getChild($fileName, $create = FALSE) {
    $name = basename($fileName);
    if (isset($this->children[$name])) {
      return $this->children[$name];
    } elseif ($create) {
      return $this->children[$name] = new HelpPage($this, $name);
    }

    return NULL;
  }


  protected function getChildBySlug($slug) {
    $splitAt = strpos($slug, ':');
    if ($splitAt !== FALSE) {
      $child = $this->getChildBySlug(substr($slug, 0, $splitAt));
      if (is_null($child)) {
        return NULL;
      } else {
        return $child->getChildBySlug(substr($slug, $splitAt + 1));
      }
    }

    foreach ($this->children as $child) {
      if ($child->slug == $slug) {
        return $child;
      }
    }

    return NULL;
  }


  /**
   * @param string $segment
   * @return \Drupal\managed\Utils\HelpPage|null
   */
  protected function getChildBySegment($segment) {
    $splitAt = strpos($segment, '/');

    if ($splitAt === 0) {
      $root = $this;
      while (!is_null($root->parent)) {
        $root = $root->parent;
      }

      return $root;
    }
    elseif ($splitAt !== FALSE) {
      $child = $this->getChildBySegment(substr($segment, 0, $splitAt));
      if (is_null($child)) {
        return NULL;
      } else {
        return $child->getChildBySegment(substr($segment, $splitAt + 1));
      }
    }

    if ($segment == '..') {
      return $this->parent;
    }

    foreach ($this->children as $child) {
      if ($child->name == $segment) {
        return $child;
      }
    }

    return NULL;
  }


  protected function render(Help $help) {
    if (isset($this->fileName) && is_file($this->fileName)) {
      $source = file_get_contents($this->fileName);

      $chunks = preg_split('/\\(([^\\)]+)\\)/', $source, -1, PREG_SPLIT_DELIM_CAPTURE);
      if (count($chunks) % 2 != 0) {
        $trailing = array_pop($chunks);
      }

      $source = '';
      for ($index = 0; $index < count($chunks); $index += 2) {
        $source .= $chunks[$index];
        $segment = $chunks[$index + 1];
        $page    = $this->parent ? $this->parent->getChildBySegment($segment) : NULL;

        if (!is_null($page)) {
          $segment = $help->getUrl($page);
        }

        $source .= '(' . $segment . ')';
      }

      if (isset($trailing)) {
        $source .= $trailing;
      }

      return MarkdownExtra::defaultTransform($source);
    }
  }
}
