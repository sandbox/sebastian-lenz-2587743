<?php

/*
 * This is a conversion of underscore.inflection to PHP.
 *
 * Copyright (c) 2014 Jeremy Ruppel
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * Portions of Underscore.inflection are inspired or borrowed from ActiveSupport
 *
 * @see https://github.com/jeremyruppel/underscore.inflection
 */

namespace Drupal\managed\Utils;


/**
 * A helper class that can generate singular and plural forms
 * of english words.
 */
class Inflector
{
  /**
   * A list of replacement rules used to generate the plural form of a word.
   *
   * @var InflectorRule[]
   */
  private $plurals;

  /**
   * A list of replacement rules used to generate the singular form of a word.
   *
   * @var InflectorRule[]
   */
  private $singulars;

  /**
   * A list of words that don't have a plural form.
   *
   * @var string[]
   */
  private $uncountables;



  /**
   * Create a new Inflector instance.
   */
  public function __construct() {
    $this->reset();
  }


  /**
   * Return the plural version of the given singular.
   *
   * @param string $value
   * @return string
   */
  public function pluralize($value) {
    return $this->applyRules($value, $this->plurals);
  }


  /**
   * Return the singular version of the given plural.
   *
   * @param string $value
   * @return string
   */
  public function singularize($value) {
    return $this->applyRules($value, $this->singulars);
  }


  /**
   * Add a new pluralization rule.
   *
   * @param string $pattern
   * @param string $replacement
   * @return $this
   */
  public function addPluralRule($pattern, $replacement) {
    array_unshift($this->plurals, new InflectorRule($pattern, $replacement));
    return $this;
  }


  /**
   * Add a new singularization rule.
   *
   * @param string $pattern
   * @param string $replacement
   * @return $this
   */
  public function addSingularRule($pattern, $replacement) {
    array_unshift($this->singulars, new InflectorRule($pattern, $replacement));
    return $this;
  }


  /**
   * A shortcut method to create both a pluralization and singularization
   * rule for the word at the same time.
   *
   * You must supply both the singular form and the plural form as
   * explicit strings.
   *
   * @param string $singular
   * @param string $plural
   * @return $this
   */
  public function addIrregularRule($singular, $plural) {
    $this->addPluralRule('/' . $singular . '$/i', $plural);
    $this->addSingularRule('/' . $plural . '$/i', $singular);
    return $this;
  }


  /**
   * Add a new uncountable rule.
   *
   * Uncountable words do not get pluralized or singularized.
   *
   * @param string $value
   * @return $this
   */
  public function addUncountable($value) {
    $this->uncountables[] = $value;
    return $this;
  }


  /**
   * Internal helper that applies a given set of rules to the supplied value.
   *
   * @param string $value
   * @param InflectorRule[] $rules
   * @return string
   */
  private function applyRules($value, $rules) {
    if (in_array($value, $this->uncountables)) {
      return $value;
    }

    foreach ($rules as $rule) {
      if ($rule->appliesTo($value)) {
        return $rule->apply($value);
      }
    }

    return $value;
  }


  /**
   * Resets all rules to their initial state.
   *
   * @return $this
   */
  public function reset() {
    $this->plurals = array(
      new InflectorRule('/(quiz)$/i', '$1zes'),
      new InflectorRule('/^(oxen)$/i', '$1'),
      new InflectorRule('/^(ox)$/i', '$1en'),
      new InflectorRule('/([m|l])ice$/i', '$1ice'),
      new InflectorRule('/([m|l])ouse$/i', '$1ice'),
      new InflectorRule('/(matr|vert|ind)(?:ix|ex)$/i', '$1ices'),
      new InflectorRule('/(x|ch|ss|sh)$/i', '$1es'),
      new InflectorRule('/([^aeiouy]|qu)y$/i', '$1ies'),
      new InflectorRule('/(hive)$/i', '$1s'),
      new InflectorRule('/(?:([^f])fe|([lr])?f)$/i', '$1$2ves'),
      new InflectorRule('/sis$/i', 'ses'),
      new InflectorRule('/([ti])a$/i', '$1a'),
      new InflectorRule('/([ti])um$/i', '$1a'),
      new InflectorRule('/(buffal|tomat)o$/i', '$1oes'),
      new InflectorRule('/(bu)s$/i', '$1ses'),
      new InflectorRule('/(alias|status)$/i', '$1es'),
      new InflectorRule('/(octop|vir)i$/i', '$1i'),
      new InflectorRule('/(octop|vir)us$/i', '$1i'),
      new InflectorRule('/(ax|test)is$/i', '$1es'),
      new InflectorRule('/s$/i', 's'),
      new InflectorRule('/$/i', 's')
    );

    $this->singulars = array(
      new InflectorRule('/(database)s$/i', '$1'),
      new InflectorRule('/(quiz)zes$/i', '$1'),
      new InflectorRule('/(matr)ices$/i', '$1ix'),
      new InflectorRule('/(vert|ind)ices$/i', '$1ex'),
      new InflectorRule('/^(ox)en/i', '$1'),
      new InflectorRule('/(alias|status)es$/i', '$1'),
      new InflectorRule('/(octop|vir)i$/i', '$1us'),
      new InflectorRule('/(cris|ax|test)es$/i', '$1is'),
      new InflectorRule('/(shoe)s$/i', '$1'),
      new InflectorRule('/(o)es$/i', '$1'),
      new InflectorRule('/(bus)es$/i', '$1'),
      new InflectorRule('/([m|l])ice$/i', '$1ouse'),
      new InflectorRule('/(x|ch|ss|sh)es$/i', '$1'),
      new InflectorRule('/(m)ovies$/i', '$1ovie'),
      new InflectorRule('/(s)eries$/i', '$1eries'),
      new InflectorRule('/([^aeiouy]|qu)ies$/i', '$1y'),
      new InflectorRule('/([lr])ves$/i', '$1f'),
      new InflectorRule('/(tive)s$/i', '$1'),
      new InflectorRule('/(hive)s$/i', '$1'),
      new InflectorRule('/([^f])ves$/i', '$1fe'),
      new InflectorRule('/(^analy)ses$/i', '$1sis'),
      new InflectorRule('/((a)naly|(b)a|(d)iagno|(p)arenthe|(p)rogno|(s)ynop|(t)he)ses$/i', '$1$2sis'),
      new InflectorRule('/([ti])a$/i', '$1um'),
      new InflectorRule('/(n)ews$/i', '$1ews'),
      '/s$/i' => ''
    );

    $this
      ->addIrregularRule('person', 'people')
      ->addIrregularRule('man', 'men')
      ->addIrregularRule('child', 'children')
      ->addIrregularRule('sex', 'sexes')
      ->addIrregularRule('move', 'moves')
      ->addIrregularRule('cow', 'kine');

    $this->uncountables = array(
      'equipment',
      'information',
      'rice',
      'money',
      'species',
      'series',
      'fish',
      'sheep',
      'jeans'
    );

    return $this;
  }
}



/**
 * Helper class for storing and using replacement rules of the inflector.
 */
class InflectorRule
{
  /**
   * The regular expression this rules applies to.
   *
   * @var string
   */
  private $pattern;

  /**
   * The replacement pattern this rules applies.
   *
   * @var string
   */
  private $replacement;



  /**
   * Create a new InflectorRule instance.
   *
   * @param string $pattern
   * @param string $replacement
   */
  public function __construct($pattern, $replacement) {
    $this->pattern     = $pattern;
    $this->replacement = $replacement;
  }


  /**
   * Test whether this rule applies to the given value.
   *
   * @param string $value
   * @return bool
   */
  public function appliesTo($value) {
    return !!preg_match($this->pattern, $value);
  }


  /**
   * Apply this rule to the given value.
   *
   * @param string $value
   * @return string
   */
  public function apply($value) {
    return preg_replace($this->pattern, $this->replacement, $value);
  }
}
