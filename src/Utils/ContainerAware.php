<?php

namespace Drupal\managed\Utils;

use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * A helper class that can generate singular and plural forms
 * of english words.
 */
class ContainerAware
{
  use ContainerAwareTrait;



  /**
   * Create a new ContainerAware instance.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container this instance should use.
   */
  public function __construct(ContainerInterface $container = NULL) {
    if (!is_null($container)) {
      $this->setContainer($container);
    }
  }
}
