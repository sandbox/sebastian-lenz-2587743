<?php

namespace Drupal\managed\Utils;

use Symfony\Component\DependencyInjection\Container;


/**
 * Exposes a set of utility functions for string manipulations.
 */
class StringUtils
{
  /**
   * The inflector instance used by the `pluralize` and `singularize` methods.
   *
   * @var \Drupal\managed\Utils\Inflector
   */
  static private $inflector;



  /**
   * Camelizes a string.
   *
   * @param string $value
   * @return string
   */
  public static function camelize($value) {
    return Container::camelize($value);
  }


  /**
   * Underscores a string.
   *
   * @param string $value
   * @return string
   */
  public static function underscore($value) {
    return Container::underscore($value);
  }


  /**
   * Create a humanized version of the given string.
   *
   * @param $value
   *   The source string using underscores or caml case.
   * @return string
   *   The humanized version of the given string.
   */
  static function humanize($value) {
    $value = strtolower(preg_replace('/(?<=\w)([A-Z])/', '_\\1', $value));
    $value = str_replace('_', ' ', $value);
    return ucfirst($value);
  }


  /**
   * Return the plural version of the given singular.
   *
   * @param string $value
   * @return string
   */
  static function pluralize($value) {
    return self::getInflector()->pluralize($value);
  }


  /**
   * Return the singular version of the given plural.
   *
   * @param string $value
   * @return string
   */
  static function singularize($value) {
    return self::getInflector()->singularize($value);
  }


  /**
   * Add an ordinal suffix to the given value.
   *
   * @param string|int|float $value
   * @return string
   */
  static public function ordinalize($value) {
    if (is_nan($value)) {
      return $value;
    }

    $value  = (string)$value;
    $digits = substr($value, -2);
    if (in_array($digits, array('11', '12', '13'))) {
      return $value . 'th';
    }

    $digits = substr($value, -1);
    switch ($digits) {
      case '1': return $value . 'st';
      case '2': return $value . 'nd';
      case '3': return $value . 'rd';
    }

    return $value . 'th';
  }


  /**
   * Return the inflector instance used by the `pluralize` and
   * `singularize` methods.
   *
   * @return \Drupal\managed\Utils\Inflector
   */
  static function getInflector() {
    if (!isset(self::$inflector)) {
      self::$inflector = new Inflector();
    }

    return self::$inflector;
  }
}
