<?php

namespace Drupal\managed\Utils;

use Drupal\managed\Entity;


trait EntityAwareRouteTrait
{
  /**
   * @var \Drupal\managed\Entity
   */
  private $entity;



  /**
   * @return \Drupal\managed\Entity
   */
  protected function resolveEntityFormRoute() {
    $routeMatch = \Drupal::routeMatch();
    if (!($route = $routeMatch->getRouteObject()) || !($parameters = $route->getOption('parameters'))) {
      return NULL;
    }

    foreach ($parameters as $name => $options) {
      if (!isset($options['type']) || strpos($options['type'], 'entity:') !== 0) {
        continue;
      }

      $entity = $routeMatch->getParameter($name);
      if ($entity instanceof Entity) {
        return $entity;
      }
    }

    return NULL;
  }


  /**
   * @param \Drupal\managed\Entity $entity
   */
  protected function setEntity(Entity $entity) {
    $this->entity = $entity;
  }


  /**
   * @return \Drupal\managed\Entity
   */
  protected function getEntity() {
    if (!isset($this->entity)) {
      $this->setEntity($this->resolveEntityFormRoute());
    }

    return $this->entity;
  }


  /**
   * @return string
   */
  protected function getEntityTypeID() {
    return $this->getEntity()->getEntityTypeId();
  }


  /**
   * @return \Drupal\managed\Core\ManagedEntityType
   */
  protected function getEntityType() {
    return $this->getEntity()->getEntityType();
  }
}
