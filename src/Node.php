<?php

namespace Drupal\managed;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\managed\Core\ManagedEntityTrait;
use Drupal\managed\Core\ManagedEntityType;


class Node extends \Drupal\node\Entity\Node
{
  use ManagedEntityTrait;


  /**
   * Provides field definitions for a specific bundle.
   *
   * This function can return definitions both for bundle fields (fields that
   * are not defined in $base_field_definitions, and therefore might not exist
   * on some bundles) as well as bundle-specific overrides of base fields
   * (fields that are defined in $base_field_definitions, and therefore exist
   * for all bundles). However, bundle-specific base field overrides can also
   * be provided by 'base_field_override' configuration entities, and that is
   * the recommended approach except in cases where an entity type needs to
   * provide a bundle-specific base field override that is decoupled from
   * configuration. Note that for most entity types, the bundles themselves are
   * derived from configuration (e.g., 'node' bundles are managed via
   * 'node_type' configuration entities), so decoupling bundle-specific base
   * field overrides from configuration only makes sense for entity types that
   * also decouple their bundles from configuration. In cases where both this
   * function returns a bundle-specific override of a base field and a
   * 'base_field_override' configuration entity exists, the latter takes
   * precedence.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entityType
   *   The entity type definition. Useful when a single class is used for multiple,
   *   possibly dynamic entity types.
   * @param string $bundleID
   *   The bundle.
   * @param \Drupal\Core\Field\FieldDefinitionInterface[] $baseFields
   *   The list of base field definitions.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   *   An array of bundle field definitions, keyed by field name.
   *
   * @see \Drupal\Core\Entity\EntityManagerInterface::getFieldDefinitions()
   * @see \Drupal\Core\Entity\FieldableEntityInterface::baseFieldDefinitions()
   *
   * @todo WARNING: This method will be changed in
   *   https://www.drupal.org/node/2346347.
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entityType, $bundleID, array $baseFields) {
    if (!($entityType instanceof ManagedEntityType)) {
      throw new \InvalidArgumentException();
    }

    $fields = array();
    $bundle = $entityType->getBundle($bundleID);
    if (!is_null($bundle)) {
      foreach ($bundle->getFields() as $field) {
        $fields[$field->getName()] = $field->getDefinition();
      }
    }

    return $fields;
  }
}
