<?php

/**
 * Addendum PHP Reflection Annotations
 * http://code.google.com/p/addendum/
 *
 * Copyright (C) 2006-2009 Jan "johno Suchal <johno@jsmf.net>
 *
 * This file has been modified to be used with Drupal class loading
 * by Sebastian Lenz, 2015
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

namespace Drupal\managed\Vendor\Addendum;


class Addendum
{
  private static $rawMode;

  private static $ignore;

  private static $annotations = array();


  public static function getDocComment($reflection) {
    if(self::checkRawDocCommentParsingNeeded()) {
      $docComment = new DocComment();
      return $docComment->get($reflection);
    } else {
      return $reflection->getDocComment();
    }
  }


  /** Raw mode test */
  private static function checkRawDocCommentParsingNeeded() {
    if(self::$rawMode === null) {
      $reflection = new \ReflectionClass('Drupal\managed\Vendor\Addendum\Addendum');
      $method = $reflection->getMethod('checkRawDocCommentParsingNeeded');
      self::setRawMode($method->getDocComment() === false);
    }
    return self::$rawMode;
  }


  public static function setRawMode($enabled = true) {
    self::$rawMode = $enabled;
  }


  public static function resetIgnoredAnnotations() {
    self::$ignore = array();
  }


  public static function ignores($class) {
    return isset(self::$ignore[$class]);
  }


  public static function ignore() {
    foreach(func_get_args() as $class) {
      self::$ignore[$class] = true;
    }
  }


  public static function resolveClassName($class) {
    if (!isset(self::$annotations[$class])) {
      return 'Drupal\managed\Vendor\Addendum\GenericAnnotation';
    }

    return self::$annotations[$class];
  }


  public static function addAnnotation($name, $class) {
    self::$annotations[$name] = $class;
  }


  public static function addAnnotations($map) {
    foreach ($map as $name => $class) {
      self::addAnnotation($name, $class);
    }
  }

  public static function removeAllAnnotations() {
    self::$annotations = array();
  }
}
