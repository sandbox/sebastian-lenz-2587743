<?php

/**
 * Addendum PHP Reflection Annotations
 * http://code.google.com/p/addendum/
 *
 * Copyright (C) 2006-2009 Jan "johno Suchal <johno@jsmf.net>
 *
 * This file has been modified to be used with Drupal class loading
 * by Sebastian Lenz, 2015
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **/

namespace Drupal\managed\Vendor\Addendum;


class Annotation
{
  public $value;


  public final function __construct($data = array(), $target = false) {
    $this->setValues($data);
    $this->checkConstraints($target);
  }


  protected function setValues($data) {
    $reflection = new \ReflectionClass($this);
    $class = $reflection->getName();

    foreach($data as $key => $value) {
      if ($reflection->hasProperty($key)) {
        $this->$key = $value;
      } else {
        throw new \Exception("Property '$key' not defined for annotation '$class'.");
      }
    }
  }


  protected function checkConstraints($reflection) {}
}

