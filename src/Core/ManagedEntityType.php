<?php

namespace Drupal\managed\Core;

use Drupal\Core\Entity\ContentEntityType;


class ManagedEntityType extends ContentEntityType
{
  /**
   * @var ManagedEntityTypeData
   */
  private $data;



  /**
   * @param \Drupal\managed\Core\ManagedEntityTypeDefinitionInterface $annotation
   * @return $this
   */
  public function pullDefinitionData(ManagedEntityTypeDefinitionInterface $annotation) {
    $data = new ManagedEntityTypeData();
    $data->fields           = $annotation->getFields();
    $data->relations        = $annotation->getRelations();
    $data->bundles          = $annotation->getBundles();
    $data->pluralLabel      = $annotation->getPluralLabel();
    $data->behaviour        = $annotation->getBehaviour();
    $data->systemProperties = $this->pullSystemProperties();
    $data->resetProperties  = $this->pullResetProperties($data);

    $this->additional['managed'] = serialize($data);
    $this->data = $data;

    return $this;
  }


  private function pullSystemProperties() {
    $systemProperties = array();
    $reflection = new \ReflectionClass($this->class);
    $reflection = $reflection->getParentClass();

    foreach ($reflection->getProperties() as $property) {
      $systemProperties[] = $property->name;
    }

    return $systemProperties;
  }


  /**
   * @return array
   */
  private function pullResetProperties(ManagedEntityTypeData $data) {
    $systemProperties = $data->systemProperties;
    $baseProperties   = array();
    $resetProperties  = array();

    foreach ($data->fields as $field) {
      $baseProperties[] = $field->getName();
    }

    if (is_array($data->bundles)) {
      foreach ($data->bundles as $bundle) {
        $bundleProperties = $baseProperties;
        foreach ($bundle->getFields() as $field) {
          $bundleProperties[] = $field->getName();
        }

        $resetProperties[$bundle->id()] = array_diff($bundleProperties, $systemProperties);
      }
    }

    $resetProperties['@DEFAULT'] = array_diff($baseProperties, $systemProperties);
    return $resetProperties;
  }


  public function hasBundles() {
    return $this->hasKey('bundle');
  }


  /**
   * @return \Drupal\managed\Core\ManagedEntityTypeData
   */
  private function getData() {
    if (!isset($this->data)) {
      if (isset($this->additional['managed'])) {
        try {
          $this->data = unserialize($this->additional['managed']);
        } catch (\Exception $e) {
          drupal_set_message($this->t(
            'The managed data payload of the entity `@entity` is damaged.',
            array('@entity' => $this->id())
          ), 'error');
        }
      }

      if (!($this->data instanceof ManagedEntityTypeData)) {
        $this->data = new ManagedEntityTypeData();
      }
    }

    return $this->data;
  }


  public function getSystemProperties() {
    $data = $this->getData();
    if (isset($data->systemProperties)) {
      return $data->systemProperties;
    } else {
      return array();
    }
  }


  /**
   * Return all bundles available for this entity type.
   *
   * @param string|bool $bundle
   * @return string[]
   */
  public function getResetProperties($bundle = FALSE) {
    $data = $this->getData();
    if (isset($data->resetProperties)) {
      $bundle = $bundle ?: '@DEFAULT';
      if (isset($data->resetProperties[$bundle])) {
        return $data->resetProperties[$bundle];
      } else {
        return array();
      }
    } else {
      return array();
    }
  }


  /**
   * Return all bundles available for this entity type.
   *
   * @return \Drupal\managed\Annotation\BundleAnnotation[]
   */
  public function getBundles() {
    $data = $this->getData();
    if (isset($data->bundles)) {
      return $data->bundles;
    } else {
      return array();
    }
  }


  /**
   * Return the bundle of this entity type with the given bundle id.
   *
   * @param string $id
   * @return \Drupal\managed\Annotation\BundleAnnotation|null
   */
  public function getBundle($id) {
    $data = $this->getData();

    if (isset($data->bundles[$id])) {
      return $data->bundles[$id];
    } else {
      return NULL;
    }
  }


  /**
   * @return string
   */
  public function getPluralLabel() {
    $data = $this->getData();
    if (isset($data->pluralLabel)) {
      return $data->pluralLabel;
    } else {
      return $this->getLabel();
    }
  }


  /**
   * @return \Drupal\managed\Annotation\Field\AbstractFieldAnnotation[]
   */
  public function getFields() {
    $data = $this->getData();
    if (isset($data->fields)) {
      return $data->fields;
    } else {
      return array();
    }
  }


  /**
   * @param string $name
   * @return \Drupal\managed\Annotation\Field\AbstractFieldAnnotation|null
   */
  public function getField($name) {
    $fields = $this->getFields();
    if (isset($fields[$name])) {
      return $fields[$name];
    } else {
      return NULL;
    }
  }


  /**
   * @return \Drupal\managed\Annotation\Relation\AbstractRelationAnnotation[]
   */
  public function getRelations($bundle = FALSE) {
    $data = $this->getData();
    if (isset($data->relations)) {
      $result = $data->relations;
    } else {
      $result = array();
    }

    if ($bundle && $bundle != $this->id) {
      $bundleAnnotation = $this->getBundle($bundle);
      if (!is_null($bundleAnnotation)) {
        $result += $bundleAnnotation->getRelations();
      }
    }

    return $result;
  }


  /**
   * Return the behaviour definition attached to this entity type.
   *
   * @return \Drupal\managed\Annotation\Behaviour\AbstractBehaviourAnnotation
   */
  public function getBehaviour() {
    $data = $this->getData();
    if (isset($data->behaviour)) {
      return $data->behaviour;
    } else {
      return NULL;
    }
  }


  /**
   * Implements hook_entity_field_storage_info_alter().
   *
   * @param \Drupal\Core\Field\BaseFieldDefinition[] $fields
   */
  public function onFieldStorageInfoAlter(&$fields) {
    foreach ($this->getBundles() as $bundle) {
      foreach ($bundle->getFields() as $field) {
        $definition = $field->getDefinition();
        if ($definition->isComputed()) {
          continue;
        }

        $definition->setTargetEntityTypeId($this->id());
        $definition->setTargetBundle($bundle->id());
        $definition->setName($field->getName());
        $definition->setProvider($bundle->getProvider());

        $fields[$field->getName()] = $definition;
      }
    }
  }


  /**
   * Convert the given EntityType instance to an instance of ManagedEntityType.
   *
   * @param \Drupal\Core\Entity\ContentEntityType $entityType
   * @return \Drupal\managed\Core\ManagedEntityType
   */
  public static function fromEntityType(ContentEntityType $entityType) {
    $type = new ManagedEntityType(array(
      'id' => $entityType->id
    ));

    foreach (get_object_vars($entityType) as $key => $value) {
      $type->$key = $value;
    }

    return $type;
  }
}


class ManagedEntityTypeData
{
  /**
   * @var string
   */
  public $pluralLabel;

  /**
   * @var string[]
   */
  public $systemProperties;

  /**
   * @var string[]
   */
  public $resetProperties;

  /**
   * @var \Drupal\managed\Annotation\Behaviour\AbstractBehaviourAnnotation
   */
  public $behaviour;

  /**
   * @var \Drupal\managed\Annotation\Field\AbstractFieldAnnotation[]
   */
  public $fields;

  /**
   * @var \Drupal\managed\Annotation\Relation\AbstractRelationAnnotation[]
   */
  public $relations;

  /**
   * @var \Drupal\managed\Annotation\BundleAnnotation[]
   */
  public $bundles;
}
