<?php

namespace Drupal\managed\Core;


interface ManagedEntityTypeDefinitionInterface
{
  /**
   * Return the plural version of the label.
   *
   * @return string
   */
  public function getPluralLabel();


  /**
   * Return all fields attached to this annotation.
   *
   * @return \Drupal\managed\Annotation\Field\AbstractFieldAnnotation[]
   */
  public function getFields();


  /**
   * Return all relations attached to this annotation.
   *
   * @return \Drupal\managed\Annotation\Relation\AbstractRelationAnnotation[]
   */
  public function getRelations();


  /**
   * Return a list of all bundles belonging to this entity.
   *
   * @return \Drupal\managed\Annotation\AbstractBundleAnnotation[]
   */
  public function getBundles();


  /**
   * Return the behaviour definition attached to this entity.
   *
   * @return \Drupal\managed\Annotation\Behaviour\AbstractBehaviourAnnotation
   */
  public function getBehaviour();
}
