<?php

namespace Drupal\managed\Core\Storage;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;


class ManagedEntityStorage extends SqlContentEntityStorage
{
  use ManagedStorageTrait;
}
