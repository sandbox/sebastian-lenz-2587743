<?php

namespace Drupal\managed\Core\Storage;

use Drupal\node\NodeStorage;


class ManagedNodeStorage extends NodeStorage
{
  use ManagedStorageTrait;
}
