<?php

namespace Drupal\managed\Core\Storage;

use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Language\LanguageInterface;
use Drupal\managed\Annotation\EntityAnnotation;
use Drupal\managed\Core\ManagedEntityType;


trait ManagedStorageTrait
{
  private $bundlesMapping;


  protected function getBundleClass($bundle) {
    if (!isset($this->bundlesMapping[$bundle])) {
      if ($this->entityType instanceof ManagedEntityType) {
        $bundleInfo = $this->entityType->getBundle($bundle);
        if (!is_null($bundleInfo)) {
          $this->bundlesMapping[$bundle] = $bundleInfo->getClass();
        } else {
          $this->bundlesMapping[$bundle] = $this->entityClass;
        }
      } else {
        $this->bundlesMapping[$bundle] = $this->entityClass;
      }
    }

    return $this->bundlesMapping[$bundle];
  }


  /**
   * {@inheritdoc}
   */
  protected function doCreate(array $values) {
    // We have to determine the bundle first.
    $bundle = FALSE;
    if ($this->bundleKey) {
      if (!isset($values[$this->bundleKey])) {
        throw new EntityStorageException('Missing bundle for entity type ' . $this->entityTypeId);
      }

      $bundle = $values[$this->bundleKey];
      $entityClass = $this->getBundleClass($bundle);
    } else {
      $entityClass = $this->entityClass;
    }

    $entity = new $entityClass(array(), $this->entityTypeId, $bundle);

    foreach ($entity as $name => $field) {
      if (isset($values[$name])) {
        $entity->$name = $values[$name];
      }
      elseif (!array_key_exists($name, $values)) {
        $entity->get($name)->applyDefaultValue();
      }
      unset($values[$name]);
    }

    // Set any passed values for non-defined fields also.
    foreach ($values as $name => $value) {
      $entity->$name = $value;
    }
    return $entity;
  }


  /**
   * Maps from storage records to entity objects, and attaches fields.
   *
   * @param array $records
   *   Associative array of query results, keyed on the entity ID.
   * @param bool $load_from_revision
   *   Flag to indicate whether revisions should be loaded or not.
   *
   * @return array
   *   An array of entity objects implementing the EntityInterface.
   */
  protected function mapFromStorageRecords(array $records, $load_from_revision = FALSE) {
    if (!$records) {
      return array();
    }

    $values = array();
    foreach ($records as $id => $record) {
      $values[$id] = array();
      // Skip the item delta and item value levels (if possible) but let the
      // field assign the value as suiting. This avoids unnecessary array
      // hierarchies and saves memory here.
      foreach ($record as $name => $value) {
        // Handle columns named [field_name]__[column_name] (e.g for field types
        // that store several properties).
        if ($field_name = strstr($name, '__', TRUE)) {
          $property_name = substr($name, strpos($name, '__') + 2);
          $values[$id][$field_name][LanguageInterface::LANGCODE_DEFAULT][$property_name] = $value;
        }
        else {
          // Handle columns named directly after the field (e.g if the field
          // type only stores one property).
          $values[$id][$name][LanguageInterface::LANGCODE_DEFAULT] = $value;
        }
      }
    }

    // Initialize translations array.
    $translations = array_fill_keys(array_keys($values), array());

    // Load values from shared and dedicated tables.
    $this->loadFromSharedTables($values, $translations);
    $this->loadFromDedicatedTables($values, $load_from_revision);

    $entities = array();
    foreach ($values as $id => $entity_values) {
      if ($this->bundleKey) {
        $bundle = $entity_values[$this->bundleKey][LanguageInterface::LANGCODE_DEFAULT];
        $entityClass = $this->getBundleClass($bundle);
      } else {
        $bundle = FALSE;
        $entityClass = $this->entityClass;
      }

      // Turn the record into an entity class.
      $entities[$id] = new $entityClass($entity_values, $this->entityTypeId, $bundle, array_keys($translations[$id]));
    }

    return $entities;
  }
}
