<?php

namespace Drupal\managed\Core;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Language\LanguageInterface;


trait ManagedEntityTrait
{
  /**
   * Overrides __construct().
   *
   * @param array $values
   * @param string $entityTypeID
   * @param bool $bundle
   * @param array $translations
   */
  public function __construct(array $values, $entityTypeID, $bundle = FALSE, $translations = array()) {
    $this->entityTypeId = $entityTypeID;
    $entityType = $this->getEntityType();
    if (!($entityType instanceof ManagedEntityType)) {
      throw new \InvalidArgumentException();
    }

    $this->langcodeKey          = $entityType->getKey('langcode');
    $this->defaultLangcodeKey   = $entityType->getKey('default_langcode');
    $this->entityKeys['bundle'] = $bundle ? $bundle : $entityTypeID;
    $this->values               = $values;

    foreach ($entityType->getResetProperties($bundle) as $property) {
      unset($this->$property);
    }

    foreach ($entityType->getSystemProperties() as $property) {
      if (isset($values[$property][LanguageInterface::LANGCODE_DEFAULT])) {
        $this->$property = $values[$property][LanguageInterface::LANGCODE_DEFAULT];
      }
    }

    foreach ($entityType->getKeys() as $key => $fieldName) {
      if (!isset($values[$fieldName]) || !is_array($values[$fieldName])) {
        continue;
      }

      // We store untranslatable fields into an entity key without using a
      // langcode key.
      if (!$this->getFieldDefinition($fieldName)->isTranslatable()) {
        if (isset($values[$fieldName][LanguageInterface::LANGCODE_DEFAULT])) {
          $fieldValue = $values[$fieldName][LanguageInterface::LANGCODE_DEFAULT];
          if (is_array($fieldValue)) {
            if (isset($fieldValue[0]['value'])) {
              $this->entityKeys[$key] = $fieldValue[0]['value'];
            }
          } else {
            $this->entityKeys[$key] = $fieldValue;
          }
        }
      }
      else {
        // We save translatable fields such as the publishing status of a node
        // into an entity key array keyed by langcode as a performance
        // optimization, so we don't have to go through TypedData when we
        // need these values.
        foreach ($values[$fieldName] as $langCode => $fieldValue) {
          if (is_array($fieldValue)) {
            if (isset($fieldValue[0]['value'])) {
              $this->translatableEntityKeys[$key][$langCode] = $fieldValue[0]['value'];
            }
          } else {
            $this->translatableEntityKeys[$key][$langCode] = $fieldValue;
          }
        }
      }
    }

    // Initialize translations. Ensure we have at least an entry for the default
    // language.
    $data = array('status' => ContentEntityBase::TRANSLATION_EXISTING);
    $this->translations[LanguageInterface::LANGCODE_DEFAULT] = $data;
    $this->setDefaultLangcode();

    if ($translations) {
      foreach ($translations as $langCode) {
        if ($langCode != $this->defaultLangcode && $langCode != LanguageInterface::LANGCODE_DEFAULT) {
          $this->translations[$langCode] = $data;
        }
      }
    }

    foreach ($entityType->getRelations($bundle) as $name => $relation) {
      $this->$name = $relation->createCollection($this);
    }
  }


  public function __wakeup() {
    $entityType = $this->getEntityType();
    if (!($entityType instanceof ManagedEntityType)) {
      throw new \InvalidArgumentException();
    }

    $bundle = $this->entityKeys['bundle'];
    if ($bundle == $this->entityTypeId) {
      $bundle = FALSE;
    }

    foreach ($entityType->getResetProperties($bundle) as $property) {
      unset($this->$property);
    }

    parent::__wakeup();
  }


  /**
   * Gets the entity type definition.
   *
   * @return \Drupal\Core\Entity\EntityTypeInterface
   *   The entity type definition.
   */
  abstract public function getEntityType();


  /**
   * Gets the definition of a contained field.
   *
   * @param string $name
   *   The name of the field.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface|null
   *   The definition of the field or null if the field does not exist.
   */
  abstract public function getFieldDefinition($name);


  /**
   * Populates the local cache for the default language code.
   */
  abstract protected function setDefaultLangcode();
}
