<?php

namespace Drupal\managed\Core;


/**
 * Describes the interface of an installer.
 */
interface InstallerInterface
{
  /**
   * Perform all pending install actions.
   *
   * @return $this
   */
  public function install();


  /**
   * Perform all pending uninstall actions.
   *
   * @return $this
   */
  public function uninstall();


  /**
   * Return a detailed info about what has been performed.
   *
   * @return array
   *   A list of tasks that have been performed.
   */
  public function getPerformedActions();


  /**
   * Return a status message about pending installs.
   *
   * @return string|bool
   *   A status message or FALSE if there is nothing pending.
   */
  public function getPendingInstallInfo();


  /**
   * Return a status message about pending uninstalls.
   *
   * @return string|bool
   *   A status message or FALSE if there is nothing pending.
   */
  public function getPendingUninstallInfo();


  /**
   * Return a detailed info about pending uninstalls.
   *
   * @return array
   *   A list of tasks that will be performed.
   */
  public function getPendingUninstallDetails();
}
