<?php

namespace Drupal\managed\Core;

use Drupal\managed\Core\Installer\BundleInstaller;
use Drupal\managed\Core\Installer\DefinitionInstaller;
use Drupal\managed\Core\Installer\NodeTypeInstaller;
use Drupal\managed\Utils\ContainerAware;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * A central host that takes care of multiple installers.
 */
class InstallerHost extends ContainerAware
{
  /**
   * All installers attached to this host.
   *
   * @var \Drupal\managed\Core\InstallerInterface[]
   */
  private $installers;

  /**
   * @var int
   */
  private $disableCachesInvocations = 0;



  /**
   * Create a new ContainerAware instance.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container this instance should use.
   */
  public function __construct(ContainerInterface $container) {
    parent::__construct($container);

    $this->installers = array(
      new DefinitionInstaller($this),
      new BundleInstaller($this),
      new NodeTypeInstaller($this)
    );
  }


  /**
   * Perform all pending install actions.
   *
   * @return $this
   */
  public function install() {
    $this->getEntityManager()->clearCachedDefinitions();
    $this->getDiscovery()->discover();

    foreach ($this->installers as $installer) {
      $installer->install();
    }

    $this->getService('router.builder')->setRebuildNeeded();
  }


  /**
   * Perform all pending uninstall actions.
   *
   * @return $this
   */
  public function uninstall() {
    /** @var \Drupal\managed\Core\InstallerInterface[] $installers */
    $installers = array_reverse($this->installers);

    foreach ($installers as $installer) {
      $installer->uninstall();
    }

    $this->getService('router.builder')->setRebuildNeeded();
  }


  public function disableCaches() {
    $this->disableCachesInvocations += 1;

    if ($this->disableCachesInvocations == 1) {
      $this->useCaches(FALSE);
    }
  }


  public function enableCaches() {
    $this->disableCachesInvocations -= 1;

    if ($this->disableCachesInvocations == 0) {
      $this->useCaches(TRUE);
    }
  }


  private function useCaches($value = TRUE) {
    $entityManager = $this->getEntityManager();
    $discovery = $this->getDiscovery();

    $entityManager->useCaches($value);
    $discovery->useCaches($value);

    // Enabling caches again keeps freshly fetched data, seems like a bug
    // @see https://www.drupal.org/node/2583111
    if ($value) {
      $entityManager->useCaches(FALSE);
      $entityManager->useCaches(TRUE);
    }
  }


  /**
   * Return a detailed info about what has been performed.
   *
   * @return array
   *   A list of tasks that have been performed.
   */
  public function getPerformedActions() {
    $messages = array();

    foreach ($this->installers as $installer) {
      $installerMessages = $installer->getPerformedActions();
      if (is_array($installerMessages)) {
        $messages = array_merge($messages, $installerMessages);
      }
    }

    return $messages;
  }


  /**
   * Return a detailed info about what has been performed as a html list.
   *
   * @return bool|string
   */
  public function getPerformedActionsMarkup() {
    $messages = $this->getPerformedActions();
    if (count($messages) == 0) {
      return FALSE;
    }

    $markup = '<ul>';
    foreach ($messages as $message) {
      $markup .= '<li>' . $message . '</li>';
    }

    return $markup . '</ul>';
  }


  /**
   * Return a status message about pending installs.
   *
   * @return string|bool
   *   A status message or FALSE if there is nothing pending.
   */
  public function getPendingInstallInfo() {
    $this->disableCaches();
    $messages = array();

    foreach ($this->installers as $installer) {
      try {
        $message = $installer->getPendingInstallInfo();
        if ($message !== FALSE) {
          $messages[] = $message;
        }
      } catch (\Exception $e) {
        drupal_set_message($e->getMessage(), 'error');
      }
    }

    $this->enableCaches();
    return count($messages) > 0 ? implode(' ', $messages) : FALSE;
  }


  /**
   * Return a status message about pending uninstalls.
   *
   * @return string|bool
   *   A status message or FALSE if there is nothing pending.
   */
  public function getPendingUninstallInfo() {
    $this->disableCaches();

    /** @var \Drupal\managed\Core\InstallerInterface[] $installers */
    $installers = array_reverse($this->installers);
    $messages = array();

    foreach ($installers as $installer) {
      try {
        $message = $installer->getPendingUninstallInfo();
        if ($message !== FALSE) {
          $messages[] = $message;
        }
      } catch (\Exception $e) {
        drupal_set_message($e->getMessage(), 'error');
      }
    }

    $this->enableCaches();
    return count($messages) > 0 ? implode(' ', $messages) : FALSE;
  }


  /**
   * Return a detailed info about pending uninstalls.
   *
   * @return array
   *   A list of tasks that will be performed.
   */
  public function getPendingUninstallDetails() {
    $this->disableCaches();

    /** @var \Drupal\managed\Core\InstallerInterface[] $installers */
    $installers = array_reverse($this->installers);
    $messages = array();

    foreach ($installers as $installer) {
      try {
        $installerMessages = $installer->getPendingUninstallDetails();
        if (is_array($installerMessages)) {
          $messages = array_merge($messages, $installerMessages);
        }
      } catch (\Exception $e) {
        drupal_set_message($e->getMessage(), 'error');
      }
    }

    $this->enableCaches();
    return $messages;
  }


  /**
   * Return the discovery service.
   *
   * @return \Drupal\managed\Discovery\Discovery
   */
  public function getDiscovery() {
    return $this->getService('managed.discovery');
  }


  /**
   * Return the entity manager.
   *
   * @return \Drupal\Core\Entity\EntityManagerInterface
   */
  public function getEntityManager() {
    return $this->getService('entity.manager');
  }
}
