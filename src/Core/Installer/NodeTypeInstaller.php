<?php

namespace Drupal\managed\Core\Installer;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\managed\Core\InstallerHost;
use Drupal\managed\Core\InstallerInterface;
use Drupal\managed\Utils\ContainerAware;


/**
 * Manages node type installations.
 */
class NodeTypeInstaller extends ContainerAware implements InstallerInterface
{
  use StringTranslationTrait;


  /**
   * @var \Drupal\managed\Core\InstallerHost
   */
  private $host;

  /**
   * Cached results of the getAvailableBundles() method.
   *
   * @var \Drupal\managed\Annotation\NodeAnnotation[]
   */
  private $availableNodeTypes;

  /**
   * All installed node types.
   *
   * @var \Drupal\node\Entity\NodeType[]
   */
  private $installedNodeTypes;

  /**
   * The storage instance used by node types.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private $nodeTypeStorage;

  /**
   * A list of descriptions of the actions that have been performed.
   *
   * @var string[]
   */
  private $actionLog = array();



  /**
   * Create a new NodeTypeInstaller instance.
   *
   * @param \Drupal\managed\Core\InstallerHost $host
   */
  public function __construct(InstallerHost $host) {
    $container = $host->getContainer();
    parent::__construct($container);

    $this->host               = $host;
    $this->availableNodeTypes = $this->getDiscovery()->getUncachedNodeAnnotations();
    $this->nodeTypeStorage    = $this->getEntityManager()->getStorage('node_type');

    $this->loadNodeTypes();
  }


  /**
   * Load the list of installed node types.
   *
   * @return $this
   */
  protected function loadNodeTypes() {
    $this->installedNodeTypes = array();

    foreach ($this->nodeTypeStorage->getQuery()->execute() as $nodeType) {
      $this->installedNodeTypes[$nodeType] = $this->nodeTypeStorage->load($nodeType);
    }

    return $this;
  }


  /**
   * Uninstall all node types that are no longer available.
   *
   * @return $this
   */
  public function uninstall() {
    $nodeTypes = $this->getNodeTypesToUninstall();
    if (count($nodeTypes) == 0) {
      return $this;
    }

    $locked = \Drupal::state()->get('node.type.locked');
    foreach ($nodeTypes as $nodeType) {
      unset($locked[$nodeType->id()]);
      $nodeType->unsetThirdPartySetting('managed', 'active');

      $numNodes = $this->getQueryFactory()->get('node')
        ->condition('type', $nodeType->id())
        ->count()
        ->execute();

      if ($numNodes) {
        $nodeType->save();
        drupal_set_message($this->formatPlural($numNodes,
          '%type is used by 1 piece of content on your site. You can not remove this content type until you have removed all of the %type content.',
          '%type is used by @count pieces of content on your site. You may not remove %type until you have removed all of the %type content.',
          array('%type' => $nodeType->label())
        ), 'warning');
      } else {
        $nodeType->delete();
        $this->actionLog[] = $this->t(
          'The node type `@node_type` has been uninstalled.',
          array('@node_type' => $nodeType->label())
        );
      }
    }

    \Drupal::state()->set('node.type.locked', $locked);
    return $this->loadNodeTypes();
  }


  /**
   * Install all new node types.
   *
   * @return $this
   */
  public function install() {
    $nodes = $this->getNodeTypesToInstall();
    if (count($nodes) == 0) {
      return $this;
    }

    $locked = \Drupal::state()->get('node.type.locked');
    foreach ($nodes as $node) {
      if (isset($this->installedNodeTypes[$node->id()])) {
        $nodeType = $this->installedNodeTypes[$node->id()];
      } else {
        $nodeType = $this->nodeTypeStorage->create($node->getDefinition());
      }

      $locked[$nodeType->id()] = 'managed';
      $nodeType->setThirdPartySetting('managed', 'active', TRUE);
      $nodeType->save();

      $this->actionLog[] = $this->t(
        'The node type `@node_type` has been installed.',
        array('@node_type' => $node->getLabel())
      );
    }

    \Drupal::state()->set('node.type.locked', $locked);
    return $this->loadNodeTypes();
  }


  /**
   * Return a detailed info about what has been performed.
   *
   * @return array
   *   A list of tasks that have been performed.
   */
  public function getPerformedActions() {
    return $this->actionLog;
  }


  /**
   * Return a status message of pending bundle installs.
   *
   * @return string|bool
   */
  public function getPendingInstallInfo() {
    $count = count($this->getNodeTypesToInstall());

    if ($count > 0) {
      return $this->formatPlural($count,
        'One node type must be installed.',
        '@count node types must be installed.'
      );
    } else {
      return FALSE;
    }
  }


  /**
   * Return a status message of pending bundle uninstalls.
   *
   * @return string|bool
   */
  public function getPendingUninstallInfo() {
    $count = count($this->getNodeTypesToUninstall());

    if ($count > 0) {
      return $this->formatPlural($count,
        'One node type can be uninstalled.',
        '@count node types can be uninstalled.'
      );
    } else {
      return FALSE;
    }
  }


  /**
   * Return a detailed info about pending uninstalls.
   *
   * @return array
   *   A list of tasks that will be performed.
   */
  public function getPendingUninstallDetails() {
    $messages = array();
    $toUninstall = $this->getNodeTypesToUninstall();

    foreach ($toUninstall as $nodeType) {
      $messages[] = $this->t(
        'The node type <em>@node_type</em> will be removed.',
        array(
          '@node_type' => $nodeType->label(),
        )
      );
    }

    return $messages;
  }


  /**
   * Return a list of all node types that must be installed.
   *
   * @return \Drupal\managed\Annotation\NodeAnnotation[]
   */
  public function getNodeTypesToInstall() {
    $result = array();

    foreach ($this->availableNodeTypes as $node) {
      if (isset($this->installedNodeTypes[$node->id()])) {
        $installed = $this->installedNodeTypes[$node->id()];
        if (!$installed->getThirdPartySetting('managed', 'active')) {
          $result[] = $node;
        }
      } else {
        $result[] = $node;
      }
    }

    return $result;
  }


  /**
   * Return a list of all node types that must be uninstalled.
   *
   * @return \Drupal\node\Entity\NodeType[]
   */
  public function getNodeTypesToUninstall() {
    $result = array();

    foreach ($this->installedNodeTypes as $nodeType) {
      if (!$nodeType->getThirdPartySetting('managed', 'active', FALSE)) {
        continue;
      }

      foreach ($this->availableNodeTypes as $node) {
        if ($node->id() == $nodeType->id()) {
          continue 2;
        }
      }

      $result[] = $nodeType;
    }

    return $result;
  }


  /**
   * The query factory to create entity queries.
   *
   * @return \Drupal\Core\Entity\Query\QueryFactory
   */
  public function getQueryFactory() {
    return $this->getService('entity.query');
  }


  /**
   * @return \Drupal\Core\Entity\EntityManager
   */
  public function getEntityManager() {
    return $this->getService('entity.manager');
  }


  /**
   * @return \Drupal\managed\Discovery\Discovery
   */
  public function getDiscovery() {
    return $this->getService('managed.discovery');
  }
}
