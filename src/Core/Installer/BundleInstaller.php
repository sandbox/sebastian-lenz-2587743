<?php

namespace Drupal\managed\Core\Installer;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\managed\Core\InstallerHost;
use Drupal\managed\Core\InstallerInterface;
use Drupal\managed\Utils\ContainerAware;


/**
 * Manages bundle installations.
 */
class BundleInstaller extends ContainerAware implements InstallerInterface
{
  use StringTranslationTrait;


  /**
   * @var \Drupal\managed\Core\InstallerHost
   */
  private $host;

  /**
   * @var \Drupal\managed\Core\ManagedEntityTypeManager
   */
  private $manager;

  /**
   * @var \Drupal\Core\Entity\EntityManager
   */
  private $entityManager;

  /**
   * @var \Drupal\managed\Core\PersistentData
   */
  private $data;

  /**
   * Cached results of the getAvailableBundles() method.
   *
   * @var array
   */
  private $availableBundles;

  /**
   * A list of descriptions of the actions that have been performed.
   *
   * @var string[]
   */
  private $actionLog = array();


  /**
   * Create a new BundleInstaller instance.
   *
   * @param \Drupal\managed\Core\InstallerHost $host
   */
  public function __construct(InstallerHost $host) {
    $container = $host->getContainer();
    parent::__construct($container);

    $this->host          = $host;
    $this->entityManager = $container->get('entity.manager');
    $this->data          = $container->get('managed.persistent');
    $this->manager       = $container->get('managed.entity_manager');
  }


  /**
   * Uninstall all bundle definitions that are no longer available.
   *
   * @return $this
   */
  public function uninstall() {
    $installed   = $this->getInstalledBundles();
    $toUninstall = $this->getBundlesToUninstall();

    foreach ($toUninstall as $bundle) {
      try {
        $this->entityManager->onBundleDelete($bundle['bundle'], $bundle['entity']);
      } catch (\Exception $e) {
        // TODO: Add a message!
      }

      $this->actionLog[] = $this->t(
        'The bundle `@bundle` of the entity `@entity` has been uninstalled.',
        array(
          '@bundle' => $bundle['bundle'],
          '@entity' => $bundle['entity']
        )
      );
    }

    $this->setInstalledBundles(array_diff_key($installed, $toUninstall));
    return $this;
  }


  /**
   * Install all new bundle definitions
   *
   * @return $this
   */
  public function install() {
    $installed = $this->getInstalledBundles();
    $toInstall = $this->getBundlesToInstall();

    foreach ($toInstall as $bundle) {
      $this->entityManager->onBundleCreate($bundle['bundle'], $bundle['entity']);

      $this->actionLog[] = $this->t(
        'The bundle `@bundle` of the entity `@entity` has been installed.',
        array(
          '@bundle' => $bundle['bundle'],
          '@entity' => $bundle['entity']
        )
      );
    }

    $this->setInstalledBundles(array_merge($installed, $toInstall));
    return $this;
  }


  /**
   * Set the list of installed bundles.
   *
   * @param array $value
   * @return $this
   */
  protected function setInstalledBundles($value) {
    $this->data->setInstalledBundles($value);
    return $this;
  }


  /**
   * Return a list of all installed bundles.
   *
   * @return array
   *   A list of installed bundles, keyed by the full entity name.
   *   Each item contains an array specifying `bundle` and `entity`.
   */
  public function getInstalledBundles() {
    return $this->data->getInstalledBundles();
  }


  /**
   * Return a detailed info about what has been performed.
   *
   * @return array
   *   A list of tasks that have been performed.
   */
  public function getPerformedActions() {
    return $this->actionLog;
  }


  /**
   * Return a status message of pending bundle installs.
   *
   * @return string|bool
   */
  public function getPendingInstallInfo() {
    $count = count($this->getBundlesToInstall());

    if ($count > 0) {
      return $this->formatPlural($count,
        'One bundle must be installed.',
        '@count bundles must be installed.'
      );
    } else {
      return FALSE;
    }
  }


  /**
   * Return a status message of pending bundle uninstalls.
   *
   * @return string|bool
   */
  public function getPendingUninstallInfo() {
    $count = count($this->getBundlesToUninstall());

    if ($count > 0) {
      return $this->formatPlural($count,
        'One bundle can be uninstalled.',
        '@count bundles can be uninstalled.'
      );
    } else {
      return FALSE;
    }
  }


  /**
   * Return a detailed info about pending uninstalls.
   *
   * @return array
   *   A list of tasks that will be performed.
   */
  public function getPendingUninstallDetails() {
    $messages = array();
    $toUninstall = $this->getBundlesToUninstall();

    foreach ($toUninstall as $bundle) {
      $messages[] = $this->t(
        'The bundle <em>@bundle</em> of the entity <em>@entity</em> will be removed.',
        array(
          '@bundle' => $bundle['bundle'],
          '@entity' => $bundle['entity']
        )
      );
    }

    return $messages;
  }


  /**
   * Return a list of all bundles that must be installed.
   *
   * @return array
   *   A list of bundles, keyed by the full entity name.
   *   Each item contains an array specifying `bundle` and `entity`.
   */
  public function getBundlesToInstall() {
    return array_diff_key($this->getAvailableBundles(), $this->getInstalledBundles());
  }


  /**
   * Return a list of all bundles that must be uninstalled.
   *
   * @return array
   *   A list of bundles, keyed by the full entity name.
   *   Each item contains an array specifying `bundle` and `entity`.
   */
  public function getBundlesToUninstall() {
    $a = $this->getInstalledBundles();
    $b = $this->getAvailableBundles();
    return array_diff_key($a, $b);
  }


  /**
   * Return a list of all available bundles.
   *
   * @return array
   *   A list of bundles, keyed by the full entity name.
   *   Each item contains an array specifying `bundle` and `entity`.
   */
  public function getAvailableBundles() {
    if (!isset($this->availableBundles)) {
      $this->host->disableCaches();
      $bundles = array();
      $types = $this->manager->getAllManagedTypes();

      foreach ($types as $type) {
        if (!$type->hasBundles() || $type->id() == 'node') {
          continue;
        }

        $typeID = $type->id();
        foreach ($type->getBundles() as $bundle) {
          $bundleID = $bundle->id();
          $bundles[$typeID . ':' . $bundleID] = array(
            'bundle' => $bundleID,
            'entity' => $typeID
          );
        }
      }

      $this->availableBundles = $bundles;
      $this->host->enableCaches();
    }

    return $this->availableBundles;
  }
}
