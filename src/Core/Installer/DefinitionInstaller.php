<?php

namespace Drupal\managed\Core\Installer;

use Drupal\Core\Entity\DynamicallyFieldableEntityStorageInterface;
use Drupal\Core\Entity\EntityDefinitionUpdateManager;
use Drupal\managed\Core\InstallerHost;
use Drupal\managed\Core\InstallerInterface;
use Drupal\managed\Core\ManagedEntityType;
use Drupal\managed\Utils\ContainerAwareTrait;


/**
 * Manages entity definition updates.
 */
class DefinitionInstaller extends EntityDefinitionUpdateManager implements InstallerInterface
{
  use ContainerAwareTrait;


  /**
   * @var \Drupal\managed\Core\InstallerHost
   */
  private $host;

  /**
   * @var \Drupal\managed\Discovery\Discovery
   */
  private $discovery;

  /**
   * Cached results of the getChangeList method.
   *
   * @var array
   */
  private $changeList;

  /**
   * A list of descriptions of the actions that have been performed.
   *
   * @var string[]
   */
  private $actionLog;



  /**
   * Create a new BundleInstaller instance.
   *
   * @param \Drupal\managed\Core\InstallerHost $host
   */
  public function __construct(InstallerHost $host) {
    parent::__construct($host->getEntityManager());

    $this->host      = $host;
    $this->discovery = $host->getService('managed.discovery');
  }


  /**
   * Perform all pending install actions.
   *
   * @return $this
   */
  public function install() {
    $this->applyUpdates($this->getInstallChangeList());
  }


  /**
   * Perform all pending uninstall actions.
   *
   * @return $this
   */
  public function uninstall() {
    $this->applyUpdates($this->getUninstallChangeList());
  }


  /**
   * Utility function that filters the given change list and either
   * removes all delete operations or leaves only the delete operations.
   *
   * @param $changeList
   *   The change list that should be filtered.
   * @param bool|FALSE $inverse
   *   Set to TRUE to return only delete operations.
   * @return array
   *   The filtered change list.
   */
  protected function filterChangeList($changeList, $inverse = FALSE) {
    foreach ($changeList as $entityTypeID => &$changes) {
      if (!empty($changes['entity_type']) && (($changes['entity_type'] == static::DEFINITION_DELETED) != $inverse)) {
        unset($changeList[$entityTypeID]['entity_type']);
      }

      if (!empty($changes['field_storage_definitions'])) {
        $definitions = &$changes['field_storage_definitions'];

        foreach ($definitions as $name => $change) {
          if (($change == static::DEFINITION_DELETED) != $inverse) {
            unset($definitions[$name]);
          }
        }

        if (count($definitions) == 0) {
          unset($changes['field_storage_definitions']);
        }
      }
    }

    return array_filter($changeList);
  }


  /**
   * Create a brief description of the given change list.
   *
   * @param $changeList
   * @return bool|string
   */
  protected function describeChangeList($changeList) {
    $summary = array(
      'type'  => array(),
      'field' => array()
    );

    foreach ($changeList as $entityTypeID => $changes) {
      if (!empty($changes['entity_type'])) {
        if (!isset($summary['type'][$changes['entity_type']])) {
          $summary['type'][$changes['entity_type']] = 1;
        } else {
          $summary['type'][$changes['entity_type']] += 1;
        }
      }

      if (!empty($changes['field_storage_definitions'])) {
        foreach ($changes['field_storage_definitions'] as $name => $change) {
          if (!isset($summary['field'][$change])) {
            $summary['field'][$change] = 1;
          } else {
            $summary['field'][$change] += 1;
          }
        }
      }
    }

    $messages = array();
    foreach ($summary as $type => $operations) {
      foreach ($operations as $operation => $count) {
        $messages[] = $this->describeChange($type, $operation, $count);
      }
    }

    $messages = array_filter($messages);
    return $messages ? implode(' ', $messages) : FALSE;
  }


  /**
   * Describe the given summed up change.
   *
   * @param string $type
   * @param int $operation
   * @param int $count
   * @return bool|string
   */
  protected function describeChange($type, $operation, $count) {
    switch ($type) {
      case 'type':
        switch ($operation) {
          case static::DEFINITION_CREATED:
            return $this->formatPlural($count,
              'One entity type must be installed.',
              '@count entity types must be installed.'
            );
          case static::DEFINITION_UPDATED:
            return $this->formatPlural($count,
              'One entity type must be updated.',
              '@count entity types must be updated.'
            );
          case static::DEFINITION_DELETED:
            return $this->formatPlural($count,
              'One entity type can be uninstalled.',
              '@count entity types can be uninstalled.'
            );
        }
        break;
      case 'field':
        switch ($operation) {
          case static::DEFINITION_CREATED:
            return $this->formatPlural($count,
              'One field must be created.',
              '@count fields must be created.'
            );
          case static::DEFINITION_UPDATED:
            return $this->formatPlural($count,
              'One field must be updated.',
              '@count fields must be updated.'
            );
          case static::DEFINITION_DELETED:
            return $this->formatPlural($count,
              'One field can be removed.',
              '@count fields can be removed.'
            );
        }
        break;
    }

    return FALSE;
  }


  /**
   * Applies all the detected valid changes.
   *
   * @param null $changeList
   *   Optional change list that should be processed.
   * @return $this|void
   */
  public function applyUpdates($changeList = NULL) {
    if (is_null($changeList)) {
      $changeList = $this->getChangeList();
    }

    foreach ($changeList as $entityTypeID => $changes) {
      try {
        if (!empty($changes['entity_type'])) {
          $this->doEntityUpdate($changes['entity_type'], $entityTypeID);
        }

        if (!empty($changes['field_storage_definitions'])) {
          $currentDefinitions  = $this->entityManager->getFieldStorageDefinitions($entityTypeID);
          $previousDefinitions = $this->entityManager->getLastInstalledFieldStorageDefinitions($entityTypeID);

          foreach ($changes['field_storage_definitions'] as $name => $change) {
            $current  = isset($currentDefinitions[$name])  ? $currentDefinitions[$name]  : NULL;
            $previous = isset($previousDefinitions[$name]) ? $previousDefinitions[$name] : NULL;
            $this->doFieldUpdate($change, $current, $previous);
          }
        }
      } catch (\Exception $e) {
        drupal_set_message($e->getMessage(), 'error');
      }
    }

    unset($this->changeList);
    return $this;
  }


  /**
   * Performs an entity type definition update.
   *
   * @param string $change
   *   The operation to perform, either static::DEFINITION_CREATED or
   *   static::DEFINITION_UPDATED.
   * @param string $entityTypeID
   *   The entity type ID.
   */
  protected function doEntityUpdate($change, $entityTypeID) {
    $translationData = array('@entity' => $entityTypeID);

    switch ($change) {
      case static::DEFINITION_CREATED:
        parent::doEntityUpdate($change, $entityTypeID);

        $this->actionLog[] = $this->t(
          'The entity `@entity` has been installed.',
          $translationData
        )->render();
        break;
      case static::DEFINITION_UPDATED:
        parent::doEntityUpdate($change, $entityTypeID);

        $this->actionLog[] = $this->t(
          'The entity `@entity` has been updated.',
          $translationData
        )->render();
        break;
      case static::DEFINITION_DELETED:
        $original = $this->entityManager->getLastInstalledDefinition($entityTypeID);
        $this->entityManager->onEntityTypeDelete($original);

        $this->actionLog[] = $this->t(
          'The entity `@entity` has been uninstalled.',
          $translationData
        )->render();
        break;
    }
  }


  /**
   * Performs a field storage definition update.
   *
   * @param string $change
   *   The operation to perform, possible values are static::DEFINITION_CREATED,
   *   static::DEFINITION_UPDATED or static::DEFINITION_DELETED.
   * @param array|null $storageDefinition
   *   The new field storage definition.
   * @param array|null $originalStorageDefinition
   *   The original field storage definition.
   */
  protected function doFieldUpdate($change, $storageDefinition = NULL, $originalStorageDefinition = NULL) {
    $definition = $storageDefinition ?: $originalStorageDefinition;
    $translationData = array(
      '@entity' => $definition->getTargetEntityTypeId(),
      '@field'  => $definition->getName(),
    );

    switch ($change) {
      case static::DEFINITION_CREATED:
        $this->actionLog[] = $this->t(
          'The field `@field` of the entity `@entity` has been created.',
          $translationData
        )->render();
        break;
      case static::DEFINITION_UPDATED:
        $this->actionLog[] = $this->t(
          'The field `@field` of the entity `@entity` has been updated.',
          $translationData
        )->render();
        break;
      case static::DEFINITION_DELETED:
        $this->actionLog[] = $this->t(
          'The field `@field` of the entity `@entity` has been removed.',
          $translationData
        )->render();
        break;
      default:
    }

    parent::doFieldUpdate($change, $storageDefinition, $originalStorageDefinition);
  }


  /**
   * Return a detailed info about what has been performed.
   *
   * @return array
   *   A list of tasks that have been performed.
   */
  public function getPerformedActions() {
    return $this->actionLog;
  }


  /**
   * Return a status message about pending installs.
   *
   * @return string|bool
   *   A status message or FALSE if there is nothing pending.
   */
  public function getPendingInstallInfo() {
    return $this->describeChangeList($this->getInstallChangeList());
  }


  /**
   * Return a status message about pending uninstalls.
   *
   * @return string|bool
   *   A status message or FALSE if there is nothing pending.
   */
  public function getPendingUninstallInfo() {
    return $this->describeChangeList($this->getUninstallChangeList());
  }


  /**
   * Return a detailed info about pending uninstalls.
   *
   * @return array
   *   A list of tasks that will be performed.
   */
  public function getPendingUninstallDetails() {
    $messages = array();
    $changeList = $this->getUninstallChangeList();

    foreach ($changeList as $entityTypeID => $changes) {
      if (!empty($changes['entity_type'])) {
        $messages[] = $this->t(
          'The entity `@entity` will be uninstalled.',
          array('@entity' => $entityTypeID)
        );
      }

      if (!empty($changes['field_storage_definitions'])) {
        foreach ($changes['field_storage_definitions'] as $name => $change) {
          $messages[] = $this->t(
            'The field `@field` of the entity `@entity` will be removed.',
            array(
              '@entity' => $entityTypeID,
              '@field'  => $name
            )
          );
        }
      }
    }

    return $messages;
  }


  /**
   * Get a list of changes that should be applied when installing.
   *
   * @return array
   */
  protected function getInstallChangeList() {
    return $this->filterChangeList($this->getChangeList(), FALSE);
  }


  /**
   * Get a list of changes that should be applied when uninstalling.
   *
   * @return array
   */
  protected function getUninstallChangeList() {
    return $this->filterChangeList($this->getChangeList(), TRUE);
  }


  /**
   * Gets a list of changes to entity type and field storage definitions.
   *
   * @return array
   *   An associative array keyed by entity type id of change descriptors. Every
   *   entry is an associative array with the following optional keys:
   *   - entity_type: a scalar having only the DEFINITION_UPDATED value.
   *   - field_storage_definitions: an associative array keyed by field name of
   *     scalars having one value among:
   *     - DEFINITION_CREATED
   *     - DEFINITION_UPDATED
   *     - DEFINITION_DELETED
   */
  protected function getChangeList() {
    if (isset($this->changeList)) {
      return $this->changeList;
    }

    $this->host->disableCaches();

    $changeList  = array();
    $installed   = $this->getAllInstalledDefinitions();
    $definitions = $this->entityManager->getDefinitions();

    foreach ($definitions as $entityTypeID => $entityType) {
      if (!($entityType instanceof ManagedEntityType)) {
        continue;
      }

      $installedKey = $entityTypeID . '.entity_type';
      if (!isset($installed[$installedKey])) {
        $changeList[$entityTypeID]['entity_type'] = static::DEFINITION_CREATED;
      }
      else {
        $original = $installed[$installedKey];
        $lastChangeList = $changeList;
        unset($installed[$installedKey]);

        try {
          if ($this->requiresEntityStorageSchemaChanges($entityType, $original)) {
            $changeList[$entityTypeID]['entity_type'] = static::DEFINITION_UPDATED;
          }

          $hasEntityChanged = isset($changeList[$entityTypeID]['entity_type']);
          $fieldChanges     = $this->getFieldChangeList($entityTypeID, $hasEntityChanged);
          if ($fieldChanges) {
            $changeList[$entityTypeID]['field_storage_definitions'] = $fieldChanges;
          }
        } catch (\Exception $e) {
          $changeList = $lastChangeList;
          drupal_set_message(t(
            'The entity type `@type` could not be analyzed: @message.',
            array(
              '@type'    => $entityTypeID,
              '@message' => $e->getMessage())
          ), 'warning');
        }
      }
    }

    foreach ($installed as $original) {
      $changeList[$original->id()]['entity_type'] = static::DEFINITION_DELETED;
    }

    $this->host->enableCaches();
    $this->changeList = array_filter($changeList);

    return $this->changeList;
  }


  /**
   * @param string $entityTypeID
   * @param bool $hasEntityChanged
   * @return array|null
   */
  protected function getFieldChangeList($entityTypeID, $hasEntityChanged) {
    $fieldChanges = array();
    $storage = $this->entityManager->getStorage($entityTypeID);
    if (!($storage instanceof DynamicallyFieldableEntityStorageInterface)) {
      return $fieldChanges;
    }

    $current = $this->entityManager->getFieldStorageDefinitions($entityTypeID);
    $previous = $this->entityManager->getLastInstalledFieldStorageDefinitions($entityTypeID);

    // Detect created field storage definitions.
    foreach (array_diff_key($current, $previous) as $name => $definition) {
      $fieldChanges[$name] = static::DEFINITION_CREATED;
    }

    // Detect deleted field storage definitions.
    foreach (array_diff_key($previous, $current) as $name => $definition) {
      $fieldChanges[$name] = static::DEFINITION_DELETED;
    }

    // Detect updated field storage definitions.
    foreach (array_intersect_key($current, $previous) as $name => $definition) {
      if (!$hasEntityChanged && $this->requiresFieldStorageSchemaChanges($definition, $previous[$name])) {
        $fieldChanges[$name] = static::DEFINITION_UPDATED;
      }
    }

    return $fieldChanges;
  }


  /**
   * @return \Drupal\managed\Core\ManagedEntityType[]
   */
  protected function getAllInstalledDefinitions() {
    $factory     = $this->getKeyValueFactory();
    $definitions = $factory->get('entity.definitions.installed');
    $result      = array();

    foreach ($definitions->getAll() as $key => $definition) {
      if ($definition instanceof ManagedEntityType || $key == 'node.entity_type') {
        $result[$key] = $definition;
      }
    }

    return $result;
  }


  /**
   * @return \Drupal\Core\KeyValueStore\KeyValueFactoryInterface
   */
  protected function getKeyValueFactory() {
    return $this->getService('keyvalue');
  }
}
