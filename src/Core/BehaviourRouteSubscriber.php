<?php

namespace Drupal\managed\Core;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\managed\Core\ManagedEntityTypeManager;
use Symfony\Component\Routing\RouteCollection;


class BehaviourRouteSubscriber extends RouteSubscriberBase
{
  /**
   * The managed entity manager.
   *
   * @var \Drupal\managed\Core\ManagedEntityTypeManager
   */
  private $manager;



  /**
   * Create a new RouteSubscriber instance.
   *
   * @param \Drupal\managed\Core\ManagedEntityTypeManager $manager
   */
  public function __construct(ManagedEntityTypeManager $manager) {
    $this->manager = $manager;
  }


  /**
   * Alters existing routes for a specific collection.
   *
   * @param \Symfony\Component\Routing\RouteCollection $collection
   *   The route collection for adding routes.
   */
  protected function alterRoutes(RouteCollection $collection) {
    foreach ($this->manager->getAllManagedTypes() as $entityType) {
      $behaviour = $this->manager->getBehaviour($entityType);
      if (is_null($behaviour)) {
        continue;
      }

      $behaviour->onAlterRoutes($collection);
    }
  }
}
