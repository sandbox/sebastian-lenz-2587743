<?php

namespace Drupal\managed\Core;

use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;


/**
 * Stores persistent data.
 */
class PersistentData
{
  /**
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  private $store;

  /**
   * @var array
   */
  private $installedBundles;

  private $entityAnnotations;

  private $nodeAnnotations;



  /**
   * Create a new PersistentData instance.
   *
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $keyValueFactory
   */
  public function __construct(KeyValueFactoryInterface $keyValueFactory) {
    $this->store = $keyValueFactory->get('managed');
  }


  /**
   * @param array $value
   * @return $this
   */
  public function setInstalledBundles($value) {
    $this->installedBundles = $value;
    $this->store->set('installed_bundles', $value);

    return $this;
  }


  /**
   * @return array
   */
  public function getInstalledBundles() {
    if (!isset($this->installedBundles)) {
      $this->installedBundles = $this->store->get('installed_bundles', array());
    }

    return $this->installedBundles;
  }


  public function setEntityAnnotations($entities) {
    $this->entityAnnotations = $entities;
    $this->store->set('entity_annotations', $entities);

    return $this;
  }


  public function getEntityAnnotations() {
    if (!isset($this->entityAnnotations)) {
      $this->entityAnnotations = $this->store->get('entity_annotations', array());
    }

    return $this->entityAnnotations;
  }


  public function setNodeAnnotations($nodes) {
    $this->nodeAnnotations = $nodes;
    $this->store->set('node_annotations', $nodes);

    return $this;
  }


  public function getNodeAnnotations() {
    if (!isset($this->nodeAnnotations)) {
      $this->nodeAnnotations = $this->store->get('node_annotations', array());
    }

    return $this->nodeAnnotations;
  }
}
