<?php

namespace Drupal\managed\Core;

use Drupal\Core\Entity\ContentEntityType;
use Drupal\managed\Utils\ContainerAware;


class ManagedNodeTypeManager extends ContainerAware implements ManagedEntityTypeDefinitionInterface
{
  /**
   * Return the plural version of the label.
   *
   * @return string
   */
  public function getPluralLabel() {
    return 'Nodes';
  }


  /**
   * Return all fields attached to this annotation.
   *
   * @return \Drupal\managed\Annotation\Field\AbstractFieldAnnotation[]
   */
  public function getFields() {
    return array();
  }


  /**
   * Return all relations attached to this annotation.
   *
   * @return \Drupal\managed\Annotation\Relation\AbstractRelationAnnotation[]
   */
  public function getRelations() {
    return array();
  }


  /**
   * Return a list of all managed node types.
   *
   * @return \Drupal\managed\Annotation\NodeAnnotation[]
   */
  public function getBundles() {
    return $this->getDiscovery()->getNodeAnnotations();
  }


  /**
   * Return the behaviour definition attached to this entity.
   *
   * @return \Drupal\managed\Annotation\Behaviour\AbstractBehaviourAnnotation
   */
  public function getBehaviour() {
    return NULL;
  }


  /**
   * @return \Drupal\managed\Discovery\Discovery
   */
  public function getDiscovery() {
    return $this->getService('managed.discovery');
  }


  /**
   * Implements hook_entity_type_alter().
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entityType
   * @return \Drupal\Core\Entity\EntityTypeInterface|\Drupal\managed\Core\ManagedEntityType
   */
  public function onNodeTypeAlter($entityType) {
    if (!($entityType instanceof ContentEntityType)) {
      return $entityType;
    }

    $entityType = ManagedEntityType::fromEntityType($entityType);
    $entityType->pullDefinitionData($this);
    $entityType->setClass('Drupal\managed\Node');
    $entityType->setHandlerClass('storage', 'Drupal\managed\Core\Storage\ManagedNodeStorage');

    return $entityType;
  }
}
