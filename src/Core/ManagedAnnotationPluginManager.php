<?php

namespace Drupal\managed\Core;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\managed\Vendor\Addendum\Addendum;


/**
 * A plugin manager that collects all available annotations.
 */
class ManagedAnnotationPluginManager extends DefaultPluginManager
{
  /**
   * Create a new ManagedAnnotationPluginManager instance.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations,
   * @param CacheBackendInterface $cache
   *   The cache backend to use.
   * @param ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache, ModuleHandlerInterface $module_handler) {
    parent::__construct('Annotation', $namespaces, $module_handler, null, 'Drupal\managed\Annotation\Drupal\ManagedAnnotation');

    $this->setCacheBackend($cache, 'managed:annotations');
  }


  /**
   * Activate all available annotations by passing them to Addendum.
   *
   * @return $this
   */
  public function activate() {
    Addendum::removeAllAnnotations();

    foreach ($this->getDefinitions() as $definition) {
      Addendum::addAnnotation($definition['id'], $definition['class']);
    }

    return $this;
  }
}
