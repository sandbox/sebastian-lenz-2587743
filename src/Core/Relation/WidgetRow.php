<?php

namespace Drupal\managed\Core\Relation;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\managed\Entity;


class WidgetRow extends ChildForm
{
  /**
   * @var int
   */
  protected $index;

  /**
   * @var string
   */
  protected $id;

  /**
   * @var \Drupal\managed\Core\Relation\Widget
   */
  protected $widget;

  /**
   * @var bool
   */
  protected $isDeleted = FALSE;

  /**
   * @var bool
   */
  protected $isNewlyCreated = FALSE;

  /**
   * @var bool
   */
  protected $isActionTarget = FALSE;

  /**
   * @var \Drupal\managed\Entity
   */
  protected $entitySnapshot;



  /**
   * @param FormStateInterface $formState
   * @param \Drupal\managed\Core\Relation\Widget $widget
   * @param \Drupal\managed\Entity $entity
   * @param int $index
   */
  public function __construct(FormStateInterface $formState, Widget $widget, Entity $entity, $index) {
    parent::__construct($formState, $entity, array_merge($widget->getParents(), array($index)));

    $this->widget = $widget;
    $this->index  = $index;
    $this->id     = $widget->getID() . '--' . $index;
  }


  /**
   * @return string[]
   */
  public function __sleep() {
    return array_merge(
      parent::__sleep(),
      array('index', 'id', 'entitySnapshot', 'isDeleted', 'isNewlyCreated')
    );
  }


  public function build(FormStateInterface $formState) {
    $columns = array();
    foreach ($this->widget->getColumns() as $column) {
      $columns[$column->getName()] = $column->getContent($this->entity);
    }

    $data = array(
      '#id'         => $this->id,
      '#isExpanded' => $this->isFormVisible,
      '#changed'    => $this->hasFormVisibilityChanged,
      '#columns'    => $columns
    );

    $data += $this->buildActions();

    if ($this->widget->getSortField()) {
      $data['#weight'] = $this->getWeight($formState);
      $data['weight']  = $this->buildWeight($formState);
    }

    if ($this->isFormVisible) {
      $data['form'] = $this->buildForm($formState);
    }

    if ($this->isActionTarget) {
      $data['messages'] = array(
        '#type'   => 'status_messages',
        '#weight' => -1000
      );
    }

    return $data;
  }


  /**
   * Build the form array of the entity form.
   *
   * @param FormStateInterface $formState
   * @return array
   */
  protected function buildForm(FormStateInterface $formState) {
    $childForm = parent::buildForm($formState);
    $this->widget->getCollection()->hideEnforcedFormItems($childForm);

    $sortField = $this->widget->getSortField();
    if ($sortField && isset($childForm[$sortField])) {
      $childForm[$sortField]['#access'] = FALSE;
    }

    return $childForm;
  }


  private function buildWeight(FormStateInterface $formState) {
    return array(
      '#type'          => 'textfield',
      '#title'         => t('Weight'),
      '#title_display' => 'invisible',
      '#default_value' => $this->getWeight($formState),
      '#size'          => 3,
      '#attributes'    => array(
        'class' => array($this->widget->getID() . '--weight')
      )
    );
  }


  private function buildActions() {
    if ($this->isFormVisible) {
      return array(
        'save'   => $this->buildActionButton(t('Apply'),  'apply',    array($this->parents)),
        'cancel' => $this->buildActionButton(t('Cancel'), 'collapse', array()),
      );
    } else {
      return array(
        'edit'   => $this->buildActionButton(t('Edit'),   'expand', array()),
        'delete' => $this->buildActionButton(t('Delete'), 'delete', array()),
      );
    }
  }


  /**
   * @param string $label
   * @param string $action
   * @param array|null $limitValidation
   * @return array
   */
  public function buildActionButton($label, $action, $limitValidation = NULL) {
    $button = $this->widget->buildActionButton($label, $action, $limitValidation);
    $id   = $this->getID();

    $button['#name']   = implode('-', array($id, $action));
    $button['#op_row'] = $id;

    return $button;
  }


  public function storeEntitySnapshot() {
    $this->entitySnapshot = clone $this->entity;
    return $this;
  }


  public function restoreEntitySnapshot() {
    if (isset($this->entitySnapshot)) {
      $this->entity = $this->entitySnapshot;
      unset($this->entitySnapshot);
    }

    return $this;
  }


  public function clearEntitySnapshot() {
    if (isset($this->entitySnapshot)) {
      unset($this->entitySnapshot);
    }

    return $this;
  }


  public function isDeleted() {
    return $this->isDeleted;
  }


  /**
   * @return boolean
   */
  public function isIsNewlyCreated() {
    return $this->isNewlyCreated;
  }


  public function getIndex() {
    return $this->index;
  }


  public function getID() {
    return $this->id;
  }


  public function getWidget() {
    return $this->widget;
  }


  public function setWidget(Widget $widget) {
    $this->widget = $widget;
  }


  private function getWeight(FormStateInterface $formState) {
    $sortField = $this->widget->getSortField();
    $keyExists = NULL;

    $weight = NestedArray::getValue(
      $formState->getUserInput(),
      array_merge($this->parents, array('weight')),
      $keyExists
    );

    if ($keyExists === FALSE) {
      return $this->entity->$sortField->value;
    } else {
      return intval($weight);
    }
  }


  public function setDeleted($value = TRUE) {
    $this->isDeleted = $value;
  }


  public function setNewlyCreated($value = TRUE) {
    $this->isNewlyCreated = $value;
  }


  public function setActionTarget($value = TRUE, array &$form) {
    $this->isActionTarget = $value;

    if ($value) {
      $element =& NestedArray::getValue($form, $this->parents);
      $element['messages'] = array(
        '#type'   => 'status_messages',
        '#weight' => -1000
      );
    }
  }


  public function onFormValidate(array &$form, FormStateInterface $formState) {
    $element =& NestedArray::getValue($form, $this->parents);
    unset($element['#changed']);
    unset($element['messages']);

    if ($this->widget->getSortField()) {
      $element['#weight'] = $this->getWeight($formState);
    }

    $this->validateForm($form, $formState);
  }


  public function onFormSubmit(array &$form, FormStateInterface $formState) {
    if ($this->isFormVisible) {
      $this->submitForm($form, $formState);
    }

    if ($this->isDeleted) {
      if (!$this->isNewlyCreated) {
        $this->entity->delete();
      }
    }
    else {
      $sortField = $this->widget->getSortField();
      if ($sortField) {
        $this->entity->$sortField->value = $this->getWeight($formState);
      }

      $this->entity->save();
    }
  }
}
