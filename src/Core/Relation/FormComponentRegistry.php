<?php

namespace Drupal\managed\Core\Relation;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\managed\Core\ManagedEntityType;
use Drupal\managed\Entity;


class FormComponentRegistry
{
  /**
   * @var \Drupal\managed\Core\Relation\FormComponent[]
   */
  private $components = array();

  /**
   *
   */
  const BUILD_INFO_KEY = 'managed_registry';



  /**
   * @param FormComponent $component
   * @return $this
   */
  public function addComponent(FormComponent $component) {
    $guid = $component->getComponentGuid();
    $this->components[$guid] = $component;

    return $this;
  }


  /**
   * @param string $guid
   * @return \Drupal\managed\Core\Relation\FormComponent|null
   */
  public function getComponent($guid) {
    if (!isset($this->components[$guid])) {
      return NULL;
    }

    return $this->components[$guid];
  }


  /**
   * @param string $name
   * @return \Drupal\managed\Core\Relation\Widget|null
   */
  public function getWidget($name) {
    foreach ($this->components as $component) {
      if (!($component instanceof Widget)) {
        continue;
      }

      if ($component->getName() == $name) {
        return $component;
      }
    }

    return NULL;
  }


  /**
   * @param FormStateInterface $formState
   * @return \Drupal\managed\Core\Relation\FormComponentRegistry
   */
  static public function get(FormStateInterface $formState) {
    if ($formState instanceof ChildFormState) {
      $formState = $formState->getRootState();
    }

    $buildInfo = $formState->getBuildInfo();
    if (!isset($buildInfo[self::BUILD_INFO_KEY])) {
      $buildInfo[self::BUILD_INFO_KEY] = new FormComponentRegistry();
      $formState->setBuildInfo($buildInfo);
    }

    return $buildInfo[self::BUILD_INFO_KEY];
  }


  /**
   * @param FormStateInterface $formState
   * @return Widget|null
   */
  static public function getWidgetFromFormState(FormStateInterface $formState) {
    $trigger = $formState->getTriggeringElement();
    if (is_null($trigger) || !isset($trigger['#op_widget'])) {
      return NULL;
    }

    $registry = self::get($formState);
    return $registry->getWidget($trigger['#op_widget']);
  }


  /**
   * Triggered when the user clicks on an ajax action button within a widget.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $formState
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  static public function onWidgetAjaxCallback(&$form, FormStateInterface $formState) {
    $widget = self::getWidgetFromFormState($formState);

    if (is_null($widget)) {
      return NULL;
    } else {
      return $widget->onAjaxCallback($form, $formState);
    }
  }


  /**
   * Implements hook_form_alter().
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $formState
   * @param string $formID
   */
  static public function onFormAlter(&$form, FormStateInterface $formState, $formID) {
    $entityForm = $formState->getFormObject();
    if (!($entityForm instanceof ContentEntityForm)) {
      return;
    }

    $entity = $entityForm->getEntity();
    if (!($entity instanceof Entity)) {
      return;
    }

    $entityType = $entity->getEntityType();
    if (!($entityType instanceof ManagedEntityType)) {
      return;
    }

    $registry = self::get($formState);
    foreach ($entityType->getRelations($entity->bundle()) as $relation) {
      $name   = $relation->getName();
      $widget = $registry->getWidget($name);

      if (is_null($widget)) {
        $widget = new Widget($formState, $entity, $relation, $form['#parents']);
      }

      $widget->onFormAlter($form, $formState, $formID);
    }
  }
}
