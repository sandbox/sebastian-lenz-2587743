<?php

namespace Drupal\managed\Core\Relation\Action;

use Drupal\Core\Form\FormStateInterface;


/**
 * Base class of all actions that are row related.
 */
class ExpandAction extends AbstractRowAction
{
  public function onFormValidate(array &$form, FormStateInterface $formState) {
    $this->row
      ->storeEntitySnapshot()
      ->setFormVisibility(TRUE);
  }
}
