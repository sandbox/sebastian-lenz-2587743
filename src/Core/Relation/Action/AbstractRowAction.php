<?php

namespace Drupal\managed\Core\Relation\Action;

use Drupal\Core\Form\FormStateInterface;
use Drupal\managed\Core\Relation\Widget;


/**
 * Base class of all actions that are row related.
 */
abstract class AbstractRowAction extends AbstractAction
{
  /**
   * The row this action is targeting.
   *
   * @var \Drupal\managed\Core\Relation\WidgetRow
   */
  protected $row;



  /**
   * Create a new AbstractRowAction instance.
   *
   * @param \Drupal\managed\Core\Relation\Widget $widget
   * @param \Drupal\Core\Form\FormStateInterface $formState
   */
  public function __construct(Widget $widget, array &$form, FormStateInterface $formState) {
    parent::__construct($widget);

    $trigger = $formState->getTriggeringElement();
    if (!isset($trigger['#op_row'])) {
      throw new \InvalidArgumentException();
    }

    $this->row = $widget->getRowByID($trigger['#op_row']);
    if (is_null($this->row)) {
      throw new \InvalidArgumentException();
    } else {
      $this->row->setActionTarget(TRUE, $form);
    }
  }
}
