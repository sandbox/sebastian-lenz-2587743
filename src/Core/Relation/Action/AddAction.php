<?php

namespace Drupal\managed\Core\Relation\Action;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;


/**
 * Base class of all actions that are row related.
 */
class AddAction extends AbstractAction
{
  public function onFormValidate(array &$form, FormStateInterface $formState) {
    $collection = $this->widget->getCollection();
    $parents = array_merge_recursive(
      $this->widget->getParents(),
      array('add', 'bundle')
    );

    $keyExists = NULL;
    $bundle = NestedArray::getValue($formState->getValues(), $parents, $keyExists);
    if (!$keyExists) {
      $bundle = FALSE;
    }

    $entity = $collection->create($bundle);
    $row    = $this->widget->createRow($formState, $entity);
    $row->setNewlyCreated(TRUE);
    $row->setFormVisibility(TRUE);
  }
}
