<?php

namespace Drupal\managed\Core\Relation\Action;

use Drupal\Core\Form\FormStateInterface;


/**
 * Base class of all actions that are row related.
 */
class ApplyAction extends AbstractRowAction
{
  public function onFormValidate(array &$form, FormStateInterface $formState) {
    if ($this->row->hasErrors()) {
      return;
    }

    $this->row
      ->submitForm($form, $formState)
      ->clearEntitySnapshot()
      ->setFormVisibility(FALSE)
      ->setNewlyCreated(FALSE);
  }
}
