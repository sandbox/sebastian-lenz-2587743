<?php

namespace Drupal\managed\Core\Relation\Action;

use Drupal\Core\Form\FormStateInterface;
use Drupal\managed\Core\Relation\FormComponentRegistry;
use Drupal\managed\Core\Relation\Widget;


/**
 * Base class of all action definitions.
 */
abstract class AbstractAction
{
  /**
   * The widget this action is targeting.
   *
   * @var \Drupal\managed\Core\Relation\Widget
   */
  protected $widget;

  /**
   * A list of all available actions.
   *
   * @var string[]
   */
  static $AVAILABLE_ACTIONS = array(
    'add'        => 'Drupal\managed\Core\Relation\Action\AddAction',
    'add_bundle' => 'Drupal\managed\Core\Relation\Action\AddBundleAction',
    'delete'     => 'Drupal\managed\Core\Relation\Action\DeleteAction',
    'apply'      => 'Drupal\managed\Core\Relation\Action\ApplyAction',
    'expand'     => 'Drupal\managed\Core\Relation\Action\ExpandAction',
    'collapse'   => 'Drupal\managed\Core\Relation\Action\CollapseAction',
  );



  /**
   * Create a new AbstractAction instance.
   *
   * @param \Drupal\managed\Core\Relation\Widget $widget
   */
  public function __construct(Widget $widget) {
    $this->widget = $widget;
  }


  public function onFormValidate(array &$form, FormStateInterface $formState) { }


  /**
   * Try to create an action instance for the given form state.
   *
   * @param \Drupal\managed\Core\Relation\Widget $widget
   * @param \Drupal\Core\Form\FormStateInterface $formState
   * @return \Drupal\managed\Core\Relation\Action\AbstractAction|null
   */
  public static function tryCreate(Widget $widget, array &$form, FormStateInterface $formState) {
    $targetWidget = FormComponentRegistry::getWidgetFromFormState($formState);
    if ($targetWidget != $widget) {
      return NULL;
    }

    $trigger = $formState->getTriggeringElement();
    if (is_null($trigger) || !isset($trigger['#op'])) {
      return NULL;
    }

    $operation = $trigger['#op'];
    if (!isset(self::$AVAILABLE_ACTIONS[$operation])) {
      return NULL;
    }

    $actionClass = self::$AVAILABLE_ACTIONS[$operation];
    try {
      $action = new $actionClass($widget, $form, $formState);
    } catch (\Exception $e) {
      $action = NULL;
    }

    return $action;
  }
}
