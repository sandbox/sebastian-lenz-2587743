<?php

namespace Drupal\managed\Core\Relation\Action;

use Drupal\Core\Form\FormStateInterface;


/**
 * Base class of all actions that are row related.
 */
class DeleteAction extends AbstractRowAction
{
  public function onFormValidate(array &$form, FormStateInterface $formState) {
    $this->row->setDeleted(TRUE);
  }
}
