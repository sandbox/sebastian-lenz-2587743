<?php

namespace Drupal\managed\Core\Relation\Action;

use Drupal\Core\Form\FormStateInterface;


/**
 * Base class of all actions that are row related.
 */
class CollapseAction extends AbstractRowAction
{
  public function onFormValidate(array &$form, FormStateInterface $formState) {
    if ($this->row->isIsNewlyCreated()) {
      $this->row->setDeleted();
    } else {
      $this->row
        ->restoreEntitySnapshot()
        ->setFormVisibility(FALSE);
    }
  }
}
