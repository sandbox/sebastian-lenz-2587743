<?php

namespace Drupal\managed\Core\Relation\Collection;

use Drupal\managed\Core\ManagedEntityType;
use Drupal\managed\Entity;


/**
 * Base class of all collection classes.
 */
abstract class AbstractCollection implements \IteratorAggregate, \Countable
{
  /**
   * The child entities of this collection.
   *
   * @var \Drupal\managed\Entity[]
   */
  protected $children;

  /**
   * The id of the owner.
   *
   * @var string
   */
  protected $ownerID;

  /**
   * The language of the owner.
   *
   * @var string
   */
  protected $ownerLanguage;

  /**
   * The entity type id of the owner.
   *
   * @var string
   */
  protected $ownerTypeID;

  /**
   * The entity type id of the all children.
   *
   * @var string
   */
  protected $childTypeID;

  /**
   * The name of the field the relations should be sorted on.
   *
   * @var string|null
   */
  protected $childSortField;

  /**
   * The entity type of the owner.
   *
   * @var \Drupal\managed\Core\ManagedEntityType
   */
  private $ownerType;

  /**
   * The entity type of all children.
   *
   * @var \Drupal\managed\Core\ManagedEntityType
   */
  private $childType;



  /**
   * Create a new AbstractCollection instance.
   *
   * @param \Drupal\managed\Entity $owner
   * @param \Drupal\managed\Core\ManagedEntityType $childType
   */
  public function __construct(Entity $owner, ManagedEntityType $childType) {
    $this->ownerID     = $owner->id();
    $this->ownerTypeID = $owner->getEntityTypeId();
    $this->childTypeID = $childType->id();

    if ($owner->isTranslatable()) {
      $language = $owner->language();
      if (!is_null($language)) {
        $this->ownerLanguage = $language->getId();
      }
    }
  }


  /**
   * Return the names of all properties that will be serialized.
   *
   * @return string[]
   */
  public function __sleep() {
    return array('ownerID', 'ownerTypeID', 'ownerLanguage', 'childTypeID', 'childSortField');
  }


  /**
   * Load the child entities of this collection.
   *
   * @return \Drupal\managed\Entity[]
   */
  abstract protected function load();


  /**
   * Create a child entity instance.
   *
   * @param bool|FALSE $bundle
   * @return \Drupal\managed\Entity
   */
  abstract public function create($bundle = FALSE);


  /**
   * Return the number of children in this collection.
   *
   * \Countable implementation.
   *
   * @return int
   */
  public function count() {
    if (!isset($this->children)) {
      $this->children = $this->load();
    }

    return count($this->children);
  }


  /**
   * Allow this collection to hide enforced form items within
   * the given child form build array.
   *
   * @param array $form
   */
  public function hideEnforcedFormItems(&$form) { }


  /**
   * Return the name of the field the relations should be sorted on.
   *
   * @return string|null
   */
  public function getChildSortField() {
    return $this->childSortField;
  }


  /**
   * Return an iterator over all children.
   *
   * \IteratorAggregate implementation.
   *
   * @return \ArrayIterator
   */
  public function getIterator() {
    if (!isset($this->children)) {
      $this->children = $this->load();
    }

    return new \ArrayIterator($this->children);
  }


  /**
   * Return the entity type id of the owner.
   *
   * @return string
   */
  public function getOwnerTypeID() {
    return $this->ownerTypeID;
  }


  /**
   * Return the entity type definition of the owner.
   *
   * @return \Drupal\managed\Core\ManagedEntityType
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Exception
   */
  public function getOwnerType() {
    if (!isset($this->ownerType)) {
      $ownerType = $this->getEntityManager()->getDefinition($this->ownerTypeID);
      if (!$ownerType instanceof ManagedEntityType) {
        throw new \Exception('Collections can only be owned by managed entities.');
      }

      $this->ownerType = $ownerType;
    }

    return $this->ownerType;
  }


  /**
   * Return the entity type id of the child type.
   *
   * @return string
   */
  public function getChildTypeID() {
    return $this->childTypeID;
  }


  /**
   * Return the entity type definition of all children.
   *
   * @return \Drupal\managed\Core\ManagedEntityType
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Exception
   */
  public function getChildType() {
    if (!isset($this->childType)) {
      $childType = $this->getEntityManager()->getDefinition($this->childTypeID);
      if (!$childType instanceof ManagedEntityType) {
        throw new \Exception('Collections can only contain managed entities.');
      }

      $this->childType = $childType;
    }

    return $this->childType;
  }


  /**
   * Return the storage of the child entity type.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   */
  public function getChildStorage() {
    return $this->getEntityManager()->getStorage($this->childTypeID);
  }


  /**
   * Return the entity manager.
   *
   * @return \Drupal\Core\Entity\EntityManager
   */
  private function getEntityManager() {
    return \Drupal::service('entity.manager');
  }
}
