<?php

namespace Drupal\managed\Core\Relation\Collection;

use Drupal\managed\Annotation\Relation\OneToManyRelationAnnotation;
use Drupal\managed\Entity;


class OneToManyCollection extends AbstractCollection
{
  private $childField;



  public function __construct(Entity $owner, OneToManyRelationAnnotation $annotation) {
    parent::__construct($owner, $annotation->getEntityType());

    $this->childSortField = $annotation->getSort();
    $this->childField     = $annotation->getField($owner);
  }


  public function __sleep() {
    return array_merge(
      parent::__sleep(),
      array('childField', 'ownerID')
    );
  }


  /**
   * Allow this collection to hide enforced form items within
   * the given child form build array.
   *
   * @param array $form
   */
  public function hideEnforcedFormItems(&$form) {
    if (isset($form[$this->childField])) {
      $form[$this->childField]['#access'] = FALSE;
    }
  }


  /**
   * Create a child entity instance.
   *
   * @param bool|FALSE $bundle
   * @return \Drupal\managed\Entity
   * @throws \Exception
   */
  public function create($bundle = FALSE) {
    $childType = $this->getChildType();
    $values = array(
      $this->childField => $this->ownerID
    );

    if ($childType->hasBundles()) {
      if ($bundle === FALSE) {
        throw new \Exception(
          'The entity `@entity` requires a bundle id.',
          array('@entity' => $childType->id())
        );
      }

      $values[$childType->getKey('bundle')] = $bundle;
    }

    return $this->getChildStorage()->create($values);
  }


  protected function load() {
    $storage = $this->getChildStorage();
    $query = $storage->getQuery()
      ->condition($this->childField, $this->ownerID);

    if (!is_null($this->childSortField)) {
      $query->sort($this->childSortField);
    }

    return $storage->loadMultiple($query->execute());
  }
}
