<?php

namespace Drupal\managed\Core\Relation;

use Drupal\Core\Form\FormStateInterface;


class FormComponent
{
  /**
   * @var string
   */
  protected $guid;



  /**
   * @param FormStateInterface $formState
   */
  public function __construct(FormStateInterface $formState) {
    $this->guid = uniqid();

    $registry = FormComponentRegistry::get($formState);
    $registry->addComponent($this);
  }


  /**
   * @return string[]
   */
  public function __sleep() {
    return array('guid');
  }


  /**
   * @return string
   */
  public function getComponentGuid() {
    return $this->guid;
  }
}
