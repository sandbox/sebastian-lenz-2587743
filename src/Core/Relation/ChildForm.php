<?php

namespace Drupal\managed\Core\Relation;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\managed\Core\Relation\Delegate\AbstractDelegate;
use Drupal\managed\Entity;


class ChildForm extends FormComponent
{
  /**
   * @var string[]
   */
  protected $parents;

  /**
   * @var \Drupal\managed\Entity
   */
  protected $entity;

  /**
   * @var \Drupal\managed\Core\Relation\ChildFormState
   */
  protected $formState;

  /**
   * @var array
   */
  protected $formStateCache;

  /**
   * @var \Drupal\Core\Entity\ContentEntityForm
   */
  protected $formObject;

  /**
   * @var bool
   */
  protected $isFormVisible = FALSE;

  /**
   * @var bool
   */
  protected $hasFormVisibilityChanged = FALSE;


  /**
   * @param FormStateInterface $formState
   * @param \Drupal\managed\Entity $entity
   * @param string[] $parents
   */
  public function __construct(FormStateInterface $formState, Entity $entity, $parents) {
    parent::__construct($formState);

    $this->entity  = $entity;
    $this->parents = $parents;
  }


  /**
   * @return string[]
   */
  public function __sleep() {
    $fields = array_merge(
      parent::__sleep(),
      array('entity', 'parents', 'isFormVisible')
    );

    if ($this->isFormVisible) {
      if (isset($this->formState)) {
        $this->formStateCache = $this->formState->getCacheableArray();
      }

      $fields[] = 'formObject';
      $fields[] = 'formStateCache';
    }

    return $fields;
  }


  private function formAction(array &$form, FormStateInterface $formState, $action) {
    if (!$this->isFormVisible) {
      return FALSE;
    }

    $childForm = &NestedArray::getValue($form, $this->getFormPath());
    if (is_null($childForm)) {
      return FALSE;
    }

    $childFormState = $this->getFormState($formState);
    $childFormObject = $this->getFormObject();
    return $childFormObject->$action($childForm, $childFormState);
  }


  public function submitForm(array &$form, FormStateInterface $formState) {
    $result = $this->formAction($form, $formState, 'submitForm');
    if ($result === FALSE) {
      return $this;
    }

    $childFormObject = $this->getFormObject();
    $this->entity = $childFormObject->getEntity();

    return $this;
  }


  public function validateForm(array &$form, FormStateInterface $formState) {
    $this->formAction($form, $formState, 'validateForm');
    return $this;
  }


  /**
   * Build the form array of the entity form.
   *
   * @param FormStateInterface $formState
   * @return array
   */
  protected function buildForm(FormStateInterface $formState) {
    $childFormObject = $this->getFormObject();
    if (is_null($childFormObject)) {
      return array();
    }

    $childFormState = $this->getFormState($formState);
    $childForm = array('#parents' => $this->getFormPath());
    $childForm = $childFormObject->buildForm($childForm, $childFormState);
    $childForm['#managed_child_form'] = TRUE;

    unset($childForm['actions']);
    unset($childForm['#parents']);

    $childFormID = $childFormObject->getFormId();
    $hooks = array('form', 'form_' . $childFormID);

    $this->getModuleHandler()->alter($hooks, $childForm, $childFormState, $childFormID);
    $this->getThemeManager()->alter($hooks, $childForm, $childFormState, $childFormID);

    AbstractDelegate::apply($this->getComponentGuid(), $childForm);

    return $childForm;
  }


  /**
   * @param bool $visible
   * @return $this
   */
  public function setFormVisibility($visible = TRUE) {
    if ($this->isFormVisible == $visible) {
      return $this;
    }

    if (!$visible) {
      unset($this->formObject);
      unset($this->formState);
    }

    $this->hasFormVisibilityChanged = true;
    $this->isFormVisible = $visible;
    return $this;
  }


  /**
   * @return bool
   */
  public function isFormVisible() {
    return $this->isFormVisible;
  }


  /**
   * @return bool
   */
  public function hasErrors() {
    if (!$this->isFormVisible) {
      return FALSE;
    }

    if (!isset($this->formState)) {
      return FALSE;
    }

    return count($this->formState->getErrors()) > 0;
  }


  /**
   * Return the form object of the entity.
   *
   * @return \Drupal\Core\Entity\ContentEntityForm
   * @throws \Exception
   */
  protected function getFormObject() {
    if (!isset($this->formObject)) {
      $type  = $this->entity->getEntityType();
      $class = $type->getFormClass('default');
      $form  = $this
        ->getClassResolver()
        ->getInstanceFromDefinition($class);

      if (!($form instanceof ContentEntityForm)) {
        throw new \Exception(t(
          'Form `default` of entity `@entity` must be a subclass of ContentEntityForm to be used as a child form.',
          array('@entity' => $this->entity->getEntityTypeId())
        ));
      }

      $this->formObject = $form
        ->setEntity($this->entity)
        ->setModuleHandler($this->getModuleHandler());
    }

    return $this->formObject;
  }


  /**
   * @param \Drupal\Core\Form\FormStateInterface $formState
   * @return \Drupal\managed\Core\Relation\ChildFormState|null
   */
  public function getFormState(FormStateInterface $formState) {
    if (!$this->isFormVisible) {
      return NULL;
    }

    if (!isset($this->formState)) {
      $state = new ChildFormState($formState);

      if (isset($this->formStateCache)) {
        $state->setFormState($this->formStateCache);
        unset($this->formStateCache);
      } else {
        $state->setFormObject($this->getFormObject());
      }

      $this->formState = $state;
    }

    return $this->formState;
  }


  /**
   * @return string[]
   */
  protected function getFormPath() {
    return array_merge($this->parents, array('form'));
  }


  /**
   * @return \Drupal\Core\Theme\ThemeManager
   */
  protected function getThemeManager() {
    return \Drupal::service('theme.manager');
  }


  /**
   * @return \Drupal\Core\DependencyInjection\ClassResolver
   */
  protected function getClassResolver() {
    return \Drupal::service('class_resolver');
  }


  /**
   * @return \Drupal\Core\Extension\ModuleHandler
   */
  protected function getModuleHandler() {
    return \Drupal::service('module_handler');
  }
}
