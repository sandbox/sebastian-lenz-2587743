<?php

namespace Drupal\managed\Core\Relation;

use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;


class ChildFormState extends FormState
{
  /**
   * @var \Drupal\Core\Form\FormStateInterface
   */
  private $parentState;

  /**
   * @var \Drupal\Core\Form\FormStateInterface
   */
  private $rootState;



  /**
   * @param \Drupal\Core\Form\FormStateInterface $parentState
   */
  public function __construct(FormStateInterface $parentState) {
    $this->parentState = $parentState;
  }


  /**
   * @return \Drupal\Core\Form\FormStateInterface
   */
  public function getParentState() {
    return $this->parentState;
  }


  /**
   * @return \Drupal\Core\Form\FormStateInterface
   */
  public function getRootState() {
    if (!isset($this->rootState)) {
      $root = $this;

      while ($root instanceof ChildFormState) {
        $root = $root->getParentState();
      }

      $this->rootState = $root;
    }

    return $this->rootState;
  }


  /**
   * Returns a reference to the complete form array.
   *
   * @return array
   *   The complete form array.
   */
  public function &getCompleteForm() {
    return $this->getRootState()->getCompleteForm();
  }


  /**
   * Gets the form element that triggered submission.
   *
   * @return array|null
   *   The form element that triggered submission, of NULL if there is none.
   */
  public function &getTriggeringElement() {
    return $this->getRootState()->getTriggeringElement();
  }


  /**
   * {@inheritdoc}
   */
  public function setRebuild($rebuild = TRUE) {
    $this->getRootState()->setRebuild($rebuild);
    return $this;
  }


  /**
   * {@inheritdoc}
   */
  public function isRebuilding() {
    return $this->getRootState()->isRebuilding();
  }


  /**
   * {@inheritdoc}
   */
  public function isValidationComplete() {
    return $this->getRootState()->isValidationComplete();
  }


  /**
   * {@inheritdoc}
   */
  public function setLimitValidationErrors($limit_validation_errors) {
    $this->getRootState()->setLimitValidationErrors($limit_validation_errors);
    return $this;
  }


  /**
   * {@inheritdoc}
   */
  public function getLimitValidationErrors() {
    return $this->getRootState()->getLimitValidationErrors();
  }


  /**
   * {@inheritdoc}
   */
  public function setErrorByName($name, $message = '') {
    $this->getRootState()->setErrorByName($name, $message);
  }


  /**
   * {@inheritdoc}
   */
  public function clearErrors() {
    $this->getRootState()->clearErrors();
  }


  /**
   * {@inheritdoc}
   */
  public function getErrors() {
    return $this->getRootState()->getErrors();
  }


  /**
   * {@inheritdoc}
   */
  public function &getValues() {
    return $this->getRootState()->getValues();
  }


  /**
   * {@inheritdoc}
   */
  public function setValues(array $values) {
    $this->getRootState()->setValues($values);
    return $this;
  }


  /**
   * {@inheritdoc}
   */
  public function &getUserInput() {
    return $this->getRootState()->getUserInput();
  }


  /**
   * {@inheritdoc}
   */
  public function setUserInput(array $user_input) {
    $this->getRootState()->setUserInput($user_input);
    return $this;
  }
}
