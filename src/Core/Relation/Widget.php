<?php

namespace Drupal\managed\Core\Relation;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InsertCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\managed\Annotation\Field\WeightFieldAnnotation;
use Drupal\managed\Annotation\Relation\AbstractRelationAnnotation;
use Drupal\managed\Behaviour\ListBuilder\Column\AbstractColumn;
use Drupal\managed\Behaviour\ListBuilder\ColumnHostInterface;
use Drupal\managed\Core\Relation\Action\AbstractAction;
use Drupal\managed\Core\Relation\Collection\AbstractCollection;
use Drupal\managed\Core\Relation\Delegate\WidgetSubmitDelegate;
use Drupal\managed\Core\Relation\Delegate\WidgetValidateDelegate;
use Drupal\managed\Entity;


/**
 * Display an inline relation editor.
 */
class Widget extends FormComponent implements ColumnHostInterface
{
  /**
   * The unique id of this widget.
   *
   * @var string
   */
  private $id;

  /**
   * The form parents of this widget.
   *
   * @var string[]
   */
  private $parents;

  /**
   * The name of the field this widget represents.
   *
   * @var string
   */
  private $name;

  /**
   * The name of the field the table should be sorted on.
   *
   * When set the table drag behaviour should be activated.
   *
   * @var string
   */
  private $sortField;

  /**
   * The underlying relation collection edited by this widget.
   *
   * @var \Drupal\managed\Core\Relation\Collection\AbstractCollection
   */
  private $collection;

  /**
   * @var \Drupal\managed\Behaviour\ListBuilder\Column\AbstractColumn[]
   */
  private $columns;

  /**
   * The displayed rows of this widget.
   *
   * @var \Drupal\managed\Core\Relation\WidgetRow[]
   */
  private $rows;

  /**
   * The action that is currently performed.
   *
   * @var \Drupal\managed\Core\Relation\Action\AbstractAction
   */
  private $action;



  /**
   * Create a new RelationWidget instance.
   *
   * @param FormStateInterface $formState
   * @param \Drupal\managed\Entity $entity
   * @param \Drupal\managed\Annotation\Relation\AbstractRelationAnnotation $relation
   * @param string[] $parents
   * @throws \Exception
   */
  public function __construct(FormStateInterface $formState, Entity $entity, AbstractRelationAnnotation $relation, $parents) {
    parent::__construct($formState);

    $name = $relation->getName();

    $this->name    = $name;
    $this->parents = array_merge($parents, array($name));

    $this->fetchCollection($entity, $name);
    $this->createID($entity);
    $this->createRows($formState);

    $this->columns = AbstractColumn::buildColumns($this, $relation->getColumnDefinitions());
  }


  public function __sleep() {
    return array_merge(
      parent::__sleep(),
      array('id', 'name', 'parents', 'sortField', 'collection', 'columns', 'rows')
    );
  }


  public function __wakeup() {
    foreach ($this->rows as $row) {
      $row->setWidget($this);
    }
  }


  /**
   * @param \Drupal\managed\Entity $entity
   * @param string $name
   * @return \Drupal\managed\Core\Relation\Collection\AbstractCollection
   * @throws \Exception
   */
  private function fetchCollection(Entity $entity, $name) {
    $collection = $entity->{$name};
    if (!($collection instanceof AbstractCollection)) {
      throw new \Exception(t(
        'The collection `@name` does not exist on entity type `@entity`.',
        array(
          '@name'   => $name,
          '@entity' => $entity->getEntityTypeId()
        )
      ));
    }

    $this->collection = $collection;

    $sortField = $collection->getChildSortField();
    if (!is_null($sortField) && $this->isWeightField($sortField)) {
      $this->sortField = $sortField;
    }
  }


  private function createID(Entity $entity) {
    $id = implode('-', array(
      $entity->getEntityTypeId(),
      $entity->id(),
      $this->name
    ));

    $this->id = 'relations--' . $id;
  }


  private function createRows($formState) {
    $rows = array();
    foreach ($this->collection as $child) {
      $rows[] = new WidgetRow($formState, $this, $child, count($rows));
    }

    $this->rows = $rows;
  }


  /**
   * @param FormStateInterface $formState
   * @param \Drupal\managed\Entity $entity
   * @return WidgetRow
   */
  public function createRow(FormStateInterface $formState, Entity $entity) {
    $row = new WidgetRow($formState, $this, $entity, count($this->rows));
    $this->rows[] = $row;
    return $row;
  }


  protected function tryCreateAction(array &$form, FormStateInterface $formState) {
    if (!isset($this->action)) {
      $this->action = AbstractAction::tryCreate($this, $form, $formState);
    }

    return !is_null($this->action);
  }


  private function build(FormStateInterface $formState) {
    $columns = array();
    foreach ($this->columns as $column) {
      $columns[$column->getName()] = $column->getLabel();
    }

    $element = array(
      '#theme'   => 'managed_relation_table',
      '#tree'    => TRUE,
      '#id'      => $this->id,
      '#drag'    => isset($this->sortField),
      '#columns' => $columns,
      'add'      => $this->buildAdd()
    );

    foreach ($this->rows as $row) {
      if ($row->isDeleted()) {
        continue;
      }

      $element[$row->getIndex()] = $row->build($formState);
    }

    return $element;
  }


  private function buildAdd() {
    $result = array();

    $childType = $this->collection->getChildType();
    if ($childType->hasBundles()) {
      $options = array();
      foreach ($childType->getBundles() as $bundle) {
        $options[$bundle->id()] = $bundle->getLabel();
      }

      $result['bundle'] = array(
        '#type'    => 'select',
        '#options' => $options
      );
    }

    $result['command'] = $this->buildActionButton(t('Add'), 'add', array());
    return $result;
  }


  /**
   * @param string $label
   * @param string $action
   * @param array|null $limitValidation
   * @return array
   */
  public function buildActionButton($label, $action, $limitValidation = NULL) {
    $id = $this->getID();

    $button = array(
      '#type'      => 'button',
      '#value'     => $label,
      '#name'      => implode('-', array($id, $action)),
      '#op'        => $action,
      '#op_widget' => $this->getName(),
      '#ajax'      => array(
        'wrapper'    => $id,
        'effect'     => 'fade',
        'callback'   => '\Drupal\managed\Core\Relation\FormComponentRegistry::onWidgetAjaxCallback',
      )
    );

    if (!is_null($limitValidation)) {
      $button['#limit_validation_errors'] = $limitValidation;
    }

    return $button;
  }


  /**
   * Test whether the given field is a WeightField.
   *
   * @param string $fieldName
   * @return bool
   */
  public function isWeightField($fieldName) {
    $entityType = $this->collection->getChildType();
    $field      = $entityType->getField($fieldName);

    if (is_null($field)) {
      return FALSE;
    } else {
      return $field instanceof WeightFieldAnnotation;
    }
  }


  /**
   * Return the unique id of this widget.
   *
   * @return string
   */
  public function getID() {
    return $this->id;
  }


  /**
   * Return the path to this widget within the form.
   *
   * @return string
   */
  public function getParents() {
    return $this->parents;
  }


  /**
   * Return the name of the field this widget represents.
   *
   * @return string
   */
  public function getName() {
    return $this->name;
  }


  /**
   * @return \Drupal\managed\Core\Relation\Collection\AbstractCollection
   */
  public function getCollection() {
    return $this->collection;
  }


  public function getSortField() {
    return $this->sortField;
  }


  /**
   * Return the row with the given id.
   *
   * @param string $id
   * @return \Drupal\managed\Core\Relation\WidgetRow|null
   */
  public function getRowByID($id) {
    foreach ($this->rows as $row) {
      if ($row->getID() == $id) {
        return $row;
      }
    }

    return NULL;
  }


  /**
   * @return \Drupal\managed\Behaviour\ListBuilder\Column\AbstractColumn[]
   */
  public function getColumns() {
    return $this->columns;
  }


  /**
   * Return the entity type displayed by this list.
   *
   * @return \Drupal\managed\Core\ManagedEntityType
   */
  public function getEntityType() {
    return $this->collection->getChildType();
  }


  /**
   * Return the field storage definitions of the entity type.
   *
   * @param string $name
   * @return \Drupal\Core\Field\FieldDefinitionInterface
   */
  public function getFieldStorageDefinition($name) {
    $manager      = $this->getEntityManager();
    $entityTypeID = $this->collection->getChildTypeID();
    $definitions  = $manager->getFieldStorageDefinitions($entityTypeID);

    return $definitions[$name];
  }


  /**
   * Return the entity manager.
   *
   * @return \Drupal\Core\Entity\EntityManager
   */
  private function getEntityManager() {
    return \Drupal::service('entity.manager');
  }


  /**
   * @return \Drupal\Core\Render\RendererInterface
   */
  private function getRenderer() {
    return \Drupal::service('renderer');
  }


  /**
   * @param $form
   * @param FormStateInterface $formState
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function onAjaxCallback(&$form, FormStateInterface $formState) {
    // $formState->setRebuild();

    $content  = NestedArray::getValue($form, $this->parents);
    $renderer = $this->getRenderer();
    $html     = $renderer->renderRoot($content);

    $response = new AjaxResponse();
    $response->setAttachments($content['#attached']);
    $response->addCommand(new InsertCommand(NULL, $html));

    return $response;
  }


  /**
   * Implements hook_entity_form_display_alter().
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $formState
   * @param string $formID
   */
  public function onFormAlter(&$form, FormStateInterface $formState, $formID) {
    $form[$this->name] = $this->build($formState);

    if ($formState instanceof ChildFormState) {
      $completeForm =& $formState->getCompleteForm();
    } else {
      $formState->setCompleteForm($form);
      $completeForm =& $form;
    }

    $completeForm['#validate'][] = new WidgetValidateDelegate($this->guid);

    $submit = &NestedArray::getValue($completeForm, array('actions', 'submit'));
    if (!is_null($submit)) {
      $submit['#submit'][] = new WidgetSubmitDelegate($this->guid);;
    }
  }


  public function onFormValidate(array &$form, FormStateInterface $formState) {
    foreach ($this->rows as $row) {
      $row->onFormValidate($form, $formState);
    }

    if ($this->tryCreateAction($form, $formState)) {
      $this->action->onFormValidate($form, $formState);
    }
  }


  public function onFormSubmit(array &$form, FormStateInterface $formState) {
    foreach ($this->rows as $row) {
      $row->onFormSubmit($form, $formState);
    }
  }
}
