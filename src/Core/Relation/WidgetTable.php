<?php

namespace Drupal\managed\Core\Relation;

use Drupal\Core\Render\Element;


class WidgetTable
{
  private $id;

  private $add;

  private $columns;

  private $allowDrag;

  private $children;


  public function __construct(&$variables) {
    $element          = &$variables['element'];
    $this->id         = $element['#id'];
    $this->allowDrag  = $element['#drag'];
    $this->columns    = $element['#columns'];

    if (isset($element['add'])) {
      $this->add = $element['add'];
      unset($element['add']);
    }

    $this->children = array();
    foreach (Element::children($element, TRUE) as $childName) {
      $this->children[] = &$element[$childName];
    }
  }


  public function buildTable() {
    $table = array(
      '#prefix' => '<div id="' . $this->id . '">',
      '#suffix' => '</div>',
      '#type'   => 'table',
      '#attributes' => array('id' => $this->id . '-table'),
      '#header' => $this->buildHeader(),
      '#rows'   => $this->buildRows(),
      '#footer' => $this->buildFooter()
    );

    if ($this->allowDrag) {
      $table['#tabledrag'] = array(
        array(
          'action'       => 'order',
          'relationship' => 'sibling',
          'group'        => $this->id . '--weight',
        )
      );
    }

    return render($table);
  }


  private function buildHeader() {
    $header = array();
    if ($this->allowDrag) {
      $header += array(
        'handle' => array(
          'width' => '30',
          'data'  => ''
        ),
        'weight'  => array(
          'data'  => t('Weight')
        )
      );
    }

    $header += $this->columns;

    $header += array(
      'actions' => array(
        'width' => '300',
        'data'  => t('Operations')
      )
    );

    return $header;
  }


  private function buildRows() {
    $rows = array();

    foreach ($this->children as &$child) {
      $cells = array();
      $row = array();

      if ($this->allowDrag) {
        $row['class'] = array('draggable', 'tabledrag-leaf');

        $cells['handle'] = array(
          'data' => ''
        );

        $cells['weight'] = array(
          'data' => $child['weight']
        );
      }

      if ($child['#changed']) {
        $wrapper = array(
          '#prefix'  => '<div class="ajax-new-content">',
          '#suffix'  => '</div>'
        );
      } else {
        $wrapper = array();
      }

      $actions = $this->buildRowActions($child);
      if ($child['#isExpanded']) {
        $cells['form'] = array(
          'colspan' => count($this->columns) + 1,
          'data'    => array(
            'messages' => $child['messages'],
            'form'     => $child['form'],
            'actions'  => $actions
          ) + $wrapper
        );
      } else {
        foreach ($child['#columns'] as $key => $value) {
          $value = is_array($value) ? $value : array('#markup' => $value);
          $cells[$key] = array(
            'data' => $value + $wrapper
          );
        }

        $cells += array(
          'actions' => array(
            'data'  => $actions
          )
        );
      }

      $row['data'] = $cells;
      $rows[] = $row;
    }

    return $rows;
  }


  private function buildRowActions(&$child) {
    $actions = array(
      '#weight' => 100,
      '#prefix' => '<div class="form-actions">',
      '#suffix' => '</div>',
    );

    foreach (array('edit', 'delete', 'save', 'cancel') as $operation) {
      if (!isset($child[$operation])) continue;
      $actions[$operation] = array('#markup' => render($child[$operation]));
    }

    return $actions;
  }


  private function buildFooter() {
    $footer = array();

    $footer[] = array(
      'add' => array(
        'colspan' => 2,
        'data'    => $this->add
      )
    );

    return $footer;
  }


  static function build(&$variables) {
    $table = new WidgetTable($variables);
    return $table->buildTable();
  }
}
