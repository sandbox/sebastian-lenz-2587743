<?php

namespace Drupal\managed\Core\Relation\Delegate;


class ElementValidateDelegate extends AbstractDelegate
{
  public function __invoke(&$element, &$formState, &$completeForm) {
    $childFormState = $this->getChildFormState($formState);
    $delegate       = $childFormState->prepareCallback($this->delegate);

    return call_user_func_array($delegate, array(&$element, &$childFormState, &$completeForm));
  }


  public static function apply($guid, &$element) {
    if (!isset($element['#element_validate'])) {
      return;
    }

    foreach ($element['#element_validate'] as $key => $handler) {
      if ($handler instanceof ElementValidateDelegate) {
        $handler->guid = $guid;
      } else {
        $element['#element_validate'][$key] = new ElementValidateDelegate($guid, $handler);
      }
    }
  }
}
