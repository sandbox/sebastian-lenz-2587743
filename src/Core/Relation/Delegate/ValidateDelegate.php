<?php

namespace Drupal\managed\Core\Relation\Delegate;


class ValidateDelegate extends AbstractDelegate
{
  public function __invoke(&$form, &$formState) {
    $childFormState = $this->getChildFormState($formState);
    $delegate       = $childFormState->prepareCallback($this->delegate);

    return call_user_func_array($delegate, array(&$form, &$childFormState));
  }


  public static function apply($guid, &$element) {
    if (!isset($element['#validate'])) {
      return;
    }

    foreach ($element['#validate'] as $key => $handler) {
      if ($handler instanceof ValidateDelegate) {
        $handler->guid = $guid;
      } else {
        $element['#validate'][$key] = new ValidateDelegate($guid, $handler);
      }
    }
  }
}
