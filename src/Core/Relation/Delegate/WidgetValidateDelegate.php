<?php

namespace Drupal\managed\Core\Relation\Delegate;

use Drupal\managed\Core\Relation\FormComponentRegistry;
use Drupal\managed\Core\Relation\Widget;


class WidgetValidateDelegate extends ValidateDelegate
{
  /**
   * @param string $guid
   */
  public function __construct($guid) {
    parent::__construct($guid, NULL);
  }


  public function __invoke(&$form, &$formState) {
    $registry = FormComponentRegistry::get($formState);
    $widget = $registry->getComponent($this->guid);

    if (is_null($widget) || !($widget instanceof Widget)) {
      throw new \Exception('Invalid guid given.');
    }

    return $widget->onFormValidate($form, $formState);
  }
}
