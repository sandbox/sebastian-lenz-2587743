<?php

namespace Drupal\managed\Core\Relation\Delegate;


class SubmitDelegate extends AbstractDelegate
{
  public function __invoke(&$form, &$formState) {
    $childFormState = $this->getChildFormState($formState);
    $delegate       = $childFormState->prepareCallback($this->delegate);

    return call_user_func_array($delegate, array(&$form, &$childFormState));
  }


  public static function apply($guid, &$element) {
    if (!isset($element['#submit'])) {
      return;
    }

    foreach ($element['#submit'] as $key => $handler) {
      if ($handler instanceof SubmitDelegate) {
        $handler->guid = $guid;
      } else {
        $element['#submit'][$key] = new SubmitDelegate($guid, $handler);
      }
    }
  }
}
