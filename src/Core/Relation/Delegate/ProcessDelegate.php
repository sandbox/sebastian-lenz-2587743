<?php

namespace Drupal\managed\Core\Relation\Delegate;


class ProcessDelegate extends AbstractDelegate
{
  public function __invoke(&$element, &$formState, &$completeForm) {
    $childFormState = $this->getChildFormState($formState);
    if (is_null($childFormState)) {
      return array();
    }

    $delegate = $childFormState->prepareCallback($this->delegate);

    $result = call_user_func_array($delegate, array(&$element, &$childFormState, &$completeForm));
    AbstractDelegate::apply($this->guid, $result);
    return $result;
  }


  public static function apply($guid, &$element) {
    if (!isset($element['#process'])) {
      return;
    }

    foreach ($element['#process'] as $key => $handler) {
      if ($handler instanceof ProcessDelegate) {
        $handler->guid = $guid;
      } else {
        $element['#process'][$key] = new ProcessDelegate($guid, $handler);
      }
    }
  }
}
