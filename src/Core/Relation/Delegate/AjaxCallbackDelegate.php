<?php

namespace Drupal\managed\Core\Relation\Delegate;


class AjaxCallbackDelegate extends AbstractDelegate
{
  public function __invoke(&$element, &$formState, $request) {
    $childFormState = $this->getChildFormState($formState);
    $delegate       = $childFormState->prepareCallback($this->delegate);

    return call_user_func_array($delegate, array(&$element, &$childFormState, $request));
  }


  public static function apply($guid, &$element) {
    if (!isset($element['#ajax']) || !isset($element['#ajax']['callback'])) {
      return;
    }

    if ($element['#ajax']['callback'] instanceof AjaxCallbackDelegate) {
      $element['#ajax']['callback']->guid = $guid;
    } else {
      $element['#ajax']['callback'] = new AjaxCallbackDelegate($guid, $element['#ajax']['callback']);
    }
  }
}
