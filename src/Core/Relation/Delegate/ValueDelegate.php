<?php

namespace Drupal\managed\Core\Relation\Delegate;


class ValueDelegate extends AbstractDelegate
{
  public function __invoke(&$element, $input, &$formState) {
    $childFormState = $this->getChildFormState($formState);
    return call_user_func_array($this->delegate, array(&$element, $input, &$childFormState));
  }


  public static function apply($guid, &$element) {
    if (!isset($element['#value_callback']) && (!isset($element['#input']) || !$element['#input'])) {
      return;
    }

    $handler = !empty($element['#value_callback']) ? $element['#value_callback'] : 'form_type_' . $element['#type'] . '_value';
    if (!is_callable($handler)) {
      $handler = '\Drupal\Core\Render\Element\FormElement::valueCallback';
    }

    if ($handler instanceof ValueDelegate) {
      $handler->guid = $guid;
    } else {
      $element['#value_callback'] = new ValueDelegate($guid, $handler);
    }
  }
}
