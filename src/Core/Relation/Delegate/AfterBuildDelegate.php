<?php

namespace Drupal\managed\Core\Relation\Delegate;


class AfterBuildDelegate extends AbstractDelegate
{
  public function __invoke(&$element, &$formState) {
    $childFormState = $this->getChildFormState($formState);
    $delegate       = $childFormState->prepareCallback($this->delegate);

    return call_user_func_array($delegate, array(&$element, &$childFormState));
  }


  public static function apply($guid, &$element) {
    if (!isset($element['#after_build'])) {
      return;
    }

    foreach ($element['#after_build'] as $key => $handler) {
      if ($handler instanceof AfterBuildDelegate) {
        $handler->guid = $guid;
      } else {
        $element['#after_build'][$key] = new AfterBuildDelegate($guid, $handler);
      }
    }
  }
}
