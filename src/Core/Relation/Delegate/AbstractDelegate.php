<?php

namespace Drupal\managed\Core\Relation\Delegate;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\managed\Core\Relation\ChildForm;
use Drupal\managed\Core\Relation\FormComponentRegistry;


abstract class AbstractDelegate
{
  /**
   * @var string
   */
  public $guid;

  /**
   * @var callable
   */
  public $delegate;

  /**
   * @var string[]
   */
  public static $DELEGATES = array(
    '\Drupal\managed\Core\Relation\Delegate\AfterBuildDelegate',
    '\Drupal\managed\Core\Relation\Delegate\AjaxCallbackDelegate',
    '\Drupal\managed\Core\Relation\Delegate\ElementValidateDelegate',
    '\Drupal\managed\Core\Relation\Delegate\ProcessDelegate',
    '\Drupal\managed\Core\Relation\Delegate\SubmitDelegate',
    '\Drupal\managed\Core\Relation\Delegate\ValidateDelegate',
    '\Drupal\managed\Core\Relation\Delegate\ValueDelegate'
  );



  /**
   * @param string $guid
   * @param callable $delegate
   */
  public function __construct($guid, $delegate) {
    $this->guid = $guid;
    $this->delegate = $delegate;
  }


  /**
   * @param \Drupal\Core\Form\FormStateInterface $formState
   * @return \Drupal\managed\Core\Relation\ChildFormState|null
   * @throws \Exception
   */
  protected function getChildFormState(FormStateInterface $formState) {
    $registry = FormComponentRegistry::get($formState);
    $childForm = $registry->getComponent($this->guid);

    if (is_null($childForm) || !($childForm instanceof ChildForm)) {
      throw new \Exception('Invalid guid given.');
    }

    return $childForm->getFormState($formState);
  }


  /**
   * @param string $guid
   * @param array $element
   */
  public static function apply($guid, &$element) {
    foreach (self::$DELEGATES as $delegate) {
      call_user_func_array(array($delegate, 'apply'), array($guid, &$element));
    }

    foreach (Element::children($element) as $key) {
      if ($element[$key]['#managed_child_form']) {
        continue;
      }

      self::apply($guid, $element[$key]);
    }
  }
}
