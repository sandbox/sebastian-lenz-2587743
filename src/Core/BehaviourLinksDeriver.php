<?php

namespace Drupal\managed\Core;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\managed\Core\ManagedEntityTypeManager;
use Symfony\Component\DependencyInjection\ContainerInterface;


class BehaviourLinksDeriver extends DeriverBase implements ContainerDeriverInterface
{
  /**
   * The managed entity manager.
   *
   * @var \Drupal\managed\Core\ManagedEntityTypeManager
   */
  protected $manager;



  /**
   * Create a new LinksDeriver instance.
   *
   * @param \Drupal\managed\Core\ManagedEntityTypeManager $manager
   */
  public function __construct(ManagedEntityTypeManager $manager) {
    $this->manager = $manager;
  }


  /**
   * Creates a new class instance.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param string $basePluginID
   * @return static
   */
  public static function create(ContainerInterface $container, $basePluginID) {
    return new static($container->get('managed.entity_manager'));
  }


  /**
   * Gets the definition of all derivatives of a base plugin.
   *
   * @param array $basePluginDefinition
   *   The definition array of the base plugin.
   * @return array
   *   An array of full derivative definitions keyed on derivative id.
   */
  public function getDerivativeDefinitions($basePluginDefinition) {
    $links = array();

    foreach ($this->manager->getAllManagedTypes() as $entityType) {
      $behaviour = $this->manager->getBehaviour($entityType);
      if (is_null($behaviour)) {
        continue;
      }

      $behaviourLinks = null;
      switch ($basePluginDefinition['id']) {
        case 'managed.action.behaviour':
          $behaviourLinks = $behaviour->getActionLinks();
          break;
        case 'managed.menu.behaviour':
          $behaviourLinks = $behaviour->getMenuLinks();
          break;
        case 'managed.task.behaviour':
          $behaviourLinks = $behaviour->getTaskLinks();
          break;
        case 'managed.contextual.behaviour':
          $behaviourLinks = $behaviour->getContextualLinks();
          break;
      }

      if (is_array($behaviourLinks)) {
        $links += $behaviourLinks;
      }
    }

    return $links;
  }
}
