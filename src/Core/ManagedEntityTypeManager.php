<?php

namespace Drupal\managed\Core;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormState;
use Drupal\managed\Core\Relation\RelationWidget;
use Drupal\managed\Entity;
use Drupal\managed\Utils\ContainerAware;


class ManagedEntityTypeManager extends ContainerAware
{
  /**
   * @return \Drupal\Core\Entity\EntityManager
   */
  public function getEntityManager() {
    return $this->getService('entity.manager');
  }


  /**
   * @return \Drupal\managed\Discovery\Discovery
   */
  public function getDiscovery() {
    return $this->getService('managed.discovery');
  }


  /**
   * @return \Drupal\managed\Core\PersistentData
   */
  public function getPersistentData() {
    return $this->getService('managed.persistent');
  }


  /**
   * Return the entity type with the provided id.
   *
   * @param string $entityTypeID
   * @return ManagedEntityType
   */
  public function getManagedType($entityTypeID) {
    $entityType = $this->getEntityManager()->getDefinition($entityTypeID, false);

    if (!is_null($entityType) && $entityType instanceof ManagedEntityType) {
      return $entityType;
    } else {
      return NULL;
    }
  }


  /**
   * Return a list of all managed entity types.
   *
   * @return ManagedEntityType[]
   */
  public function getAllManagedTypes() {
    $entityTypes = array();

    foreach ($this->getEntityManager()->getDefinitions() as $entityType) {
      if ($entityType instanceof ManagedEntityType) {
        $entityTypes[] = $entityType;
      }
    }

    return $entityTypes;
  }


  /**
   * Return the bundle of the given entity with the provided id.
   *
   * @param string $typeID
   * @param string $bundleID
   * @return \Drupal\managed\Annotation\BundleAnnotation
   */
  public function getBundle($typeID, $bundleID) {
    $type = $this->getManagedType($typeID);

    if (!is_null($type)) {
      return $type->getBundle($bundleID);
    } else {
      return NULL;
    }
  }


  /**
   * Return the instance of the behaviour of the given entity.
   *
   * @param \Drupal\managed\Core\ManagedEntityType $entityType
   * @return \Drupal\managed\Behaviour\AbstractBehaviour
   */
  public function getBehaviour(ManagedEntityType $entityType) {
    $definition = $entityType->getBehaviour();
    if (is_null($definition)) {
      return NULL;
    }

    $container = $this->getContainer();
    $serviceID = 'managed.behaviour.' . $entityType->id();

    if (!$container->has($serviceID)) {
      $class = $definition->getBehaviourClass();
      $container->set($serviceID, new $class($entityType));
    }

    return $container->get($serviceID);
  }


  /**
   * Implements hook_entity_type_build().
   *
   * @param \Drupal\Core\Entity\EntityType[] $entityTypes
   */
  public function onEntityTypeBuild(&$entityTypes) {
    foreach ($this->getDiscovery()->getEntityAnnotations() as $entity) {
      $entityTypes[$entity->id()] = $entity->getEntityType();
    }
  }


  /**
   * Implements hook_entity_bundle_info().
   */
  public function onEntityBundleInfo() {
    $result = array();
    $installedBundles = $this->getPersistentData()->getInstalledBundles();

    foreach ($installedBundles as $installedBundle) {
      $typeID = $installedBundle['entity'];
      if ($typeID == 'node') {
        continue;
      }

      $bundleID = $installedBundle['bundle'];
      $bundle   = $this->getBundle($typeID, $bundleID);

      $result[$typeID][$bundleID] = is_null($bundle)
        ? array('label' => 'Unknwon')
        : $bundle->getInfo();
    }

    return $result;
  }


  /**
   * Implements hook_entity_extra_field_info().
   */
  public function onEntityExtraFieldInfo() {
    $extra = array();

    foreach ($this->getDiscovery()->getEntityAnnotations() as $entityAnnotation) {
      $baseData = $entityAnnotation->getExtraFieldData();
      if ($baseData) {
        $extra[$entityAnnotation->id()][$entityAnnotation->id()] = $baseData;
      }

      $bundleAnnotations = $entityAnnotation->getBundles();
      if (is_array($bundleAnnotations)) {
        foreach ($bundleAnnotations as $bundleAnnotation) {
          $bundleData = $bundleAnnotation->getExtraFieldData($baseData);
          if ($bundleData) {
            $extra[$entityAnnotation->id()][$bundleAnnotation->id()] = $bundleData;
          }
        }
      }
    }

    return $extra;
  }
}
