<?php

namespace Drupal\managed\Core;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\managed\Core\ManagedEntityTypeManager;
use Symfony\Component\DependencyInjection\ContainerInterface;


class BehaviourPermissionProvider implements ContainerInjectionInterface
{
  /**
   * The managed entity manager.
   *
   * @var \Drupal\managed\Core\ManagedEntityTypeManager
   */
  protected $manager;



  /**
   * Create a new PermissionProvider instance.
   *
   * @param \Drupal\managed\Core\ManagedEntityTypeManager $manager
   */
  public function __construct(ManagedEntityTypeManager $manager) {
    $this->manager = $manager;
  }


  /**
   * Creates a new class instance.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('managed.entity_manager'));
  }


  /**
   * Return the permissions defined by all attached behaviours.
   *
   * @return array
   */
  public function onPermissions() {
    $permissions = array();

    foreach ($this->manager->getAllManagedTypes() as $entityType) {
      $behaviour = $this->manager->getBehaviour($entityType);
      if (is_null($behaviour)) {
        continue;
      }

      $permissions += $behaviour->onPermissions();
    }

    return $permissions;
  }
}
