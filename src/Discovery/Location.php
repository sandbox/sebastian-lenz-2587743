<?php

namespace Drupal\managed\Discovery;

use Drupal\Core\Extension\Extension;


/**
 * Defines a location the scanner should scan for annotated classes in.
 */
class Location
{
  /**
   * The base path that should be scanned.
   *
   * @var string
   */
  private $path;

  /**
   * The namespace the classes are located in.
   *
   * @var string
   */
  private $namespace;

  /**
   * The extension that exposes this location.
   *
   * @var \Drupal\Core\Extension\Extension
   */
  private $extension;

  /**
   * Defines a value test against the path property.
   */
  const MATCH_PATH = 1;

  /**
   * Defines a value test against the namespace property.
   */
  const MATCH_NAMESPACE = 2;

  /**
   * Defines a value test against the module name property.
   */
  const MATCH_MODULE = 3;



  /**
   * Create a new Location instance.
   *
   * @param string $path
   *   The base path that should be scanned.
   * @param string $namespace
   *   The namespace the classes are located in.
   * @param \Drupal\Core\Extension\Extension $extension
   *   The extension that exposes this path.
   */
  public function __construct($path, $namespace, Extension $extension) {
    $this->path      = $path;
    $this->namespace = $namespace;
    $this->extension = $extension;
  }


  /**
   * Test whether the given value matches with a property of this location.
   *
   * @param int $type
   *   The property that should be tested. Use one of the Location::MATCH_* constants.
   * @param string $value
   *   The value that should be tested for.
   * @return bool
   */
  public function match($type, $value) {
    switch ($type) {
      case self::MATCH_PATH:
        return $this->path == $value;
      case self::MATCH_NAMESPACE:
        return $this->namespace == $value;
      case self::MATCH_MODULE:
        return $this->extension->getName() == $value;
      default:
        return false;
    }
  }


  /**
   * Return a list of all classes exposed by this location.
   *
   * @return string[]
   *   An array containing all class names keyed by their location.
   * @throws \Exception
   */
  public function getAllClasses() {
    if (!file_exists($this->path)) {
      throw new \Exception(t('The path `@path` does not exists.', array('@path' => $this->path)));
    }

    $classes = array();
    $iterator = new \RecursiveIteratorIterator(
      new \RecursiveDirectoryIterator($this->path, \RecursiveDirectoryIterator::SKIP_DOTS)
    );

    foreach ($iterator as $fileInfo) {
      if ($fileInfo->getExtension() != 'php') {
        continue;
      }

      $path = $iterator->getSubIterator()->getSubPath();
      $path = $path ? str_replace(DIRECTORY_SEPARATOR, '\\', $path) . '\\' : '';

      $class = $this->namespace . '\\' . $path . $fileInfo->getBasename('.php');
      $classes[$fileInfo->getPathName()] = $class;
    }

    return $classes;
  }


  /**
   * Return the base path that should be scanned.
   *
   * @return string
   */
  public function getPath() {
    return $this->path;
  }


  /**
   * Return the namespace the classes are located in.
   *
   * @return string
   */
  public function getNamespace() {
    return $this->namespace;
  }


  /**
   * Return the extension that exposes this location.
   *
   * @return Extension
   */
  public function getExtension() {
    return $this->extension;
  }
}
