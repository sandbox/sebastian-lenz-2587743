<?php

namespace Drupal\managed\Discovery\Listener;

use Drupal\managed\Annotation\HandlerAnnotation;
use Drupal\managed\Vendor\Addendum\ReflectionAnnotatedClass;
use Drupal\managed\Discovery\AbstractListener;
use Drupal\managed\Discovery\Location;
use Drupal\managed\Discovery\Scanner;


/**
 * A listener that collects all classes decorated with an `@Handler` decorator.
 */
class HandlerListener extends AbstractListener
{
  /**
   * @var \Drupal\managed\Annotation\HandlerAnnotation[]
   */
  private $handlerAnnotations;



  /**
   * Return all found handler annotations.
   *
   * @return \Drupal\managed\Annotation\HandlerAnnotation[]
   */
  public function getHandlerAnnotations() {
    return $this->handlerAnnotations;
  }


  /**
   * Called when a scanner is about to begin to look for classes.
   *
   * @param \Drupal\managed\Discovery\Scanner $scanner
   *   The scanner instance that has triggered this event.
   */
  public function onStart(Scanner $scanner) {
    $this->handlerAnnotations = array();
  }


  /**
   * Called when a scanner has found a class.
   *
   * @param \Drupal\managed\Vendor\Addendum\ReflectionAnnotatedClass $reflection
   *   The reflection of the found class.
   * @param \Drupal\managed\Discovery\Location $location
   *   The location the class has been found in.
   * @param \Drupal\managed\Discovery\Scanner $scanner
   *   The scanner instance that has triggered this event.
   */
  public function onReflection(ReflectionAnnotatedClass $reflection, Location $location, Scanner $scanner) {
    foreach ($reflection->getAllAnnotations('Handler') as $handlerAnnotation) {
      if (!($handlerAnnotation instanceof HandlerAnnotation)) {
        continue;
      }

      $this->handlerAnnotations[] = $handlerAnnotation;
    }
  }
}
