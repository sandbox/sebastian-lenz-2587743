<?php

namespace Drupal\managed\Discovery\Listener;

use Drupal\managed\Annotation\NodeAnnotation;
use Drupal\managed\Vendor\Addendum\ReflectionAnnotatedClass;
use Drupal\managed\Discovery\AbstractListener;
use Drupal\managed\Discovery\Location;
use Drupal\managed\Discovery\Scanner;


/**
 * A listener that collects all classes decorated with an `@Node` decorator.
 */
class NodeListener extends AbstractListener
{
  /**
   * @var \Drupal\managed\Annotation\NodeAnnotation[]
   */
  private $nodeAnnotations;



  /**
   * Return all found node annotations.
   *
   * @return \Drupal\managed\Annotation\NodeAnnotation[]
   */
  public function getNodeAnnotations() {
    return $this->nodeAnnotations;
  }


  /**
   * Called when a scanner is about to begin to look for classes.
   *
   * @param \Drupal\managed\Discovery\Scanner $scanner
   *   The scanner instance that has triggered this event.
   */
  public function onStart(Scanner $scanner) {
    $this->nodeAnnotations = array();
  }


  /**
   * Called when a scanner has found a class.
   *
   * @param \Drupal\managed\Vendor\Addendum\ReflectionAnnotatedClass $reflection
   *   The reflection of the found class.
   * @param \Drupal\managed\Discovery\Location $location
   *   The location the class has been found in.
   * @param \Drupal\managed\Discovery\Scanner $scanner
   *   The scanner instance that has triggered this event.
   */
  public function onReflection(ReflectionAnnotatedClass $reflection, Location $location, Scanner $scanner) {
    if (!$reflection->hasAnnotation('Node')) {
      return;
    }

    $nodeAnnotation = $reflection->getAnnotation('Node');
    if (!($nodeAnnotation instanceof NodeAnnotation)) {
      return;
    }

    $nodeAnnotation->pullReflectionData($reflection);
    $nodeAnnotation->pullLocationData($location);

    $parent = $reflection;
    while ($parent && $nodeAnnotation->pullParentClassData($parent)) {
      $parent = $parent->getParentClass();
    }

    $this->nodeAnnotations[$nodeAnnotation->id()] = $nodeAnnotation;
  }


  /**
   * Called when a scanner has finished its work.
   *
   * @param \Drupal\managed\Discovery\Scanner $scanner
   *   The scanner instance that has triggered this event.
   */
  public function onEnd(Scanner $scanner) {
    /** @var \Drupal\managed\Discovery\Listener\HandlerListener $handlerListener */
    $handlerListener = $scanner->getListenerByClass('Drupal\managed\Discovery\Listener\HandlerListener');
    if (!is_null($handlerListener)) {
      $handlerAnnotations = $handlerListener->getHandlerAnnotations();
    }

    foreach ($this->nodeAnnotations as $nodeAnnotation) {
      if (isset($handlerAnnotations)) {
        $nodeAnnotation->pullHandlers($handlerAnnotations);
      }

      $nodeAnnotation->pullFieldData();
    }
  }
}
