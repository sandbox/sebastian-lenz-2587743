<?php

namespace Drupal\managed\Discovery\Listener;

use Drupal\managed\Annotation\EntityAnnotation;
use Drupal\managed\Vendor\Addendum\ReflectionAnnotatedClass;
use Drupal\managed\Discovery\AbstractListener;
use Drupal\managed\Discovery\Location;
use Drupal\managed\Discovery\Scanner;


/**
 * A listener that collects all classes decorated with an `@Entity` decorator.
 */
class EntityListener extends AbstractListener
{
  /**
   * @var \Drupal\managed\Annotation\EntityAnnotation[]
   */
  private $entityAnnotations;



  /**
   * Return all found entity annotations.
   *
   * @return \Drupal\managed\Annotation\EntityAnnotation[]
   */
  public function getEntityAnnotations() {
    return $this->entityAnnotations;
  }


  /**
   * Called when a scanner is about to begin to look for classes.
   *
   * @param \Drupal\managed\Discovery\Scanner $scanner
   *   The scanner instance that has triggered this event.
   */
  public function onStart(Scanner $scanner) {
    $this->entityAnnotations = array();
  }


  /**
   * Called when a scanner has found a class.
   *
   * @param \Drupal\managed\Vendor\Addendum\ReflectionAnnotatedClass $reflection
   *   The reflection of the found class.
   * @param \Drupal\managed\Discovery\Location $location
   *   The location the class has been found in.
   * @param \Drupal\managed\Discovery\Scanner $scanner
   *   The scanner instance that has triggered this event.
   */
  public function onReflection(ReflectionAnnotatedClass $reflection, Location $location, Scanner $scanner) {
    if (!$reflection->hasAnnotation('Entity')) {
      return;
    }

    $entityAnnotation = $reflection->getAnnotation('Entity');
    if (!($entityAnnotation instanceof EntityAnnotation)) {
      return;
    }

    $entityAnnotation->pullReflectionData($reflection);
    $entityAnnotation->pullLocationData($location);

    $parent = $reflection;
    while ($parent && $entityAnnotation->pullParentClassData($parent)) {
      $parent = $parent->getParentClass();
    }

    $this->entityAnnotations[$entityAnnotation->id()] = $entityAnnotation;
  }


  /**
   * Called when a scanner has finished its work.
   *
   * @param \Drupal\managed\Discovery\Scanner $scanner
   *   The scanner instance that has triggered this event.
   */
  public function onEnd(Scanner $scanner) {
    /** @var \Drupal\managed\Discovery\Listener\BundleListener $bundleListener */
    $bundleListener = $scanner->getListenerByClass('Drupal\managed\Discovery\Listener\BundleListener');
    if (!is_null($bundleListener)) {
      $bundleAnnotations = $bundleListener->getBundleAnnotations();
    }

    /** @var \Drupal\managed\Discovery\Listener\HandlerListener $handlerListener */
    $handlerListener = $scanner->getListenerByClass('Drupal\managed\Discovery\Listener\HandlerListener');
    if (!is_null($handlerListener)) {
      $handlerAnnotations = $handlerListener->getHandlerAnnotations();
    }

    foreach ($this->entityAnnotations as $entityAnnotation) {
      if (isset($bundleAnnotations)) {
        $entityAnnotation->pullBundles($bundleAnnotations);
      }

      if (isset($handlerAnnotations)) {
        $entityAnnotation->pullHandlers($handlerAnnotations);
      }

      $entityAnnotation->pullFieldData();
    }
  }
}
