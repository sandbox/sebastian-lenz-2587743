<?php

namespace Drupal\managed\Discovery\Listener;

use Drupal\managed\Annotation\BundleAnnotation;
use Drupal\managed\Vendor\Addendum\ReflectionAnnotatedClass;
use Drupal\managed\Discovery\AbstractListener;
use Drupal\managed\Discovery\Location;
use Drupal\managed\Discovery\Scanner;


/**
 * A listener that collects all classes decorated with an `@Bundle` decorator.
 */
class BundleListener extends AbstractListener
{
  /**
   * @var \Drupal\managed\Annotation\BundleAnnotation[]
   */
  private $bundleAnnotations;



  /**
   * Return all found bundle annotations.
   *
   * @return \Drupal\managed\Annotation\BundleAnnotation[]
   */
  public function getBundleAnnotations() {
    return $this->bundleAnnotations;
  }


  /**
   * Called when a scanner is about to begin to look for classes.
   *
   * @param \Drupal\managed\Discovery\Scanner $scanner
   *   The scanner instance that has triggered this event.
   */
  public function onStart(Scanner $scanner) {
    $this->bundleAnnotations = array();
  }


  /**
   * Called when a scanner has found a class.
   *
   * @param \Drupal\managed\Vendor\Addendum\ReflectionAnnotatedClass $reflection
   *   The reflection of the found class.
   * @param \Drupal\managed\Discovery\Location $location
   *   The location the class has been found in.
   * @param \Drupal\managed\Discovery\Scanner $scanner
   *   The scanner instance that has triggered this event.
   */
  public function onReflection(ReflectionAnnotatedClass $reflection, Location $location, Scanner $scanner) {
    if (!$reflection->hasAnnotation('Bundle')) {
      return;
    }

    $bundleAnnotation = $reflection->getAnnotation('Bundle');
    if (!($bundleAnnotation instanceof BundleAnnotation)) {
      return;
    }

    $bundleAnnotation->pullReflectionData($reflection);
    $bundleAnnotation->pullLocationData($location);

    $parent = $reflection;
    while ($parent && $bundleAnnotation->pullParentClassData($parent)) {
      $parent = $parent->getParentClass();
    }

    $this->bundleAnnotations[$bundleAnnotation->id()] = $bundleAnnotation;
  }
}
