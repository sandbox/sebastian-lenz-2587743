<?php

namespace Drupal\managed\Discovery;

use Drupal\Core\Extension\InfoParser;
use Drupal\managed\Vendor\Addendum\ReflectionAnnotatedClass;
use Drupal\managed\Utils\ContainerAware;


/**
 * A file and directory scanner that parses annotated classes.
 */
class Scanner extends ContainerAware
{
  /**
   * A list of locations the discovery should check for.
   *
   * @var \Drupal\managed\Discovery\Location[]
   */
  private $locations = array();

  /**
   * @var \Drupal\managed\Discovery\AbstractListener[]
   */
  private $listeners = array();



  /**
   * Run the scanner.
   *
   * @return $this
   */
  public function scan() {
    $this->getAnnotationManager()->activate();

    foreach ($this->listeners as $listener) {
      $listener->onStart($this);
    }

    foreach ($this->locations as $location) {
      $this->scanLocation($location);
    }

    foreach ($this->listeners as $listener) {
      $listener->onEnd($this);
    }

    return $this;
  }


  /**
   * Scan the given location for classes.
   *
   * @param \Drupal\managed\Discovery\Location $location
   *   The location that should be scanned.
   */
  private function scanLocation(Location $location) {
    foreach ($location->getAllClasses() as $class) {
      try {
        $reflection = new ReflectionAnnotatedClass($class);
      } catch (\Exception $e) {
        drupal_set_message(t(
          'The class `@class` could not be reflected: @message.',
          array(
            '@class'   => $class,
            '@message' => $e->getMessage()
          )
        ), 'error');
        continue;
      }

      foreach ($this->listeners as $listener) {
        $listener->onReflection($reflection, $location, $this);
      }
    }
  }


  /**
   * Attach a listener to this scanner instance.
   *
   * @param \Drupal\managed\Discovery\AbstractListener $listener
   *   The listener instance that should be attached.
   * @return $this
   */
  public function addListener(AbstractListener $listener) {
    $this->listeners[] = $listener;

    return $this;
  }


  /**
   * Add a location to be checked for.
   *
   * @param \Drupal\managed\Discovery\Location $location
   *   The location that should be checked for classes.
   * @return $this
   */
  public function addLocation(Location $location) {
    $this->locations[] = $location;
    return $this;
  }


  /**
   * Add the source paths of all managed modules as locations
   * to be checked for classes.
   *
   * @return $this
   */
  public function addAllManagedLocations() {
    $parser        = new InfoParser();
    $moduleHandler = $this->getModuleHandler();
    $namespaces    = $this->getAllNamespaces();

    foreach ($moduleHandler->getModuleList() as $extension) {
      $info = $parser->parse($extension->getPathname());
      if (!isset($info['managed']) || !$info['managed']) {
        continue;
      }

      $namespace = 'Drupal\\' . $extension->getName();
      if (isset($namespaces[$namespace])) {
        $this->addLocation(new Location($namespaces[$namespace], $namespace, $extension));
      }
    }

    return $this;
  }


  /**
   * Detach a listener from this scanner instance.
   *
   * @param \Drupal\managed\Discovery\AbstractListener $listener
   * @return $this
   */
  public function removeListener(AbstractListener $listener) {
    $index = array_search($listener, $this->listeners);
    if ($index !== FALSE) {
      array_splice($this->listeners, $index, 1);
    }

    return $this;
  }


  /**
   * Remove all listeners that have been attached.
   *
   * @return $this
   */
  public function removeAllListeners() {
    $this->listeners = array();
    return $this;
  }


  /**
   * Remove a specific location from this instance.
   *
   * @param \Drupal\managed\Discovery\Location $location
   *   The location instance that should be removed.
   * @return $this
   */
  public function removeLocation(Location $location) {
    $index = array_search($location, $this->locations);
    if ($index !== FALSE) {
      array_splice($this->locations, $index, 1);
    }

    return $this;
  }


  /**
   * Remove all locations that have been added.
   *
   * @return $this
   */
  public function removeAllLocations() {
    $this->locations = array();
    return $this;
  }


  /**
   * Remove a location based on the given type and value.
   *
   * @param int $type
   *   The property that should be tested. Use one of the Location::MATCH_* constants.
   * @param string $value
   *   The value that should be tested for.
   * @return $this
   */
  public function removeLocationByValue($type, $value) {
    $locations = $this->locations;
    foreach ($locations as $location) {
      if ($location->match($type, $value)) {
        $this->removeLocation($location);
      }
    }

    return $this;
  }


  /**
   * Return all listeners attached to this scanner.
   *
   * @return \Drupal\managed\Discovery\AbstractListener[]
   */
  public function getListeners() {
    return $this->listeners;
  }


  /**
   * Retrieve a listener by its class.
   *
   * @param string $class
   * @return \Drupal\managed\Discovery\AbstractListener|null
   */
  public function getListenerByClass($class) {
    foreach ($this->listeners as $listener) {
      if ($listener instanceof $class) {
        return $listener;
      }
    }

    return NULL;
  }


  /**
   * Return all locations attached to this scanner.
   *
   * @return \Drupal\managed\Discovery\Location[]
   */
  public function getLocations() {
    return $this->locations;
  }


  /**
   * Return the module handler service.
   *
   * @return \Drupal\Core\Extension\ModuleHandler
   */
  private function getModuleHandler() {
    return $this->getService('module_handler');
  }


  /**
   * Return the module handler service.
   *
   * @return \Drupal\managed\Core\ManagedAnnotationPluginManager
   */
  private function getAnnotationManager() {
    return $this->getService('managed.annotation_manager');
  }


  /**
   * Return a list of all known namespaces.
   *
   * @return string[]
   */
  private function getAllNamespaces() {
    return $this->getService('container.namespaces');
  }
}
