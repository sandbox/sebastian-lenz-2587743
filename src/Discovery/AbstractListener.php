<?php

namespace Drupal\managed\Discovery;

use Drupal\managed\Vendor\Addendum\ReflectionAnnotatedClass;


/**
 * Base class of all listeners that can be attached to a Scanner instance.
 */
abstract class AbstractListener
{
  /**
   * Called when a scanner is about to begin to look for classes.
   *
   * @param \Drupal\managed\Discovery\Scanner $scanner
   *   The scanner instance that has triggered this event.
   */
  public function onStart(Scanner $scanner) { }


  /**
   * Called when a scanner has found a class.
   *
   * @param \Drupal\managed\Vendor\Addendum\ReflectionAnnotatedClass $reflection
   *   The reflection of the found class.
   * @param \Drupal\managed\Discovery\Location $location
   *   The location the class has been found in.
   * @param \Drupal\managed\Discovery\Scanner $scanner
   *   The scanner instance that has triggered this event.
   */
  public function onReflection(ReflectionAnnotatedClass $reflection, Location $location, Scanner $scanner) { }


  /**
   * Called when a scanner has finished its work.
   *
   * @param \Drupal\managed\Discovery\Scanner $scanner
   *   The scanner instance that has triggered this event.
   */
  public function onEnd(Scanner $scanner) { }
}
