<?php

namespace Drupal\managed\Discovery;

use Drupal\managed\Discovery\Listener\BundleListener;
use Drupal\managed\Discovery\Listener\EntityListener;
use Drupal\managed\Discovery\Listener\HandlerListener;
use Drupal\managed\Discovery\Listener\NodeListener;
use Drupal\managed\Utils\ContainerAware;


class Discovery extends ContainerAware
{
  /**
   * The list of all discovered entity annotations.
   *
   * @var \Drupal\managed\Annotation\EntityAnnotation[]
   */
  private $entityAnnotations;

  /**
   * The list of all discovered node annotations.
   *
   * @var \Drupal\managed\Annotation\NodeAnnotation[]
   */
  private $nodeAnnotations;

  /**
   * The uncached list of all discovered entity annotations.
   *
   * @var \Drupal\managed\Annotation\EntityAnnotation[]
   */
  private $uncachedEntityAnnotations;

  /**
   * The uncached list of all discovered node annotations.
   *
   * @var \Drupal\managed\Annotation\NodeAnnotation[]
   */
  private $uncachedNodeAnnotations;

  /**
   * Whether caches should be used or not.
   *
   * @var bool
   */
  private $useCaches = TRUE;



  /**
   * Start the class discovery process.
   *
   * @return $this
   */
  public function discover() {
    $this->discoverScan();

    $this->entityAnnotations = $this->uncachedEntityAnnotations;
    $this->nodeAnnotations   = $this->uncachedNodeAnnotations;

    $this->getPersistentData()
      ->setEntityAnnotations($this->entityAnnotations)
      ->setNodeAnnotations($this->nodeAnnotations);

    return $this;
  }


  /**
   * Run the scanner.
   *
   * @return $this
   */
  private function discoverScan() {
    $entityListener = new EntityListener();
    $nodeListener   = new NodeListener();

    $scanner = new Scanner();
    $scanner
      ->setContainer($this->getContainer())
      ->addListener($entityListener)
      ->addListener($nodeListener)
      ->addListener(new BundleListener())
      ->addListener(new HandlerListener())
      ->addAllManagedLocations()
      ->scan();

    $this->uncachedEntityAnnotations = $entityListener->getEntityAnnotations();
    $this->uncachedNodeAnnotations   = $nodeListener->getNodeAnnotations();

    return $this;
  }


  /**
   * Set whether caches should be used or not.
   *
   * @param $value
   */
  public function useCaches($value) {
    $this->useCaches = $value;
  }


  /**
   * Return a list of all discovered entity annotations.
   *
   * @return \Drupal\managed\Annotation\EntityAnnotation[]
   */
  public function getEntityAnnotations() {
    if (!$this->useCaches) {
      return $this->getUncachedEntityAnnotations();
    }

    if (!isset($this->entityAnnotations)) {
      $this->entityAnnotations = $this->getPersistentData()->getEntityAnnotations();
    }

    return $this->entityAnnotations;
  }


  /**
   * Return a list of all discovered node annotations.
   *
   * @return \Drupal\managed\Annotation\NodeAnnotation[]
   */
  public function getNodeAnnotations() {
    if (!$this->useCaches) {
      return $this->getUncachedNodeAnnotations();
    }

    if (!isset($this->nodeAnnotations)) {
      $this->nodeAnnotations = $this->getPersistentData()->getNodeAnnotations();
    }

    return $this->nodeAnnotations;
  }


  /**
   * Return a list of all discovered entity annotations without using
   * the caching mechanism.
   *
   * @return \Drupal\managed\Annotation\EntityAnnotation[]
   */
  public function getUncachedEntityAnnotations() {
    if (!isset($this->uncachedEntityAnnotations)) {
      $this->discoverScan();
    }

    return $this->uncachedEntityAnnotations;
  }


  /**
   * Return a list of all discovered node annotations without using
   * the caching mechanism.
   *
   * @return \Drupal\managed\Annotation\NodeAnnotation[]
   */
  public function getUncachedNodeAnnotations() {
    if (!isset($this->uncachedNodeAnnotations)) {
      $this->discoverScan();
    }

    return $this->uncachedNodeAnnotations;
  }


  /**
   * Return the cache backend that should be used to cache
   * discovery results.
   *
   * @return \Drupal\managed\Core\PersistentData
   */
  public function getPersistentData() {
    return $this->getService('managed.persistent');
  }
}
