<?php

namespace Drupal\managed_examples\TranslatableEntity;

use Drupal\managed\Entity;


/**
 * A translatable entity.
 *
 * @Entity()
 * @ContentAdminBehaviour()
 */
class TranslatableEntity extends Entity
{
  /**
   * @PrimaryField()
   */
  public $id;

  /**
   * @LanguageField()
   */
  public $language;

  /**
   * @TextField()
   * @EntityKey('label')
   */
  public $label;

  /**
   * @TextField(isTranslatable=false)
   */
  public $sharedText;

  /**
   * @TextField(isPayload=true, isTranslatable=false)
   */
  public $payloadText;

  /**
   * @TextField(isPayload=true, isTranslatable=true)
   */
  public $translateablePayloadText;
}
