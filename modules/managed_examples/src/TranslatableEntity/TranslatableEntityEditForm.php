<?php

namespace Drupal\managed_examples\TranslatableEntity;

use Drupal\Core\Form\FormStateInterface;
use Drupal\managed\Behaviour\Form\EditForm;


/**
 * A translatable entity.
 *
 * @Handler(entity="translatable_entity", type="default")
 */
class TranslatableEntityEditForm extends EditForm
{
  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['message'] = array(
      '#markup' => '<p><strong>This is Drupal\managed_examples\TranslatableEntity\TranslatableEntityEditForm</strong></p>',
      '#weight' => -100
    );

    return $form;
  }
}
