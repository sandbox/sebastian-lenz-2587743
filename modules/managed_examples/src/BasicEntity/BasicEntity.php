<?php

namespace Drupal\managed_examples\BasicEntity;

use Drupal\managed\Entity;


/**
 * A basic managed entity.
 *
 * @Entity()
 * @ContentAdminBehaviour(listColumns = {'@label', 'more', '@operations'})
 */
class BasicEntity extends Entity
{
  /**
   * @PrimaryField()
   */
  public $id;

  /**
   * @TextField()
   * @EntityKey('label')
   */
  public $labels;

  /**
   * @TextField()
   */
  public $more;

  /**
   * @var \Drupal\managed_examples\BundleEntity\BundleEntity[]
   * @OneToManyRelation(entity='bundle_entity', sort='weight')
   */
  public $bundleEntities;
}
