<?php

namespace Drupal\managed_examples\BundleEntity;


/**
 * A basic managed entity with bundles.
 *
 * @Bundle(id="type_a")
 */
class TypeABundleEntity extends BundleEntity
{
  /**
   * @TextField()
   */
  public $additionalText;

  /**
   * @ImageField()
   */
  public $additionalImage;


  /**
   * Gets the label of the entity.
   *
   * @return string|null
   *   The label of the entity, or NULL if there is no label defined.
   */
  public function label() {
    $val = $this->label->getString();
    return $val . ' (Type A)';
  }
}
