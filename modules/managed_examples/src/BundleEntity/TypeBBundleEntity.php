<?php

namespace Drupal\managed_examples\BundleEntity;


/**
 * A basic managed entity with bundles.
 *
 * @Bundle(id="type_b")
 */
class TypeBBundleEntity extends BundleEntity
{
  /**
   * @SelectField()
   * @Option(value=1, label="First option")
   * @Option(value=2, label="Second option")
   * @Option(value=3, label="Third option")
   */
  public $additionalSelect;


  /**
   * Gets the label of the entity.
   *
   * @return string|null
   *   The label of the entity, or NULL if there is no label defined.
   */
  public function label() {
    $val = $this->label->getString();
    return $val . ' (Type B)';
  }
}
