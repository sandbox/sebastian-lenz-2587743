<?php

namespace Drupal\managed_examples\BundleEntity;

use Drupal\managed\Entity;


/**
 * A basic managed entity with bundles.
 *
 * @Entity()
 * @ContentAdminBehaviour()
 */
class BundleEntity extends Entity
{
  /**
   * @var \Drupal\Core\Field\FieldItemList
   * @PrimaryField()
   */
  public $id;

  /**
   * @var \Drupal\Core\Field\FieldItemList
   * @BundleField()
   */
  public $type;

  /**
   * @var \Drupal\Core\Field\FieldItemList
   * @WeightField()
   */
  public $weight;

  /**
   * @var \Drupal\Core\Field\FieldItemList
   * @TextField(isRequired=TRUE)
   * @EntityKey('label')
   */
  public $label;

  /**
   * @var \Drupal\Core\Field\FieldItemList
   * @ReferenceField(entity='basic_entity')
   */
  public $parent;
}
