<?php

namespace Drupal\managed_examples\CustomNode;

use Drupal\managed\Node;


/**
 * A custom node type defined in code.
 *
 * @Node()
 */
class CustomNode extends Node
{
  /**
   * @var \Drupal\Core\Field\FieldItemList
   * @TextField()
   */
  public $teaserTitle;



  public function getTitle() {
    return $this->get('title')->toString() . ' (CustomNode)';
  }
}
