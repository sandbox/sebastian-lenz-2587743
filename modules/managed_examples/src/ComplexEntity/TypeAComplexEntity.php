<?php

namespace Drupal\managed_examples\ComplexEntity;

use Drupal\managed\Entity;


/**
 * A translatable and revisionable entity.
 *
 * @Bundle()
 */
class TypeAComplexEntity extends ComplexEntity
{
}
