<?php

namespace Drupal\managed_examples\ComplexEntity;

use Drupal\managed\Entity;
use Drupal\managed\Scaffold\TranslatableRevisionableTrait;


/**
 * A translatable and revisionable entity.
 *
 * @Entity()
 * @ContentAdminBehaviour()
 */
class ComplexEntity extends Entity
{
  use TranslatableRevisionableTrait;


  /**
   * @PrimaryField()
   */
  public $id;

  /**
   * @BundleField()
   */
  public $type;

  /**
   * @TextField(entityKey='label')
   */
  public $label;
}
